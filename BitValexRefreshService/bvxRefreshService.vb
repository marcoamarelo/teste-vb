﻿Imports System.Runtime.InteropServices

Public Class bvxRefreshService


#Region "Define"

    Public Enum ServiceState
        SERVICE_STOPPED = &H1
        SERVICE_START_PENDING = &H2
        SERVICE_STOP_PENDING = &H3
        SERVICE_RUNNING = &H4
        SERVICE_CONTINUE_PENDING = &H5
        SERVICE_PAUSE_PENDING = &H6
        SERVICE_PAUSED = &H7
    End Enum

    <StructLayout(LayoutKind.Sequential)>
    Public Structure ServiceStatus
        Public dwServiceType As Integer
        Public dwCurrentState As ServiceState
        Public dwControlsAccepted As Integer
        Public dwWin32ExitCode As Integer
        Public dwServiceSpecificExitCode As Integer
        Public dwCheckPoint As Integer
        Public dwWaitHint As Integer
    End Structure

#End Region


#Region "DLL"
    <DllImport("advapi32.dll", SetLastError:=True)>
    Private Shared Function SetServiceStatus(ByVal handle As IntPtr, ByRef serviceStatus As ServiceStatus) As Boolean
    End Function

#End Region


    Private eventId As Integer = 1
    Private ExchangeId As Integer = -1

    Private timer As System.Timers.Timer

    Public Sub New()
        ' Esta chamada é requerida pelo designer.
        InitializeComponent()
        ' Adicione qualquer inicialização após a chamada InitializeComponent().
        eventLog1 = New System.Diagnostics.EventLog
        If Not System.Diagnostics.EventLog.SourceExists("bvxRefreshServiceEventSource") Then
            EventLog.CreateEventSource("bvxRefreshServiceEventSource", "bvxRefreshServiceEventSourceLog")
        End If
        eventLog1.Source = "bvxRefreshServiceEventSource"
        eventLog1.Log = "bvxRefreshServiceEventSourceLog"
    End Sub


    Public Sub New(args As String())
        ' Esta chamada é requerida pelo designer.
        InitializeComponent()
        'espera o primeiro argumento como exchange id
        If args.Count > 0 Then
            ExchangeId = args(0)
        End If

        ' Adicione qualquer inicialização após a chamada InitializeComponent().
        eventLog1 = New System.Diagnostics.EventLog
        If Not System.Diagnostics.EventLog.SourceExists("bvxRefreshServiceEventSource") Then
            EventLog.CreateEventSource("bvxRefreshServiceEventSource", "bvxRefreshServiceEventSourceLog")
        End If
        eventLog1.Source = "bvxRefreshServiceEventSource"
        eventLog1.Log = "bvxRefreshServiceEventSourceLog"
    End Sub





#Region "Event Handlers"
    Protected Overrides Sub OnStart(ByVal args() As String)
        eventLog1.WriteEntry("bvxRefreshService in OnStart")
        timer = New System.Timers.Timer
        timer.Interval = 5000
        'timer.Elapsed += New System.Timers.ElapsedEventHandler(Me.OnTimer)
        AddHandler timer.Elapsed, New System.Timers.ElapsedEventHandler(AddressOf Me.OnTimer)
        timer.Start()

        ' Update the service state to Start Pending.
        Dim serviceStatus As ServiceStatus = New ServiceStatus()
        serviceStatus.dwCurrentState = ServiceState.SERVICE_START_PENDING
        serviceStatus.dwWaitHint = 100000
        SetServiceStatus(Me.ServiceHandle, serviceStatus)

        ' Update the service state to Running.  
        serviceStatus.dwCurrentState = ServiceState.SERVICE_RUNNING
        SetServiceStatus(Me.ServiceHandle, serviceStatus)
    End Sub


    Protected Overrides Sub OnStop()
        eventLog1.WriteEntry("bvxRefreshService in OnStop")

        Dim serviceStatus As ServiceStatus = New ServiceStatus()
        serviceStatus.dwCurrentState = ServiceState.SERVICE_STOPPED
        serviceStatus.dwWaitHint = 100000
        SetServiceStatus(Me.ServiceHandle, serviceStatus)
    End Sub


    Protected Overrides Sub OnContinue()
        eventLog1.WriteEntry("bvxRefreshService in OnContinue")
    End Sub


    Protected Overrides Sub OnPause()
        eventLog1.WriteEntry("bvxRefreshService in OnPause")
    End Sub


    Protected Overrides Sub OnShutdown()
        eventLog1.WriteEntry("bvxRefreshService in OnShutdown")
    End Sub


    Public Sub OnTimer(sender As Object, args As System.Timers.ElapsedEventArgs)
        eventLog1.WriteEntry("bvxRefreshService monitoring before sleep", EventLogEntryType.Information, eventId)
        Threading.Thread.Sleep(5000)
        eventLog1.WriteEntry("bvxRefreshService monitoring after sleep", EventLogEntryType.Information, eventId)

        eventId = eventId + 1
    End Sub

#End Region



End Class
