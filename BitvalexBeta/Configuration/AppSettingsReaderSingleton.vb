﻿Imports System.Configuration

Public Class AppSettingsReaderSingleton

    Private Shared _instance As AppSettingsReader
    Private Shared ReadOnly _lock As Object = New Object()


    Public Shared ReadOnly Property Instance() As AppSettingsReader
        Get
            SyncLock _lock
                If (_instance Is Nothing) Then
                    _instance = New AppSettingsReader
                End If
            End SyncLock

            Return _instance
        End Get

    End Property

End Class
