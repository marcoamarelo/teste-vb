﻿Public Class ConfigOportunitiesSingleton

    Private Shared _instance As bvxConfigOportunities
    Private Shared ReadOnly _lock As Object = New Object()
    Public Shared Sub LoadConfig(jsonObject As JsonObject)
        _instance = jsonObject.ToClass(Of bvxConfigOportunities)
    End Sub

    Public Shared ReadOnly Property Instance() As bvxConfigOportunities
        Get
            SyncLock _lock
                If (_instance Is Nothing) Then
                    Return Nothing
                    'Throw New Exception("Não foi possível carregar as configurações de busca de oportunidades")
                End If
            End SyncLock

            Return _instance
        End Get

    End Property




End Class
