﻿Imports System.IO

Public Class ExchangesSingleton

    Private Shared _instance As bvxExchangesList


    Private Shared ReadOnly _lock As Object = New Object()

    Public Shared Sub LoadConfig()

        Try
            Dim config = AppSettingsReaderSingleton.Instance
            Dim pFileName As String = Application.StartupPath + config.GetValue("ExchangesFile", GetType(String))
            Dim rawJson = File.ReadAllText(pFileName)
            _instance = New bvxExchangesList
            _instance.List = JsonFactory.CreateJsonObject(rawJson).ToClass(Of List(Of bvxExchange))
            If _instance.List Is Nothing Then
                Throw New Exception("Error loading Exchanges json file.")
            End If
        Catch ex As Exception
            Throw New Exception("Error loading Exchanges json file.", ex)
        End Try


    End Sub

    Public Shared ReadOnly Property Instance() As bvxExchangesList

        Get
            SyncLock _lock
                If (_instance Is Nothing) Then
                    LoadConfig()
                    Return _instance
                    'Throw New Exception("Não foi possível carregar as configurações de busca de oportunidades")
                End If
            End SyncLock

            Return _instance
        End Get

    End Property




End Class
