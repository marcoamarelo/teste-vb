﻿Public Class ProcessManagerSigleton



    Private Shared _instance As bvxProcessManager
    Private Shared ReadOnly _lock As Object = New Object()


    Public Shared ReadOnly Property Instance() As bvxProcessManager
        Get
            SyncLock _lock
                If (_instance Is Nothing) Then
                    _instance = ProcessManagerFactory.CreateObject
                End If
            End SyncLock

            Return _instance
        End Get

    End Property


End Class
