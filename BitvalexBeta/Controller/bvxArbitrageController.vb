﻿Imports System.IO
Imports System.Threading

Public Class bvxArbitrageController

    '    Public Property MarketNameList As List(Of String)
    Public Property ConfigFileName As String = Nothing


    Public Property PersistInDatabase As Boolean = False


#Region "Events"

    Public Delegate Sub LoadFinishedDelegate(ByVal pConfigOportunities As bvxConfigOportunities)
    Public Delegate Sub RefreshFinishedDelegate(ByVal pConfigOportunities As bvxConfigOportunities)
    Public Delegate Sub MessageEventDelegate(ByVal Sender As Object, ByVal pMessage As String, ByVal pMessageLevel As Integer)

    Public Event OnLoadFinished As LoadFinishedDelegate
    Public Event OnRefreshFinished As RefreshFinishedDelegate
    Public Event OnMessageEvent As MessageEventDelegate

    Private Sub RaiseLoadFinished(ByVal objConfigOportunities As bvxConfigOportunities)
        RaiseEvent OnLoadFinished(objConfigOportunities)
    End Sub

    Private Sub RaiseRefreshFinished(ByVal objConfigOportunities As bvxConfigOportunities)
        RaiseEvent OnRefreshFinished(objConfigOportunities)
    End Sub

    Private Sub RaiseMessage(pMessage As String, pLevel As Integer)
        RaiseEvent OnMessageEvent(Me, pMessage, pLevel)
    End Sub

#End Region


#Region "Initial Methods"

    'load information from json configuration file
    'call methods to fill the objects WITH INITIAL VALUES
    Public Sub LoadConfigOportunitiesFromFile(ByVal pConfigFileName As String)
        Try

            RaiseMessage("Loading configuration json file.", 3)
            LogService.Write("***************************************************************", LogService.Type.INFO)
            LogService.Write("******Starting process", LogService.Type.INFO)
            LogService.Write("******Loading configuration json file. Filename: " + pConfigFileName, LogService.Type.INFO)
            ConfigFileName = pConfigFileName
            Dim rawJson = File.ReadAllText(ConfigFileName)
            ConfigOportunitiesSingleton.LoadConfig(JsonFactory.CreateJsonObject(rawJson))

            ConfigOportunitiesSingleton.Instance.PersistInDatabase = Me.PersistInDatabase

            RaiseMessage("Creating objects.", 3)


            InitializeExchanges(ConfigOportunitiesSingleton.Instance)

            InitializeConfigOportunitiesIntra(ConfigOportunitiesSingleton.Instance)
            InitializeConfigOportunitiesBetween(ConfigOportunitiesSingleton.Instance)

            ConfigOportunitiesSingleton.Instance.State = bvxConfigOportunities.bvxConfigOportunitiesState.Ready
            RaiseMessage("Load completed.", 3)

            LogService.Write("******Finished: Loading configuration json file.", LogService.Type.INFO)
            LogService.Write("***************************************************************", LogService.Type.INFO)
        Catch ex As Exception
            LogService.Write("***Error on bvxArbitrageController.LoadConfigOportunitiesFromFile. " + Environment.NewLine + ex.ToString(), LogService.Type.FATAL)
            RaiseMessage("Error on Controller.LoadConfigOportunitiesFromFile. Error: " + ex.Message, 3)
        Finally
            RaiseLoadFinished(ConfigOportunitiesSingleton.Instance)
        End Try

    End Sub


    Private Sub InitializeExchanges(ByRef pConfigOportunities As bvxConfigOportunities)

        Dim List As List(Of String) = (From T1 In pConfigOportunities.Intra.AsEnumerable Select T1.Exchange).Union(
        From T2 In pConfigOportunities.Between.AsEnumerable Select T2.Exchange).Distinct.ToList()

        pConfigOportunities.Exchanges.Clear()
        For Each ExchangeName In List
            pConfigOportunities.Exchanges.Add(
                GetExchangeByNameFromDbWithMarketListFromAPI(ExchangeName,
                                                    ConfigOportunitiesSingleton.Instance.PersistInDatabase))
        Next
    End Sub


    Public Function GetExchangeByNameFromDbWithMarketListFromAPI(pExchangeName As String,
                                                                  pPersistInDatabase As Boolean) As bvxExchange

        Dim Exchange As bvxExchange = GetExchangeByNameFromDb(pExchangeName, pPersistInDatabase)
        'carrega a lista de mercados vinda da web
        InitializeExchangeMarketList(Exchange)
        Return Exchange

    End Function

    Private Sub InitializeConfigOportunitiesBetween(ByRef pConfigOportunities As bvxConfigOportunities)
        'LoadExchangesFromJsonFile(pConfigOportunities)
        LogService.Write("Starting between configuration.", LogService.Type.INFO)
        'Intra rounds
        'lista com nome e id do banco
        Dim ExchangeList = ExchangesSingleton.Instance
        For i As Integer = 0 To pConfigOportunities.Between.Count - 1
            Dim Btw1 As bvxArbitBetween = pConfigOportunities.Between(i)
            For j As Integer = i + 1 To pConfigOportunities.Between.Count - 1
                Dim Btw2 As bvxArbitBetween = pConfigOportunities.Between(j)
                'cria o round
                Dim Round As bvxArbitBetweenRound = New bvxArbitBetweenRound
                Round.Exchange1 = GetExchangeByNameFromMasterList(Btw1.Exchange)
                Round.Exchange2 = GetExchangeByNameFromMasterList(Btw2.Exchange)
                Round.FeeExchange1 = Btw1.Fee
                Round.FeeExchange2 = Btw2.Fee

                Round.Wallet1 = Btw1.Wallet.Split(",")
                Round.Wallet2 = Btw2.Wallet.Split(",")
                pConfigOportunities.BetweenRounds.Add(Round)
                'cria as legs
                'para cada round, gera lista de mercados possíveis para as moedas da carteira
                'ListCoins é a lista de moedas comuns nas duas carteiras
                Dim ListCoins = ((From T1 In Round.Wallet1.AsEnumerable Select T1.ToUpper()).Intersect(From T1 In Round.Wallet2.AsEnumerable Select T1.ToUpper()).Distinct.ToList())
                For iCoin1 As Integer = 0 To ListCoins.Count - 2
                    Dim sCoin1 As String = ListCoins(iCoin1).Trim()
                    For iCoin2 As Integer = iCoin1 + 1 To ListCoins.Count - 1
                        Dim sCoin2 As String = ListCoins(iCoin2).Trim()
                        'pegar mercados das duas exchanges que atendam este par e fazer a intercessão das listas
                        Dim Ida As String = sCoin1.ToUpper() + "_" + sCoin2.ToUpper
                        Dim Volta As String = sCoin2.ToUpper() + "_" + sCoin1.ToUpper
                        Dim ListMarketExchange1 = Round.Exchange1.MarketList.Where(Function(x) x.ToUpper = Ida Or x.ToUpper = Volta).Select(Function(y) y.ToUpper())
                        Dim ListMarketExchange2 = Round.Exchange2.MarketList.Where(Function(x) x.ToUpper = Ida Or x.ToUpper = Volta).Select(Function(y) y.ToUpper())
                        Dim ListMarketIntersect = ListMarketExchange1.Intersect(ListMarketExchange2)
                        For Each sPair In ListMarketIntersect
                            Dim Leg As bvxArbitBetweenRoundLeg = New bvxArbitBetweenRoundLeg()
                            Leg.MarketName = sPair
                            Dim PairSplited = sPair.Split("_")
                            Leg.Coin1 = PairSplited(0)
                            Leg.Coin2 = PairSplited(1)
                            'busca na lista de markets master para ambas exchanges
                            Dim Pair1Ida = GetPairPriceObject(sCoin1, sCoin2, Round.Exchange1)
                            Dim Pair1Volta = GetPairPriceObject(sCoin2, sCoin1, Round.Exchange1)
                            Dim Pair2Ida = GetPairPriceObject(sCoin1, sCoin2, Round.Exchange2)
                            Dim Pair2Volta = GetPairPriceObject(sCoin2, sCoin1, Round.Exchange2)

                            'se o ida for igual, usa ele
                            If Pair1Ida.MarketName.ToUpper() = Pair2Ida.MarketName.ToUpper() And
                                Pair1Ida.MarketNameInverted = Pair2Ida.MarketNameInverted Then
                                Leg.PairPricesExchange1 = Pair1Ida
                                Leg.PairPricesExchange2 = Pair2Ida
                                'se o volta foi igual, usa ele
                            ElseIf Pair1Volta.MarketName.ToUpper() = Pair2Volta.MarketName.ToUpper() And
                                Pair1Volta.MarketNameInverted = Pair2Volta.MarketNameInverted Then
                                Leg.PairPricesExchange1 = Pair1Volta
                                Leg.PairPricesExchange2 = Pair2Volta
                            Else

                                Leg.Valid = False
                                Leg.InvalidReason = "Markets incompatibles."
                                'Leg.InvalidReason = "[Exchange1 Message: " + Leg.PairPricesExchange1.InvalidReason + "] - [Exchange2 Message: " + Leg.PairPricesExchange2.InvalidReason
                            End If
                            'ultima checada se o pair é válido para o par Ex1
                            If Leg.Valid And (Not Leg.PairPricesExchange1 Is Nothing) Then
                                If Not Leg.PairPricesExchange1.Valid Then
                                    Leg.Valid = False
                                    Leg.InvalidReason = "PairPricesEx1 is invalid. [Message: " + Leg.PairPricesExchange1.InvalidReason + "]"
                                End If
                            End If
                            'ultima checada se o pair é válido para o par Ex2
                            If (Not Leg.PairPricesExchange2 Is Nothing) Then
                                If Not Leg.PairPricesExchange2.Valid Then
                                    Leg.Valid = False
                                    Leg.InvalidReason = Leg.InvalidReason + " - PairPricesEx2 is invalid. [Message: " + Leg.PairPricesExchange2.InvalidReason + "]"
                                End If
                            End If


                            Round.Legs.Add(Leg)
                        Next
                    Next
                Next
            Next
        Next
    End Sub


    Private Sub InitializeConfigOportunitiesIntra(ByRef pConfigOportunities As bvxConfigOportunities)
        'LoadExchangesFromJsonFile(pConfigOportunities)
        LogService.Write("Starting intra configuration.", LogService.Type.INFO)
        'Intra rounds
        For Each oArbitIntra As bvxArbitIntra In pConfigOportunities.Intra
            InitializeIntra(oArbitIntra)
        Next
    End Sub


    'create objects on the rounds level
    Private Sub InitializeIntra(ByRef pArbitIntra As bvxArbitIntra)
        RaiseMessage("Starting intra configuration for Exchange: " + pArbitIntra.Exchange, 3)
        LogService.Write("Exchange: " + pArbitIntra.Exchange, LogService.Type.INFO)
        'recupera somente o ponteiro do objeto que já existe na lista master
        pArbitIntra.ObjExchange = GetExchangeByNameFromMasterList(pArbitIntra.Exchange)
        'InitializeFeeList(oArbitIntra)
        Dim Rounds = pArbitIntra.MarketRounds_Text.Count
        Dim RoundActual = 1
        For Each s As String In pArbitIntra.MarketRounds_Text
            Dim oArbitIntraRound = New bvxArtbitIntraRound(pArbitIntra)
            'LogService.Write("Loading round: " + s, LogService.Type.INFO)
            oArbitIntraRound.Coins = s
            If RoundActual Mod 100 = 0 Then
                RaiseMessage("Loading round: " + RoundActual.ToString + " of " + Rounds.ToString, 3)
            End If
            pArbitIntra.Rounds.Add(oArbitIntraRound)
            InitializeIntraRound(oArbitIntraRound)
            RoundActual = RoundActual + 1
        Next
    End Sub


    'retorna o ponteiro do objeto já criado em memória, com API e MarketList carregada
    Private Function GetExchangeByNameFromMasterList(pExchangeName As String) As bvxExchange
        Dim obj = ConfigOportunitiesSingleton.Instance.Exchanges.Where(Function(x) x.Name = pExchangeName).FirstOrDefault
        Return obj
    End Function


    Private Function GetExchangeByNameFromDb(pExchangeName As String, pPersistInDatabase As Boolean) As bvxExchange
        Dim obj As bvxExchange = Nothing
        'If ConfigOportunitiesSingleton.Instance.PersistInDatabase Then
        If pPersistInDatabase Then
            Dim Dao As OportunityDAO = New OportunityDAO()
            obj = Dao.GetExchangeByName(pExchangeName)
        Else
            Dim x = ExchangesSingleton.Instance
            obj = x.GetExchangeByName(pExchangeName)
        End If
        Return obj
    End Function


    'create objects on the legs level
    Private Sub InitializeIntraRound(ByRef pArbitIntraRound As bvxArtbitIntraRound)
        Try
            Dim ListCoins As String() = pArbitIntraRound.Coins.Split(",")
            Dim ObjExchange = pArbitIntraRound.Parent.ObjExchange
            'só monta se tiver mais de dois
            If ListCoins.Count > 2 Then
                For i As Integer = 0 To ListCoins.Count - 1
                    'tirei para testar, deixando os rounds inválidos serem gerados, mesmo que já tenha um inválido
                    'If Not pArbitIntraRound.Valid Then
                    '    Exit For
                    'End If
                    Dim oArbitIntraRoundLeg = New bvxArbitIntraRoundLeg(pArbitIntraRound)
                    Dim Coin1 As String = ListCoins(i).Trim
                    Dim Coin2 As String

                    If i < ListCoins.Count - 1 Then
                        Coin2 = ListCoins(i + 1).Trim
                    Else 'é a última, monta o último com o primeiro
                        Coin2 = ListCoins(0).Trim
                    End If
                    'cria o objeto pairprice ou usa um existente
                    'os objetos pairprice ficam na lista da exchange. 
                    'nas legs tem somente um apontamento para a lista master
                    oArbitIntraRoundLeg.PairPrices = GetPairPriceObject(Coin1, Coin2, ObjExchange)
                    'seta invalido no round, caso alguma leg seja inválida
                    'mas adiciona ela mesmo assim, tanto na lista master quanto na lista dos rounds
                    If Not oArbitIntraRoundLeg.PairPrices.Valid Then
                        pArbitIntraRound.Valid = False
                        pArbitIntraRound.InvalidReason = oArbitIntraRoundLeg.PairPrices.InvalidReason
                    End If

                    pArbitIntraRound.Legs.Add(oArbitIntraRoundLeg)
                    ''adiciona a uma cópia da leg na lista master, caso não exista para este mercado ainda
                    'CheckAndAddLegInMasterList(oArbitIntraRoundLeg)
                Next
            End If
        Catch ex As Exception

            Throw
        End Try

    End Sub


    'Private Sub CheckAndAddLegInMasterList(ByRef pLeg As bvxArtbitIntraRoundLeg)

    '    '***TODO***ver se funciona o parent no byref
    '    Dim oIntra = pLeg.Parent.Parent
    '    Dim sMarket As String = pLeg.PairPrices.MarketName
    '    If oIntra.MarketLegMaster.Where(Function(x) x.PairPrices.MarketName.ToUpper() = sMarket.ToUpper()).Count = 0 Then
    '        Dim oNewLeg = pLeg.GetClone(pLeg.Parent)
    '        oIntra.MarketLegMaster.Add(oNewLeg)
    '    End If

    'End Sub


    ''load market list from api (web) for each exchange
    'Private Sub InitializeFeeList(ByRef pArbitIntra As bvxArbitIntra)
    '    Try
    '        pArbitIntra.FeeList = pArbitIntra.API.GetFeeList()

    '    Catch ex As Exception
    '        Dim s = "***Error*** bvxArbitrageController.InitializeMarketList. [Exchange: " + pArbitIntra.Exchange + "] "
    '        LogService.Write(s, LogService.Type.FAILURE)

    '        pArbitIntra.Valid = False
    '        If ex.Message.Contains("503") Then
    '            pArbitIntra.InvalidReason = "503"
    '        End If
    '    End Try

    'End Sub


    'load market list from api (web) for each exchange
    Private Sub InitializeExchangeMarketList(ByRef pExchange As bvxExchange)
        Try
            pExchange.MarketList = pExchange.API.GetMarketNameList()
        Catch ex As Exception
            Dim s = "***Error*** bvxArbitrageController.InitializeMarketList. [Exchange: " + pExchange.Name + "] "
            LogService.Write(s, LogService.Type.FAILURE)
            pExchange.Valid = False
            If ex.Message.Contains("503") Then
                pExchange.InvalidReason = "503"
            End If
        End Try
    End Sub


    'retorna um objeto pair price para a exchange existente ou cria um novo se a combinação ainda não existir
    Private Function GetPairPriceObject(ByVal pCoin1 As String, pCoin2 As String, ByVal pExchange As bvxExchange) As bvxPairPrices

        Try
            Dim Ida As String = pCoin1 + "_" + pCoin2
            Dim Volta As String = pCoin2 + "_" + pCoin1
            Dim newPairPrices = New bvxPairPrices(pExchange)
            newPairPrices.Coin1 = pCoin1
            newPairPrices.Coin2 = pCoin2
            Dim i As Integer = pExchange.MarketList.Select(Function(s) s.ToUpper()).ToList().IndexOf(Ida.ToUpper())
            If i > -1 Then
                'pega o valor como está na lista, para evitar problemas de case sensitive na chamada da api
                newPairPrices.MarketName = pExchange.MarketList(i)
                newPairPrices.BaseCoin = newPairPrices.Coin2
                newPairPrices.MarketNameInverted = False
            Else
                i = pExchange.MarketList.Select(Function(s) s.ToUpper()).ToList().IndexOf(Volta.ToUpper())
                If i > -1 Then
                    'pArbitIntraRoundLeg.MarketName = Volta
                    newPairPrices.MarketName = pExchange.MarketList(i)
                    newPairPrices.BaseCoin = newPairPrices.Coin1
                    newPairPrices.MarketNameInverted = True
                Else
                    Dim s = "***Error*** Leg cannot be created because theres no market with this coins in this Exchange. [Exchange: " +
                        pExchange.Name + "][Coin1: " + newPairPrices.Coin1 +
                        "][Coin2: " + newPairPrices.Coin2 + "]"
                    LogService.Write(s, LogService.Type.FAILURE)
                    newPairPrices.MarketName = "Invalid"
                    newPairPrices.BaseCoin = "Invalid"
                    newPairPrices.Valid = False
                    newPairPrices.InvalidReason = s
                End If
            End If
            'se existe na lista master, usa o que existe, senao adiciona o novo e retorna o ponteiro
            Dim existingPairPrices As bvxPairPrices = pExchange.PairListMaster.Where(
                Function(x) x.MarketName.ToUpper = newPairPrices.MarketName.ToUpper() And
                x.MarketNameInverted = newPairPrices.MarketNameInverted).FirstOrDefault
            If existingPairPrices Is Nothing Then
                pExchange.PairListMaster.Add(newPairPrices)
                Return newPairPrices
            Else
                Return existingPairPrices
            End If
        Catch ex As Exception
            Throw
        End Try

    End Function

#End Region


#Region "Refreshing Oportunities"

    'Public Sub RefreshIntraOportunities()

    '    Dim pConfigOportunities = ConfigOportunitiesSingleton.Instance
    '    Try

    '        If pConfigOportunities.State = bvxConfigOportunities.bvxConfigOportunitiesState.Ready Then
    '            pConfigOportunities.State = bvxConfigOportunities.bvxConfigOportunitiesState.Updating
    '            RaiseMessage("Refreshing market values...", 3)

    '            RefreshRounds(pConfigOportunities)
    '            RaiseMessage("Calculating oportunities.", 3)
    '            CalculateIntraOportunities2(pConfigOportunities)
    '            IdentifyOportunities(pConfigOportunities)

    '            RaiseMessage("Object refrehed.", 3)
    '            pConfigOportunities.State = bvxConfigOportunities.bvxConfigOportunitiesState.Ready
    '            RaiseRefreshFinished(pConfigOportunities)
    '        End If
    '    Catch ex As Exception
    '        LogService.Write("Error on RefreshOportunities: " + ex.ToString(), LogService.Type.FATAL)
    '        pConfigOportunities.State = bvxConfigOportunities.bvxConfigOportunitiesState.Ready
    '        RaiseMessage("Finished with errors.", 2)
    '        RaiseRefreshFinished(pConfigOportunities)
    '    End Try

    'End Sub


    Public Sub RefreshIntraAndBetweenOportunities(pCalculateWithRefresh As Boolean)

        Dim pConfigOportunities = ConfigOportunitiesSingleton.Instance
        Try
            If pConfigOportunities.State = bvxConfigOportunities.bvxConfigOportunitiesState.Ready Then
                pConfigOportunities.State = bvxConfigOportunities.bvxConfigOportunitiesState.Updating
                'utilizado na fase de testes dos cálculos para ser mais rápido
                If pCalculateWithRefresh Then
                    RaiseMessage("Refreshing market values...", 3)
                    RefreshExchangesPairPrices(pConfigOportunities)
                End If

                RaiseMessage("Calculating oportunities.", 3)
                CalculateIntraOportunities2(pConfigOportunities)
                CalculateBetweenOportunities(pConfigOportunities)

                IdentifyOportunities(pConfigOportunities)

                RaiseMessage("Object refrehed.", 3)
                pConfigOportunities.State = bvxConfigOportunities.bvxConfigOportunitiesState.Ready
                RaiseRefreshFinished(pConfigOportunities)
            End If
        Catch ex As Exception
            LogService.Write("Error on RefreshOportunities: " + ex.ToString(), LogService.Type.FATAL)
            pConfigOportunities.State = bvxConfigOportunities.bvxConfigOportunitiesState.Ready
            RaiseMessage("Finished with errors.", 2)
            RaiseRefreshFinished(pConfigOportunities)
        End Try

    End Sub


    Private Sub ClearExchangesPairPrices(ByRef pConfigOportunities As bvxConfigOportunities)
        'limpa os valores de preço e mercado da lista MarketMaster
        For Each oExchange As bvxExchange In pConfigOportunities.Exchanges
            For Each objPair As bvxPairPrices In oExchange.PairListMaster
                objPair.ResetAskAndBidValues(bvxThreadUpdatableObject.bvxUpdatableObjectStateList.Empty)
            Next
        Next
    End Sub

    'Private Sub ClearAskAndBidMaster(ByRef pConfigOportunities As bvxConfigOportunities)
    '    'limpa os valores de preço e mercado da lista MarketMaster
    '    For Each oArbitIntra As bvxArbitIntra In pConfigOportunities.Intra
    '        For Each objLegMaster As bvxArtbitIntraRoundLeg In oArbitIntra.MarketLegMaster
    '            objLegMaster.ResetAskAndBidValues(bvxThreadUpdatableObject.bvxUpdatableObjectStateList.Empty)
    '        Next
    '    Next
    'End Sub

    'Private Sub SpreadMasterToLegs(ByRef pConfigOportunities As bvxConfigOportunities)

    '    Dim SomeValueEmpty As Boolean
    '    'Propaga preço e valores da lista MarketMaster para as Legs
    '    For Each oArbitIntra As bvxArbitIntra In pConfigOportunities.Intra

    '        If Not oArbitIntra.Valid Then
    '            Continue For
    '        End If
    '        For Each oRound In oArbitIntra.Rounds

    '            SomeValueEmpty = False
    '            For Each oLeg In oRound.Legs
    '                Dim oMaster As bvxArtbitIntraRoundLeg = oArbitIntra.MarketLegMaster.Where(Function(x) x.PairPrices.MarketName = oLeg.PairPrices.MarketName).First
    '                If Not oMaster Is Nothing Then
    '                    If oMaster.PairPrices.Ask <= 0 Or oMaster.PairPrices.Bid <= 0 Or oMaster.PairPrices.AskVolume <= 0 Or oMaster.PairPrices.BidVolume <= 0 Then
    '                        oLeg.SetAskAndBidValues(oMaster.PairPrices.Ask, oMaster.PairPrices.Bid, oMaster.PairPrices.AskVolume, oMaster.PairPrices.BidVolume,
    '                                            bvxThreadUpdatableObject.bvxUpdatableObjectStateList.Empty)
    '                        SomeValueEmpty = True
    '                    Else
    '                        oLeg.SetAskAndBidValues(oMaster.PairPrices.Ask, oMaster.PairPrices.Bid, oMaster.PairPrices.AskVolume, oMaster.PairPrices.BidVolume,
    '                                            bvxThreadUpdatableObject.bvxUpdatableObjectStateList.UpToDate)
    '                    End If
    '                End If
    '            Next
    '            If SomeValueEmpty Then
    '                oRound.SetUpdateState(bvxThreadUpdatableObject.bvxUpdatableObjectStateList.Empty)
    '            Else
    '                oRound.SetUpdateState(bvxThreadUpdatableObject.bvxUpdatableObjectStateList.UpToDate)
    '            End If
    '        Next
    '    Next
    'End Sub


    Public Sub RefreshExchangesPairPrices(ByRef pConfigOportunities As bvxConfigOportunities)

        ClearExchangesPairPrices(pConfigOportunities)
        LogService.Write("RefreshRounds: Starting requests for ASK, BID and VOLUME values.", LogService.Type.INFO)
        Dim TimeIni As DateTime = Now

        For Each objExchange In pConfigOportunities.Exchanges
            If Not objExchange.Valid Then
                Continue For
            End If
            RaiseMessage("Refreshing Exchange: " + objExchange.Name, 2)
            For Each objPair In objExchange.PairListMaster
                Try
                    RaiseMessage("Pair: " + objPair.MarketName, 3)
                    objPair.SetAskAndBidValues(objExchange.API.GetAskBidForMarket(objPair.MarketName))
                Catch ex As Exception
                    objPair.ResetAskAndBidValues(bvxThreadUpdatableObject.bvxUpdatableObjectStateList.ErrorResponse)
                    LogService.Write("***Error in RefreshRounds. [Exchange: " + objExchange.Name +
                                     "][Market: " + objPair.MarketName + "]" +
                                     Environment.NewLine + ex.ToString(), LogService.Type.FAILURE)
                End Try
            Next
        Next

        Dim TimeEnd As DateTime = Now
        Dim EllapsedTime = TimeEnd - TimeIni
        LogService.Write("RefreshRounds: Finished requests in: " + EllapsedTime.Seconds.ToString() + " seconds.", LogService.Type.INFO)
        'SpreadMasterToLegs(pConfigOportunities)

    End Sub

    'refresh book orders and check oportunities
    'Public Sub RefreshRounds(ByRef pConfigOportunities As bvxConfigOportunities)

    '    ClearAskAndBidMaster(pConfigOportunities)
    '    LogService.Write("RefreshRounds: Starting requests for ASK, BID and VOLUME values.", LogService.Type.INFO)
    '    Dim TimeIni As DateTime = Now
    '    'carrega preço e valores na lista MarketMaster
    '    For Each oArbitIntra As bvxArbitIntra In pConfigOportunities.Intra
    '        If Not oArbitIntra.Valid Then
    '            Continue For
    '        End If
    '        For Each objLegMaster As bvxArtbitIntraRoundLeg In oArbitIntra.MarketLegMaster
    '            Try
    '                objLegMaster.SetAskAndBidValues(oArbitIntra.ObjExchange.API.GetAskBidForMarket(objLegMaster.PairPrices.MarketName))
    '            Catch ex As Exception
    '                objLegMaster.ResetAskAndBidValues(bvxThreadUpdatableObject.bvxUpdatableObjectStateList.ErrorResponse)
    '                LogService.Write("***Error in RefreshRounds. [Exchange: " + oArbitIntra.Exchange +
    '                                 "][Market: " + objLegMaster.PairPrices.MarketName + "]" +
    '                                 Environment.NewLine + ex.ToString(), LogService.Type.FAILURE)
    '            End Try
    '        Next
    '    Next
    '    Dim TimeEnd As DateTime = Now
    '    Dim EllapsedTime = TimeEnd - TimeIni
    '    LogService.Write("RefreshRounds: Finished requests in: " + EllapsedTime.Seconds.ToString() + " seconds.", LogService.Type.INFO)
    '    SpreadMasterToLegs(pConfigOportunities)


    'End Sub


    Public Sub IdentifyIntraOportunities(ByRef pConfigOportunities As bvxConfigOportunities)

        For Each oArbitIntra As bvxArbitIntra In pConfigOportunities.Intra

            ClearIntraOportunityList(oArbitIntra)
            For Each objRound As bvxArtbitIntraRound In oArbitIntra.Rounds
                Try
                    If objRound.Rate > oArbitIntra.OportunityPercent Then
                        objRound.IsOportunity = True
                        objRound.SetUpdateState(bvxThreadUpdatableObject.bvxUpdatableObjectStateList.UpToDate)
                        oArbitIntra.HasOportunity = True

                        'If ConfigOportunitiesSingleton.Instance.PersistInDatabase Then
                        '    PerssistOportunityIntra(objRound)
                        'End If
                    End If
                Catch ex As Exception
                    LogService.Write("***Error in IdentifyIntraOportunities. [Exchange: " + oArbitIntra.ObjExchange.Name +
                                    "][Round Coins: " + objRound.Coins + "]" +
                                    Environment.NewLine + ex.ToString(), LogService.Type.FAILURE)
                    Throw
                End Try
            Next

        Next


    End Sub





    Public Sub IdentifyBetweenOportunities(ByRef pConfigOportunities As bvxConfigOportunities)
        For Each oRound As bvxArbitBetweenRound In pConfigOportunities.BetweenRounds
            ClearBetweenOportunityList(oRound)
            For Each objLeg In oRound.Legs
                If (objLeg.Rate1To2 > pConfigOportunities.BetweenOportunityPercent Or
                    objLeg.Rate2To1 > pConfigOportunities.BetweenOportunityPercent) Then
                    objLeg.IsOportunity = True
                    objLeg.SetUpdateState(bvxThreadUpdatableObject.bvxUpdatableObjectStateList.UpToDate)
                    oRound.HasOportunity = True
                End If
            Next
            oRound.SetUpdateState(bvxThreadUpdatableObject.bvxUpdatableObjectStateList.UpToDate)
            'If ConfigOportunitiesSingleton.Instance.PersistInDatabase Then
            '    PerssistOportunityBetween(oRound)
            'End If
        Next
    End Sub


    Public Sub IdentifyOportunities(ByRef pConfigOportunities As bvxConfigOportunities)
        IdentifyIntraOportunities(pConfigOportunities)
        IdentifyBetweenOportunities(pConfigOportunities)
        CalculateLowerVolume(pConfigOportunities)
        PerssistOportunities(pConfigOportunities)
    End Sub


    'calcula o menor volume para intra e entre
    Private Sub CalculateLowerVolume(ByRef pConfigOportunities As bvxConfigOportunities)

        CalculateIntraLowerVolume(pConfigOportunities)
        CalculateBetweenLowerVolume(pConfigOportunities)

    End Sub


    Private Sub CalculateIntraLowerVolume(ByRef pConfigOportunities As bvxConfigOportunities)
        For Each oIntra In pConfigOportunities.Intra
            For Each oRound In oIntra.Rounds
                'calcula somente se estiver marcado como oportunidade, para evitar processamento desnecessário
                'If oRound.IsOportunity Then
                Dim arrBidAsk(oRound.Legs.Count - 1) As Double
                    Dim arrVK(oRound.Legs.Count - 1) As Double
                    Dim arrVolume(oRound.Legs.Count - 1) As Double
                    Dim arrVolumeFee(oRound.Legs.Count - 1) As Double
                    Dim bidaskanteriores As Double = 1
                    For i As Integer = 0 To oRound.Legs.Count - 1
                        arrBidAsk(i) = IIf(oRound.Legs(i).PairPrices.MarketNameInverted,
                                           oRound.Legs(i).PairPrices.Ask,
                                           1 / oRound.Legs(i).PairPrices.Bid)
                        arrVK(i) = IIf(oRound.Legs(i).PairPrices.MarketNameInverted,
                                       oRound.Legs(i).PairPrices.Ask * oRound.Legs(i).PairPrices.AskVolume,
                                       oRound.Legs(i).PairPrices.BidVolume)
                        arrVolume(i) = arrVK(i) * bidaskanteriores
                        bidaskanteriores = bidaskanteriores * arrBidAsk(i)
                        arrVolumeFee(i) = arrVolume(i) / Math.Pow((1 - oIntra.Fee / 100), i)
                        'campos para testes
                        oRound.Legs(i).TesteBidAsk = arrBidAsk(i)
                        oRound.Legs(i).TesteVK = arrVK(i)
                        oRound.Legs(i).TesteVolume = arrVolume(i)
                        oRound.Legs(i).TesteVolumeWithFee = arrVolumeFee(i)
                    Next
                    oRound.LowerVolume = arrVolumeFee.Min
                    oRound.LowerVolumeLegIndex = Array.IndexOf(arrVolumeFee, oRound.LowerVolume)

                '  End If
            Next
        Next
    End Sub


    Private Sub CalculateBetweenLowerVolume(ByRef pConfigOportunities As bvxConfigOportunities)
        For Each oRound In pConfigOportunities.BetweenRounds
            For Each oLeg In oRound.Legs
                Dim VolumeEx1 As Double = IIf(oLeg.PairPricesExchange1.MarketNameInverted,
                                              oLeg.PairPricesExchange1.Ask * oLeg.PairPricesExchange1.AskVolume,
                                              oLeg.PairPricesExchange1.BidVolume)
                Dim VolumeEx2 As Double = IIf(oLeg.PairPricesExchange2.MarketNameInverted,
                                              oLeg.PairPricesExchange2.Ask * oLeg.PairPricesExchange2.AskVolume,
                                              oLeg.PairPricesExchange2.BidVolume)
                oLeg.LowerVolume = IIf(VolumeEx1 <= VolumeEx2, VolumeEx1, VolumeEx2)
                oLeg.LowerVolumeExchange = IIf(VolumeEx1 <= VolumeEx2, 1, 2)
            Next
        Next
    End Sub


    Private Sub ClearIntraOportunityList(ByRef pArbitIntra As bvxArbitIntra)

        pArbitIntra.HasOportunity = False
        For Each oRound In pArbitIntra.Rounds
            oRound.IsOportunity = False
        Next

    End Sub


    Private Sub ClearBetweenOportunityList(ByRef pArbitBetweenRound As bvxArbitBetweenRound)

        pArbitBetweenRound.HasOportunity = False
        For Each oLeg In pArbitBetweenRound.Legs
            oLeg.IsOportunity = False
        Next

    End Sub


    'Private Sub AddOportunity(ByRef pArbitIntra As bvxArbitIntra, ByVal pRound As bvxArtbitIntraRound)

    '    pArbitIntra.RoundsOportunities.Add(pRound.GetClone())

    'End Sub




    ''antigo, em desuso
    'Public Function CalculateIntraOportunities(ByRef pConfigOportunities As bvxConfigOportunities) As List(Of String)
    '    Dim list As List(Of String) = New List(Of String)
    '    LogService.Write("Starting CalculateIntraOportunities", LogService.Type.INFO)
    '    For Each oArbitIntra As bvxArbitIntra In pConfigOportunities.Intra
    '        If Not oArbitIntra.Valid Then
    '            LogService.Write("Exchange: [" + oArbitIntra.Exchange + "] is invalid. Could not calculate.", LogService.Type.INFO)
    '            Continue For
    '        End If
    '        LogService.Write("Calculating exchange: [" + oArbitIntra.Exchange + "].", LogService.Type.INFO)
    '        For Each oRound In oArbitIntra.Rounds
    '            Dim Rate As Double
    '            Dim Log As String = ""
    '            Dim obj0 = oRound.Legs(0)
    '            Dim obj1 = oRound.Legs(1)
    '            Dim obj2 = oRound.Legs(2)
    '            Rate = IIf(obj1.MarketNameInverted, obj1.Ask, obj1.Bid) / IIf(obj0.MarketNameInverted, obj0.Ask, obj0.Bid)
    '            Rate = IIf(obj2.MarketNameInverted, Rate / obj2.Ask, Rate * obj2.Bid)
    '            Log = IIf(obj1.MarketNameInverted, "(0:ASK: (" + obj1.Ask.ToString(), ") 0:BID: (" + obj1.Bid.ToString() + ")") +
    '                ") / " +
    '                IIf(obj0.MarketNameInverted, "1:ASK: (" + obj0.Ask.ToString(), "1:BID: (" + obj0.Bid.ToString()) + ")" +
    '                IIf(obj2.MarketNameInverted, " / (2:ASK: (" + obj2.Ask.ToString() + ")", " * (2:BID: (" + obj2.Bid.ToString() + ")") + ")"
    '            Log = Log + " : Rate = " + Rate.ToString()
    '            oRound.Rate = Rate
    '            oRound.LogOperation = Log
    '            list.Add(Log)
    '            LogService.Write("Rate: [" + oRound.Rate.ToString() + "] Round: " + oRound.Coins, LogService.Type.INFO)
    '        Next
    '    Next
    '    Return list
    'End Function

    'o calculo de um round é feito sempre com pares de legs, neste par, chamamos sempre de ActualLeg e NextLeg
    'cada leg determina se usa ASK ou BID pelo flag MarketNameInverted (Y=ASK, N=BID)
    'o cálculo de legs é feito com / ou *. Quem determina a operação é o flag do NextLeg (Y=/, N=*)
    'Na primeira interação, caso o do ActualLeg = Y, então dividir o valor de seu ASK por 1 antes do início. Isso vale somente para a primeira interação
    '


    Public Function CalculateIntraOportunities2(ByRef pConfigOportunities As bvxConfigOportunities) As List(Of String)
        Dim list As List(Of String) = New List(Of String)

        LogService.Write("Starting CalculateIntraOportunities", LogService.Type.INFO)
        For Each oArbitIntra As bvxArbitIntra In pConfigOportunities.Intra
            If Not oArbitIntra.Valid Then
                LogService.Write("Exchange: [" + oArbitIntra.Exchange + "] is invalid. Could not calculate.", LogService.Type.INFO)
                Continue For
            End If
            LogService.Write("Calculating Intra Exchange: [" + oArbitIntra.Exchange + "].", LogService.Type.INFO)
            Dim Fee As Double = oArbitIntra.Fee
            For Each oRound In oArbitIntra.Rounds
                If oRound.GetActualUpdateStatus = bvxThreadUpdatableObject.bvxUpdatableObjectStateList.UpToDate Then
                    Dim CoinRate As String = ""
                    'Dim FeeActual As Double = 0
                    'Dim FeeNext As Double = 0

                    Dim Rate As Double = 0
                    Dim RateNoFee As Double = 0
                    Dim LogA As String = ""
                    Dim LogB As String = ""
                    Dim LogANoFee As String = ""
                    Dim LogBNoFee As String = ""
                    Dim ActualLeg As bvxArbitIntraRoundLeg
                    Dim NextLeg As bvxArbitIntraRoundLeg
                    Dim First As Boolean = True
                    For i As Integer = 0 To oRound.Legs.Count - 2
                        ActualLeg = oRound.Legs(i)
                        NextLeg = oRound.Legs(i + 1)

                        'FeeActual = oArbitIntra.FeeList.Where(Function(x) x.Coin = ActualLeg.Coin1).FirstOrDefault.Fee
                        'FeeNext = oArbitIntra.FeeList.Where(Function(x) x.Coin = NextLeg.Coin1).FirstOrDefault.Fee

                        Dim ValActual As Double
                        'teste do 1 / ask, documentar se funcionar
                        ValActual = IIf(ActualLeg.PairPrices.MarketNameInverted, ActualLeg.PairPrices.Ask, ActualLeg.PairPrices.Bid)
                        Dim ValNext As Double = IIf(NextLeg.PairPrices.MarketNameInverted, NextLeg.PairPrices.Ask, NextLeg.PairPrices.Bid)
                        Dim ValActualName As String = IIf(ActualLeg.PairPrices.MarketNameInverted, ActualLeg.PairPrices.MarketName + " (ASK) ", ActualLeg.PairPrices.MarketName + " (BID) ")
                        Dim ValNextName As String = IIf(NextLeg.PairPrices.MarketNameInverted, NextLeg.PairPrices.MarketName + " (ASK) ", NextLeg.PairPrices.MarketName + " (BID) ")
                        If First Then
                            'na primeira interação calcula o fee 2 vezes. A primeira para a Leg1 e a segunda para a Leg2
                            If ActualLeg.PairPrices.MarketNameInverted Then
                                'Rate = 1 / ValActual * (1 - FeeActual / 100)
                                Rate = 1 / ValActual * (1 - Fee / 100)
                                Rate = IIf(NextLeg.PairPrices.MarketNameInverted, Rate / ValNext, Rate * ValNext)
                                'Rate = Rate * (1 - FeeNext / 100)
                                Rate = Rate * (1 - Fee / 100)
                                LogA = IIf(NextLeg.PairPrices.MarketNameInverted, "1 / " + ValActual.ToString() + " / " + ValNext.ToString(), "1 / " + ValActual.ToString() + " * " + ValNext.ToString())
                                LogB = IIf(NextLeg.PairPrices.MarketNameInverted, "1 / " + ValActualName + " / " + ValNextName, "1 / " + ValActualName + " * " + ValNextName)
                            Else
                                'Rate = 1 * ValActual * (1 - FeeActual / 100)
                                Rate = 1 * ValActual * (1 - Fee / 100)
                                Rate = IIf(NextLeg.PairPrices.MarketNameInverted, Rate / ValNext, Rate * ValNext)
                                'Rate = Rate * (1 - FeeNext / 100)
                                Rate = Rate * (1 - Fee / 100)
                                LogA = IIf(NextLeg.PairPrices.MarketNameInverted, ValActual.ToString() + " / " + ValNext.ToString(), ValActual.ToString() + " * " + ValNext.ToString())
                                LogB = IIf(NextLeg.PairPrices.MarketNameInverted, ValActualName + " / " + ValNextName, ValActualName + " * " + ValNextName)
                            End If
                        Else
                            Rate = IIf(NextLeg.PairPrices.MarketNameInverted, Rate / ValNext, Rate * ValNext)
                            'Rate = Rate * (1 - FeeNext / 100)
                            Rate = Rate * (1 - Fee / 100)
                            LogA = LogA + " || " + IIf(NextLeg.PairPrices.MarketNameInverted, Rate.ToString() + " / " + ValNext.ToString(), Rate.ToString() + " * " + ValNext.ToString())
                            LogB = LogB + " || " + IIf(NextLeg.PairPrices.MarketNameInverted, "RATE / " + ValNextName, "RATE  * " + ValNextName)
                        End If

                        'backup antes da fee
                        'rate no fee... só durante os testes
                        '******************
                        If First Then
                            If ActualLeg.PairPrices.MarketNameInverted Then
                                RateNoFee = IIf(NextLeg.PairPrices.MarketNameInverted, 1 / ValActual / ValNext, 1 / ValActual * ValNext)
                                LogANoFee = IIf(NextLeg.PairPrices.MarketNameInverted, "1 / " + ValActual.ToString() + " / " + ValNext.ToString(), "1 / " + ValActual.ToString() + " * " + ValNext.ToString())
                                LogBNoFee = IIf(NextLeg.PairPrices.MarketNameInverted, "1 / " + ValActualName + " / " + ValNextName, "1 / " + ValActualName + " * " + ValNextName)
                            Else
                                RateNoFee = IIf(NextLeg.PairPrices.MarketNameInverted, ValActual / ValNext, ValActual * ValNext)
                                LogANoFee = IIf(NextLeg.PairPrices.MarketNameInverted, ValActual.ToString() + " / " + ValNext.ToString(), ValActual.ToString() + " * " + ValNext.ToString())
                                LogBNoFee = IIf(NextLeg.PairPrices.MarketNameInverted, ValActualName + " / " + ValNextName, ValActualName + " * " + ValNextName)
                            End If
                        Else
                            RateNoFee = IIf(NextLeg.PairPrices.MarketNameInverted, RateNoFee / ValNext, RateNoFee * ValNext)
                            LogANoFee = LogANoFee + " || " + IIf(NextLeg.PairPrices.MarketNameInverted, RateNoFee.ToString() + " / " + ValNext.ToString(), RateNoFee.ToString() + " * " + ValNext.ToString())
                            LogBNoFee = LogBNoFee + " || " + IIf(NextLeg.PairPrices.MarketNameInverted, "RATE / " + ValNextName, "RATE  * " + ValNextName)
                        End If
                        '******************
                        First = False
                    Next 'legs
                    oRound.Rate = Rate
                    oRound.RateNoFee = RateNoFee
                    oRound.LogOperation = LogA + "  ||  || " + LogB
                    list.Add(oRound.LogOperation)
                    LogService.Write("Rate: [" + oRound.Rate.ToString() + "] Round: " + oRound.Coins, LogService.Type.INFO)
                End If

            Next
        Next
        Return list

    End Function


    Public Sub CalculateBetweenOportunities(ByRef pConfigOportunities As bvxConfigOportunities)
        Dim list As List(Of String) = New List(Of String)

        LogService.Write("Starting CalculateBetweenOportunities", LogService.Type.INFO)
        For Each oArbitBetweenRound As bvxArbitBetweenRound In pConfigOportunities.BetweenRounds
            If Not oArbitBetweenRound.Valid Then
                LogService.Write("Exchanges: [" + oArbitBetweenRound.Exchange1.Name + " and " + oArbitBetweenRound.Exchange2.Name + "] is invalid. Could not calculate.", LogService.Type.INFO)
                Continue For
            End If
            LogService.Write("Calculating Between: [" + oArbitBetweenRound.Exchange1.Name + " and " + oArbitBetweenRound.Exchange2.Name + "].", LogService.Type.INFO)
            For Each oLeg In oArbitBetweenRound.Legs
                oLeg.Rate2To1 = oLeg.PairPricesExchange1.Bid / oLeg.PairPricesExchange2.Ask
                oLeg.Rate1To2 = oLeg.PairPricesExchange2.Bid / oLeg.PairPricesExchange1.Ask
                oLeg.LogOperation = String.Format("[Rate2To1: {0}][Rate1To2: {1}]",
                    oLeg.PairPricesExchange1.Bid.ToString + " / " + oLeg.PairPricesExchange2.Ask.ToString + " = " + oLeg.Rate1To2.ToString +
                        " (BID " + oArbitBetweenRound.Exchange1.Name + " / ASK " + oArbitBetweenRound.Exchange2.Name + ") ",
                    oLeg.PairPricesExchange2.Bid.ToString + " / " + oLeg.PairPricesExchange1.Ask.ToString + " = " + oLeg.Rate2To1.ToString +
                        " (BID " + oArbitBetweenRound.Exchange2.Name + " / ASK " + oArbitBetweenRound.Exchange1.Name + ") ")
            Next
        Next


    End Sub

#End Region



#Region "Results"


    Public Function GetLegsList(ByVal pRound As bvxArtbitIntraRound)

        If Not pRound Is Nothing Then
            Dim x = From T1 In pRound.Legs.AsEnumerable
                    Select T1.PairPrices.Coin1, T1.PairPrices.Coin2, T1.PairPrices.MarketName,
                        T1.PairPrices.MarketNameInverted, T1.PairPrices.GetLastUpdate, T1.GetActualUpdateStatus,
                        T1.PairPrices.Ask, T1.PairPrices.AskVolume, T1.PairPrices.Bid, T1.PairPrices.BidVolume

            Dim oDt As DataTable = bvxEncode.LINQToDataTable(x, "Legs")
            Return oDt
        Else Return Nothing
        End If

    End Function

    Public Function GetLegsBetweenByExchangesNames(ByVal pConfigOportunities As bvxConfigOportunities,
                                                   ByVal pExchangeName1 As String,
                                                   ByVal pExchangeName2 As String,
                                                   ByVal pOnlyOportunities As Boolean)

        Dim oDt As DataTable = Nothing
        Dim oRound = pConfigOportunities.BetweenRounds.Where(Function(x) x.Exchange1.Name = pExchangeName1 And x.Exchange2.Name = pExchangeName2).FirstOrDefault
        If Not oRound Is Nothing Then
            Dim Legs = From T1 In oRound.Legs.AsEnumerable
                       Select T1.IsOportunity, T1.Coin1, T1.Coin2, T1.MarketName,
                           InvertedEx1 = T1.PairPricesExchange1.MarketNameInverted,
                           InvertedEx2 = T1.PairPricesExchange2.MarketNameInverted,
                           T1.Rate1To2, T1.Rate2To1,
                           T1.LowerVolume, T1.LowerVolumeExchange,
                           AskEx1 = T1.PairPricesExchange1.Ask,
                           BidEx1 = T1.PairPricesExchange1.Bid,
                           AskEx2 = T1.PairPricesExchange2.Ask,
                           BidEx2 = T1.PairPricesExchange2.Bid,
                           AskVolumeEx1 = T1.PairPricesExchange1.AskVolume,
                           BidVolumeEx1 = T1.PairPricesExchange1.BidVolume,
                           AskVolumeEx2 = T1.PairPricesExchange2.AskVolume,
                           BidVolumeEx2 = T1.PairPricesExchange2.BidVolume,
                           LastUpdateEx1 = T1.PairPricesExchange1.GetLastUpdate,
                           LastUpdateEx2 = T1.PairPricesExchange2.GetLastUpdate,
                           Log = T1.LogOperation,
                           ValidLeg = T1.Valid,
                           InvalidReasonLeg = T1.InvalidReason,
                           ValidEx1 = T1.PairPricesExchange1.Valid,
                           ValidEx2 = T1.PairPricesExchange1.Valid,
                           InvalidReasonEx1 = T1.PairPricesExchange1.InvalidReason,
                           InvalidReasonEx2 = T1.PairPricesExchange2.InvalidReason


            If pOnlyOportunities Then
                Legs = From T1 In Legs.AsEnumerable
                       Where T1.IsOportunity = True
                       Select T1
            End If

            oDt = bvxEncode.LINQToDataTable(Legs, "LegsBetween")
        End If
        Return oDt
    End Function





    Public Function GetLegsIntraByExchangeAndCoins(ByVal pConfigOportunities As bvxConfigOportunities, ByVal pExchange As String,
                                              ByVal pCoins As String, ByVal pOportunities As Boolean)

        Dim Legs = (From T0 In pConfigOportunities.Intra
                    From T1 In pConfigOportunities.Intra.SelectMany(Function(r) r.Rounds)
                    Where T1.Parent Is T0 And T0.Exchange = pExchange
                    From T2 In pConfigOportunities.Intra.SelectMany(Function(r) r.Rounds).SelectMany(Function(f) f.Legs)
                    Where T2.Parent Is T1 And T1.Coins = pCoins
                    Select T2.PairPrices.GetLastUpdate, T2.PairPrices.Coin1, T2.PairPrices.Coin2, T2.PairPrices.MarketName,
                        T2.PairPrices.MarketNameInverted, T2.GetActualUpdateStatus,
                        T2.PairPrices.Ask, T2.PairPrices.Bid, T2.PairPrices.AskVolume, T2.PairPrices.BidVolume,
                        T2.TesteBidAsk, T2.TesteVK, T2.TesteVolume, T2.TesteVolumeWithFee).Distinct()
        Dim oDt As DataTable = bvxEncode.LINQToDataTable(Legs, "Legs")
        If pOportunities Then

        Else

        End If
        Return oDt



        'If pOportunities Then
        '    Dim Legs = (From T0 In pConfigOportunities.Intra
        '                From T1 In pConfigOportunities.Intra.SelectMany(Function(r) r.RoundsOportunities)
        '                Where T1.Parent Is T0 And T0.Exchange = pExchange
        '                From T2 In pConfigOportunities.Intra.SelectMany(Function(r) r.RoundsOportunities).SelectMany(Function(f) f.Legs)
        '                Where T2.Parent Is T1 And T1.Coins = pCoins
        '                Select T1.GetLastUpdate, T2.PairPrices.Coin1, T2.PairPrices.Coin2, T2.PairPrices.MarketName, T2.PairPrices.MarketNameInverted, T2.GetActualUpdateStatus, T2.PairPrices.Ask, T2.PairPrices.AskVolume, T2.PairPrices.Bid, T2.PairPrices.BidVolume).Distinct()
        '    Dim oDt As DataTable = bvxEncode.LINQToDataTable(Legs, "Legs")
        '    Return oDt
        'Else
        '    Dim Legs = (From T0 In pConfigOportunities.Intra
        '                From T1 In pConfigOportunities.Intra.SelectMany(Function(r) r.Rounds)
        '                Where T1.Parent Is T0 And T0.Exchange = pExchange
        '                From T2 In pConfigOportunities.Intra.SelectMany(Function(r) r.Rounds).SelectMany(Function(f) f.Legs)
        '                Where T2.Parent Is T1 And T1.Coins = pCoins
        '                Select T1.GetLastUpdate, T2.PairPrices.Coin1, T2.PairPrices.Coin2, T2.PairPrices.MarketName, T2.PairPrices.MarketNameInverted, T2.GetActualUpdateStatus, T2.PairPrices.Ask, T2.PairPrices.AskVolume, T2.PairPrices.Bid, T2.PairPrices.BidVolume).Distinct()
        '    Dim oDt As DataTable = bvxEncode.LINQToDataTable(Legs, "Legs")
        '    Return oDt
        'End If

    End Function

    Public Function GetIntraRoundsDataTable(ByVal pConfigOportunities As bvxConfigOportunities,
                                              ByVal pOnlyOportunities As Boolean) As DataTable
        Dim oDt = Nothing
        If Not pConfigOportunities Is Nothing Then


            Dim Rounds = (From T0 In pConfigOportunities.Intra
                          From T1 In pConfigOportunities.Intra.SelectMany(Function(r) r.Rounds)
                          Where T1.Parent Is T0
                          Select T1.IsOportunity, T0.Exchange, T1.Valid, T1.Rate, T1.RateNoFee, T1.LowerVolume, T1.LowerVolumeLegIndex, T1.Coins, T1.GetActualUpdateStatus,
                              T1.InvalidReason, T1.LogOperation Order By Rate Descending).Distinct()
            If pOnlyOportunities Then
                Rounds = From T1 In Rounds.AsEnumerable Where T1.IsOportunity = True Select T1
            End If
            oDt = bvxEncode.LINQToDataTable(Rounds, "RoundsIntra")
        End If
        Return oDt
    End Function

    'Public Function GetRoundsBetweenDataTable(ByVal pConfigOportunities As bvxConfigOportunities) As DataTable

    '    Dim Rounds = (From T1 In pConfigOportunities.BetweenRounds
    '                  Select
    '                      HasOportunity = T1.HasOportunity,
    '                      Exchange1 = T1.Exchange1.Name,
    '                      Exchange2 = T1.Exchange2.Name,
    '                      T1.FeeExchange1, T1.FeeExchange2, T1.Valid, T1.InvalidReason)

    '    Dim oDt = bvxEncode.LINQToDataTable(Rounds, "RoundsBetween")
    '    Return oDt
    'End Function


    Public Function GetBetweenRoundsDatatable(ByVal pConfigOportunities As bvxConfigOportunities,
                                              ByVal pOnlyOportunities As Boolean) As DataTable

        If Not pConfigOportunities Is Nothing Then
            Dim Rounds = (From T1 In pConfigOportunities.BetweenRounds
                          From T2A In pConfigOportunities.Between
                          From T2B In pConfigOportunities.Between
                          Where T1.Exchange1.Name = T2A.Exchange And T1.Exchange2.Name = T2B.Exchange
                          Select
                            T1.HasOportunity,
                            Exchange1 = T1.Exchange1.Name,
                            Exchange2 = T1.Exchange2.Name,
                            T1.FeeExchange1, T1.FeeExchange2, T1.Valid, T1.InvalidReason,
                            WalletEx1 = T2A.Wallet, WalletEx2 = T2A.Wallet)



            If pOnlyOportunities Then
                Rounds = From T1 In Rounds.AsEnumerable Where T1.HasOportunity = True Select T1
            End If
            Dim oDt = bvxEncode.LINQToDataTable(Rounds, "RoundsBetween")
            Return oDt
        Else
            Return Nothing
        End If

    End Function



    'Public Function GetOportunitiesIntraDataTable(ByVal pConfigOportunities As bvxConfigOportunities) As DataTable

    '    Dim Rounds = (From T0 In pConfigOportunities.Intra
    '                  From T1 In pConfigOportunities.Intra.SelectMany(Function(r) r.RoundsOportunities)
    '                  Where T1.Parent Is T0
    '                  Select T1.GetLastUpdate, T0.Exchange, T1.Valid, T1.Rate, T1.RateNoFee, T1.Coins, T1.InvalidReason, T1.LogOperation Order By Rate Descending).Distinct()


    '    'Dim Rounds = (From T0 In pConfigOportunities.Intra
    '    '              From T1 In pConfigOportunities.Intra.SelectMany(Function(r) r.RoundsOportunities)
    '    '              Where T1.Parent Is T0
    '    '              Select T1.GetLastUpdate, T0.Exchange, T1.Rate, T1.RateNoFee, T1.Coins, T1.LogOperation Order By Rate Descending).Distinct()

    '    Dim oDt = bvxEncode.LINQToDataTable(Rounds, "Rounds")
    '    Return oDt

    'End Function


    'Public Function GetBetweenRoundsDatatable(ByVal pBetweenRound As bvxArbitBetweenRound)

    '    'If Not pBetween Is Nothing Then
    '    '    Dim x = From T1 In pBetween. .Rounds.AsEnumerable
    '    '            Select T1.Valid, T1.Rate, T1.RateNoFee, T1.Coins, T1.InvalidReason, T1.LogOperation

    '    '    Dim oDt As DataTable = bvxEncode.LINQToDataTable(x, "Legs")
    '    '    Return oDt
    '    'Else Return Nothing
    '    'End If

    'End Function

    Public Function GetIntraRoundsDatatable(ByVal pIntra As bvxArbitIntra)

        If Not pIntra Is Nothing Then
            'ANTES DE TIRAR O VALID DO ROUND
            Dim x = From T1 In pIntra.Rounds.AsEnumerable
                    Select T1.Valid, T1.Rate, T1.RateNoFee, T1.Coins, T1.InvalidReason, T1.LogOperation



            Dim oDt As DataTable = bvxEncode.LINQToDataTable(x, "Legs")
            Return oDt
        Else Return Nothing
        End If

    End Function

    Public Function GetArbitrageList(ByVal pConfigOportunities As bvxConfigOportunities) As DataTable
        'exemplo que gera uma lista dos objetos fazendo joins com os parents
        LoadArbitraeListWithTestVlaues(pConfigOportunities)

        'ANTES DE TIRAR O VALID DO ROUND
        Dim Legs = (From T0 In pConfigOportunities.Intra
                    From T1 In pConfigOportunities.Intra.SelectMany(Function(r) r.Rounds)
                    Where T1.Parent Is T0
                    From T2 In pConfigOportunities.Intra.SelectMany(Function(r) r.Rounds).SelectMany(Function(f) f.Legs)
                    Where T2.Parent Is T1
                    Select T0.Exchange, T1.Rate, T1.Valid, T2.PairPrices.Ask, T2.PairPrices.Bid, T2.PairPrices.AskVolume, T2.PairPrices.BidVolume, T1.InvalidReason, T1.LogOperation).Distinct()



        Dim oDt = bvxEncode.LINQToDataTable(Legs, "Legs")
        Return oDt

    End Function


#End Region



#Region "Database"


    Public Sub PerssistOportunities(ByVal pConfigOportunities As bvxConfigOportunities)
        If pConfigOportunities.PersistInDatabase Then
            'intra
            For Each oIntra In pConfigOportunities.Intra
                For Each oRound In oIntra.Rounds.Where(Function(x) x.IsOportunity = True)
                    PerssistOportunityIntra(oRound)
                Next
            Next
            'entre
            For Each oBetween In pConfigOportunities.BetweenRounds.Where(Function(x) x.HasOportunity)
                PerssistOportunityBetween(oBetween)
            Next
        End If
    End Sub


    Public Sub PerssistOportunityIntra(ByVal pRound As bvxArtbitIntraRound)
        Dim objDao As OportunityDAO = New OportunityDAO()
        objDao.SaveOportunityIntra(pRound)
    End Sub

    Public Sub PerssistOportunityBetween(ByVal pRound As bvxArbitBetweenRound)
        Dim objDao As OportunityDAO = New OportunityDAO()
        objDao.SaveOportunityBetween(pRound)
    End Sub

#End Region


#Region "Testes"

    Property ThreadList As List(Of String)

    'Private Sub RefreshLegMethodForThread(pLeg As bvxArtbitIntraRoundLeg)

    '    Dim API = pLeg.Parent.Parent.ObjExchange.API
    '    pLeg.SetUpdateState(bvxThreadUpdatableObject.bvxUpdatableObjectStateList.WaitingAPIResponse)
    '    pLeg.SetAskAndBidValues(API.GetAskBidForMarket(pLeg.PairPrices.MarketName))

    'End Sub

    'Public Sub RefreshIntraOportunities_Fase1(ByRef pConfigOportunities As bvxConfigOportunities)
    '    Try
    '        If pConfigOportunities.State = bvxConfigOportunities.bvxConfigOportunitiesState.Updating Then
    '            LogService.Write("***Controller starting refresh legs...", LogService.Type.INFO)
    '            RaiseMessage("Refreshing market values...", 2)

    '            RaiseMessage("Cleaning master markets...", 3)
    '            ClearAskAndBidMaster(pConfigOportunities)

    '            RaiseMessage("Calling exchanges APIs...", 3)
    '            For Each oArbitIntra As bvxArbitIntra In pConfigOportunities.Intra
    '                If Not oArbitIntra.Valid Then
    '                    Continue For
    '                End If
    '                For Each objLegMaster As bvxArtbitIntraRoundLeg In oArbitIntra.MarketLegMaster
    '                    Try
    '                        LogService.Write("***Launching thread: " + oArbitIntra.Exchange + " - " + objLegMaster.PairPrices.MarketName, LogService.Type.INFO)
    '                        Dim oThread = New Thread(AddressOf RefreshLegMethodForThread)
    '                        oThread.Name = oArbitIntra.Exchange + " - " + objLegMaster.PairPrices.MarketName
    '                        oThread.Start(objLegMaster)
    '                    Catch ex As Exception
    '                        objLegMaster.ResetAskAndBidValues(bvxThreadUpdatableObject.bvxUpdatableObjectStateList.ErrorResponse)
    '                        LogService.Write("***Error in RefreshRounds. [Exchange: " + oArbitIntra.Exchange +
    '                                 "][Market: " + objLegMaster.PairPrices.MarketName + "]" +
    '                                 Environment.NewLine + ex.ToString(), LogService.Type.FAILURE)
    '                    End Try
    '                Next
    '            Next
    '            LogService.Write("***Finishing fase 1, all threads launched.", LogService.Type.INFO)
    '            RaiseMessage("Finishing fase 1, all threads launched.", 3)
    '        End If
    '    Catch ex As Exception
    '        LogService.Write("Error on RefreshOportunities: " + ex.ToString(), LogService.Type.FATAL)
    '        pConfigOportunities.State = bvxConfigOportunities.bvxConfigOportunitiesState.Ready
    '        RaiseMessage("Finished with errors.", 2)
    '        RaiseRefreshFinished(pConfigOportunities)
    '    End Try


    'End Sub

    'Public Sub RefreshIntraOportunities_Fase2(ByRef pConfigOportunities As bvxConfigOportunities)

    '    Try
    '        If pConfigOportunities.State = bvxConfigOportunities.bvxConfigOportunitiesState.Updating Then
    '            RaiseMessage("Calculating oportunities.", 3)
    '            SpreadMasterToLegs(pConfigOportunities)
    '            CalculateIntraOportunities2(pConfigOportunities)
    '            IdentifyOportunities(pConfigOportunities)
    '            RaiseMessage("Object refrehed.", 3)
    '            pConfigOportunities.State = bvxConfigOportunities.bvxConfigOportunitiesState.Ready
    '            RaiseRefreshFinished(pConfigOportunities)
    '        End If
    '    Catch ex As Exception
    '        LogService.Write("Error on RefreshOportunities: " + ex.ToString(), LogService.Type.FATAL)
    '        pConfigOportunities.State = bvxConfigOportunities.bvxConfigOportunitiesState.Ready
    '        RaiseMessage("Finished with errors.", 2)
    '        RaiseRefreshFinished(pConfigOportunities)
    '    End Try


    'End Sub

    Public Sub SaveToJson(ByRef pConfigOportunities As bvxConfigOportunities, ByVal pFileName As String)

        Dim x = JsonObject.FromClass(Of bvxConfigOportunities)(pConfigOportunities)
        File.WriteAllText(pFileName, x)

    End Sub

    Public Sub LoadFromJson(ByVal pFileName As String)

        Dim rawJson = File.ReadAllText(ConfigFileName)
        ConfigOportunitiesSingleton.LoadConfig(JsonFactory.CreateJsonObject(rawJson))

    End Sub

    Public Sub LoadArbitraeListWithTestVlaues(ByVal pConfigOportunities As bvxConfigOportunities)

        Dim i As Integer = 101
        For Each oIntra In pConfigOportunities.Intra
            For Each oRound In oIntra.Rounds
                For Each oLeg In oRound.Legs
                    oLeg.PairPrices.Ask = i
                    i += 1
                    oLeg.PairPrices.AskVolume = i
                    i += 1
                    oLeg.PairPrices.Bid = i
                    i += 1
                    oLeg.PairPrices.BidVolume = i
                    i += 1
                Next
            Next
        Next

    End Sub

    Public Function GenerateIntraRounds(pExchangeName As String, pPreferentialCoinsFileName As String, pPersistInDatabase As Boolean) As List(Of String)


        LogService.Write("Starting rounds generation for exchange: " + pExchangeName + ".", LogService.Type.INFO)
        Dim objIntra = New bvxArbitIntra()
        objIntra.Exchange = pExchangeName
        objIntra.ObjExchange = GetExchangeByNameFromDbWithMarketListFromAPI(pExchangeName, pPersistInDatabase)

        'cria lista de markets com moedas separadas
        Dim MarketList As List(Of bvxMarketSplited) = New List(Of bvxMarketSplited)
        For Each sMarketName In objIntra.ObjExchange.MarketList
            MarketList.Add(New bvxMarketSplited(sMarketName))
        Next

        Dim ListPreferentialCoins As List(Of String) = FileService.TextFileToList(pPreferentialCoinsFileName)
        Dim ListNewRounds As List(Of String) = New List(Of String)
        Dim LoopCount As Integer = 0
        Dim CoinCount As Integer = 0

        For Each sCoin1Aux In ListPreferentialCoins
            RaiseMessage("Generating rounds for " + sCoin1Aux, 2)
            LogService.Write("Starting round generation for preferential coin: " + sCoin1Aux + ".", LogService.Type.INFO)
            'List1 são moedas que tem mercado com a preferencial
            'List2 sao mercados de cada moeda preferencial
            Dim List2 = MarketList.Where(Function(x) _
                                            x.CoinA.ToUpper() = sCoin1Aux.ToUpper() Or
                                            x.CoinB.ToUpper() = sCoin1Aux.ToUpper())

            If List2.Count > 0 Then
                'faço esta manobra para aproveitar o case sensitive da moeda preferncial vinda do MarketList e não do meu arquivo de preferencialCoins
                'sCoin1 é a moeda preferencial
                Dim sCoin1 = IIf(List2(0).CoinA.ToUpper() = sCoin1Aux.ToUpper(), List2(0).CoinA, List2(0).CoinB)
                CoinCount = 0
                For Each Market2 In List2
                    'pega a moeda diferente da moeda 1
                    Dim sCoin2 = IIf(Market2.CoinA.ToUpper() = sCoin1.ToUpper(), Market2.CoinB, Market2.CoinA)
                    Dim List3 = MarketList.Where(Function(x) _
                                                     (x.CoinA.ToUpper() <> sCoin1.ToUpper() And x.CoinB.ToUpper() = sCoin2.ToUpper()) Or
                                                     (x.CoinA.ToUpper() = sCoin2.ToUpper() And x.CoinB.ToUpper() <> sCoin1.ToUpper()))

                    For Each Market3 In List3
                        LoopCount = LoopCount + 1
                        'pega a moeda diferente da moeda 2
                        Dim sCoin3 = IIf(Market3.CoinA.ToUpper() = sCoin2.ToUpper(), Market3.CoinB, Market3.CoinA)
                        'verifica se a terceira moeda tem mercado com a primeira
                        If MarketList.Where(Function(x) _
                                             (x.CoinA.ToUpper() = sCoin3.ToUpper() And x.CoinB.ToUpper() = sCoin1.ToUpper()) Or
                                             (x.CoinA.ToUpper() = sCoin1.ToUpper() And x.CoinB.ToUpper() = sCoin3.ToUpper())).Count > 0 Then
                            CoinCount = CoinCount + 1
                            ListNewRounds.Add(String.Format("""{0}, {1}, {2}"",", sCoin1, sCoin2, sCoin3))
                            If ListNewRounds.Count Mod 100 = 0 Then
                                RaiseMessage(ListNewRounds.Count.ToString + " rounds generated", 3)
                                LogService.Write("Generating rounds: " + ListNewRounds.Count.ToString, LogService.Type.INFO)
                            End If
                        End If
                    Next
                Next
                LogService.Write("Finishing round generation for preferential coin: " + sCoin1Aux +
                                     ". || Rounds: " + CoinCount.ToString, LogService.Type.INFO)
            Else
                LogService.Write("No markets found for this preferential coin: " + sCoin1Aux + ".", LogService.Type.INFO)
            End If
        Next
        LogService.Write("Round generation finished.", LogService.Type.INFO)
        Return ListNewRounds

    End Function

    Private Function GetCoinsFromMarketList(pList As List(Of String)) As List(Of String)

        Dim list As List(Of String) = New List(Of String)
        Dim arr As String()
        For Each s In pList

            arr = s.Split("_")
            If arr.Length = 2 Then
                list.Add(arr(0))
                list.Add(arr(1))
            End If

        Next

        Return list.Distinct().ToList()


    End Function





#End Region


End Class
