﻿Imports MySql.Data.MySqlClient

Public Class OportunityDAO


    Protected appSettingsReader As New Configuration.AppSettingsReader
    Protected xmlService As XMLService
    Protected adapter As MySQLDataAccess

#Region "Database"


    Public Sub New()

        xmlService = New XMLService()
        Dim host As String = appSettingsReader.GetValue("dataSource", GetType(String))
        Dim user As String = appSettingsReader.GetValue("user", GetType(String))
        Dim pwd As String = appSettingsReader.GetValue("password", GetType(String))
        Dim schema As String = appSettingsReader.GetValue("schema", GetType(String))
        adapter = New MySQLDataAccess(host, user, pwd, schema)

    End Sub




    Public Function GetExchangeById(pId As Integer) As bvxExchange

        adapter.OpenConnection()
        Dim params As New Dictionary(Of String, Object)
        Try
            params.Add("id", pId)
            Dim oDt = adapter.GetDataTable(xmlService.GetSqlQuery("GET_EXCHANGE_BY_ID"), params)
            If oDt.Rows.Count > 0 Then
                Dim obj As bvxExchange = New bvxExchange(oDt.Rows(0)("id"), oDt.Rows(0)("name").ToString)
                Return obj
            Else Return Nothing
            End If

        Catch ex As Exception
            LogService.Write("***Error on OportunityDAO.GetExchangeById: " + ex.ToString(), LogService.Type.FATAL)
            Throw
        End Try

    End Function

    Public Function GetExchangeByName(pName As String) As bvxExchange

        adapter.OpenConnection()
        Dim params As New Dictionary(Of String, Object)
        Try
            params.Add("name", pName)
            Dim oDt = adapter.GetDataTable(xmlService.GetSqlQuery("GET_EXCHANGE_BY_NAME"), params)
            If oDt.Rows.Count > 0 Then
                Dim obj As bvxExchange = New bvxExchange(oDt.Rows(0)("id"), oDt.Rows(0)("name").ToString)
                Return obj
            Else Return Nothing
            End If

        Catch ex As Exception
            LogService.Write("***Error on OportunityDAO.GetExchangeByName: " + ex.ToString(), LogService.Type.FATAL)
            Throw
        End Try

    End Function

    Public Function ListExchanges() As List(Of bvxExchange)

        adapter.OpenConnection()
        Try
            Dim oDt = adapter.GetDataTable(xmlService.GetSqlQuery("LIST_EXCHANGES"))
            Dim List As List(Of bvxExchange) = New List(Of bvxExchange)
            For Each oRow In oDt.Rows
                List.Add(New bvxExchange(oDt.Rows(0)("id"), oDt.Rows(0)("name").ToString))
            Next
            Return List
        Catch ex As Exception
            LogService.Write("***Error on OportunityDAO.ListExchanges: " + ex.ToString(), LogService.Type.FATAL)
            Throw
        End Try

    End Function

    Public Function SaveExchange(pName As String) As Integer

        Dim Id As Integer
        adapter.OpenConnection()
        Dim params As New Dictionary(Of String, Object)
        params.Add("name", pName)

        Dim oExchange = GetExchangeByName(pName)
        If Not oExchange Is Nothing Then
            Id = oExchange.Id
            params.Add("id", Id)
            adapter.ExecuteNonQuery(xmlService.GetSqlQuery("UPDATE_EXCHANGE"), params)
        Else
            adapter.ExecuteNonQuery(xmlService.GetSqlQuery("INSERT_EXCHANGE"), params)
            Id = GetLastInsertedId()
        End If
        Return Id

    End Function


    Private Function GetLastInsertedId() As Integer

        Dim oDt = adapter.GetDataTable(xmlService.GetSqlQuery("GET_LAST_INSERTED_ID"))
        If oDt.Rows.Count > 0 Then
            Return oDt.Rows(0)(0)
        Else
            Return -1
        End If

    End Function


    Public Sub SaveOportunityIntra(pRound As bvxArtbitIntraRound)
        adapter.OpenConnection()
        Dim oTran As MySqlTransaction = adapter.BeginTransaction()
        Try
            Dim RoundId = SaveOportIntraRound(pRound, oTran)
            Dim LegIndex As Integer = 0
            For Each oLeg As bvxArbitIntraRoundLeg In pRound.Legs
                SaveOportIntraLeg(oLeg, RoundId, LegIndex, oTran)
                LegIndex = LegIndex + 1
            Next
            oTran.Commit()
        Catch ex As Exception
            oTran.Rollback()
            LogService.Write("***Error on OportunityDAO.SaveOportunityIntra: " + ex.ToString(), LogService.Type.FATAL)
            Throw
        End Try
    End Sub


    Public Sub SaveOportunityBetween(pRound As bvxArbitBetweenRound)
        adapter.OpenConnection()
        Dim oTran As MySqlTransaction = adapter.BeginTransaction()
        Try
            Dim RoundId = SaveOportBetweenRound(pRound, oTran)
            For Each oLeg As bvxArbitBetweenRoundLeg In pRound.Legs.Where(Function(x) x.IsOportunity = True)
                SaveOportBetweenLeg(oLeg, RoundId, oTran)
            Next
            oTran.Commit()
        Catch ex As Exception
            oTran.Rollback()
            LogService.Write("***Error on OportunityDAO.SaveOportunityBetween: " + ex.ToString(), LogService.Type.FATAL)
            Throw
        End Try
    End Sub




    Private Sub SaveOportIntraLeg(pLeg As bvxArbitIntraRoundLeg, pRoundId As Integer, pLegIndex As Integer,
                             pTransaction As MySqlTransaction)
        Dim params As New Dictionary(Of String, Object)
        Try
            params.Add("id_oportintraround", pRoundId)
            params.Add("round_index", pLegIndex)
            params.Add("market_name", pLeg.PairPrices.MarketName)
            params.Add("coin1", pLeg.PairPrices.Coin1)
            params.Add("coin2", pLeg.PairPrices.Coin2)
            params.Add("ask", pLeg.PairPrices.Ask)
            params.Add("bid", pLeg.PairPrices.Bid)
            params.Add("ask_volume", pLeg.PairPrices.AskVolume)
            params.Add("bid_volume", pLeg.PairPrices.BidVolume)
            adapter.ExecuteNonQuery(xmlService.GetSqlQuery("INSERT_OPORTINTRALEG"), params, pTransaction)
        Catch ex As Exception
            LogService.Write("***Error on OportunityDAO.SaveOportIntraLeg: " + ex.ToString(), LogService.Type.FATAL)
            Throw
        End Try
    End Sub



    Private Function SaveOportIntraRound(pRound As bvxArtbitIntraRound, pTransaction As MySqlTransaction) As Integer
        Dim params As New Dictionary(Of String, Object)
        Try
            params.Add("id_exchange", pRound.Parent.ObjExchange.Id)
            params.Add("coins", pRound.Coins)
            params.Add("fee", pRound.Parent.Fee)
            params.Add("date_oport", pRound.GetLastUpdate)
            params.Add("Rate", pRound.Rate)
            params.Add("lowervolume", pRound.LowerVolume)
            params.Add("lowervolumelegindex", pRound.LowerVolumeLegIndex)
            adapter.ExecuteNonQuery(xmlService.GetSqlQuery("INSERT_OPORTINTRAROUND"), params, pTransaction)
            Dim id = GetLastInsertedId()
            Return id
        Catch ex As Exception
            LogService.Write("***Error on OportunityDAO.SaveOportIntraRound: " + ex.ToString(), LogService.Type.FATAL)
            Throw
        End Try
    End Function



    Private Sub SaveOportBetweenLeg(pLeg As bvxArbitBetweenRoundLeg, pRoundId As Integer,
                             pTransaction As MySqlTransaction)
        Dim params As New Dictionary(Of String, Object)
        Try
            params.Add("date", pLeg.PairPricesExchange1.GetLastUpdate)
            params.Add("id_oportbetweenround", pRoundId)
            params.Add("market_name", pLeg.MarketName)
            params.Add("coin1", pLeg.Coin1)
            params.Add("coin2", pLeg.Coin2)
            params.Add("rate1to2", pLeg.Rate1To2)
            params.Add("rate2to1", pLeg.Rate2To1)
            params.Add("ask_ex1", pLeg.PairPricesExchange1.Ask)
            params.Add("ask_ex2", pLeg.PairPricesExchange2.Ask)
            params.Add("bid_ex1", pLeg.PairPricesExchange1.Bid)
            params.Add("bid_ex2", pLeg.PairPricesExchange2.Bid)
            params.Add("askvolume_ex1", pLeg.PairPricesExchange1.AskVolume)
            params.Add("askvolume_ex2", pLeg.PairPricesExchange2.AskVolume)
            params.Add("bidvolume_ex1", pLeg.PairPricesExchange1.BidVolume)
            params.Add("bidvolume_ex2", pLeg.PairPricesExchange2.BidVolume)
            params.Add("log_operation", pLeg.LogOperation)
            params.Add("lowervolume", pLeg.LowerVolume)
            params.Add("lowervolumeexchange", pLeg.LowerVolumeExchange)

            adapter.ExecuteNonQuery(xmlService.GetSqlQuery("INSERT_OPORTBETWEENLEG"), params, pTransaction)
        Catch ex As Exception
            LogService.Write("***Error on OportunityDAO.SaveOportBetweenLeg: " + ex.ToString(), LogService.Type.FATAL)
            Throw
        End Try
    End Sub



    Private Function SaveOportBetweenRound(pRound As bvxArbitBetweenRound, pTransaction As MySqlTransaction) As Integer
        Dim params As New Dictionary(Of String, Object)
        Try
            params.Add("date_oport  ", pRound.GetLastUpdate)
            params.Add("id_exchange1", pRound.Exchange1.Id)
            params.Add("id_exchange2", pRound.Exchange2.Id) 
            adapter.ExecuteNonQuery(xmlService.GetSqlQuery("INSERT_OPORTBETWEENROUND"), params, pTransaction)
            Dim id = GetLastInsertedId()
            Return id
        Catch ex As Exception
            LogService.Write("***Error on OportunityDAO.SaveOportBetweenRound: " + ex.ToString(), LogService.Type.FATAL)
            Throw
        End Try
    End Function


#End Region


End Class
