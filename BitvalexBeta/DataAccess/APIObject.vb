﻿
Public Class APIObject

    Protected ExchangeName As String
    Protected APIKey As String
    Protected APISecret As String
    Protected PublicURL As String
    Protected PrivateURL As String

    Protected WebAdapter As New WebAdapter

    Public Sub New()
        Throw New Exception("Não é possível instanciar uma API sem seus parâmetros de configuração")
    End Sub

    Public Sub New(apiKey As String, apiSecret As String, publicURL As String, privateURL As String, pExchangeName As String)
        Me.ExchangeName = pExchangeName
        Me.APIKey = apiKey
        Me.APISecret = apiSecret
        Me.PublicURL = publicURL
        Me.PrivateURL = privateURL
    End Sub

    Public Function Request(Of T)(ByVal pCommand As String) As T

        Try
            Dim rawJson As String = WebAdapter.SendRequest(pCommand)
            Dim response = JsonFactory.CreateJsonObject(rawJson).ToClass(Of T)
            Return response
        Catch ex As Exception
            LogService.Write("***Error on ParentAPI.Request. [Command: " + pCommand + "]" + Environment.NewLine + ex.ToString(), LogService.Type.FATAL)
            Throw
        End Try

    End Function

End Class
