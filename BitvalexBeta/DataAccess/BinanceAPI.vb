﻿Imports Teste1

'Documentação da API
'https://github.com/binance-exchange/binance-official-api-docs




Public Class BinanceAPI
    Inherits APIObject
    Implements IAPIDAO

    Public Sub New(apiKey As String, apiSecret As String, publicURL As String, privateURL As String, pExchangeName As String)
        MyBase.New(apiKey, apiSecret, publicURL, privateURL, pExchangeName)
    End Sub


#Region "Interface Methods"

    Public Function GetMarketNameList() As List(Of String) Implements IAPIDAO.GetMarketNameList

        Dim obj As BinanceInfoResult = GetExchangeInfo()
        Dim x = (From T1 In obj.Symbols.AsEnumerable Select T1.BaseAsset + "_" + T1.QuoteAsset).ToList()
        x.Sort()
        Return x

    End Function

    Public Function GetAskBidForMarket(pMarketName As String) As bvxPairPrices Implements IAPIDAO.GetAskBidForMarket
        Dim obj As BinanceDepthResult = Nothing
        Dim result As bvxPairPrices = New bvxPairPrices(Nothing)
        Try
            'OS MERCADOS DA BINANCE NÃO TEM O _
            obj = GetDepth(pMarketName.Replace("_", ""))
            If Not obj Is Nothing Then
                result.Ask = obj.Asks(0)(0).replace(".", ",")
                result.AskVolume = obj.Asks(0)(1).replace(".", ",")
                result.Bid = obj.Bids(0)(0).replace(".", ",")
                result.BidVolume = obj.Bids(0)(1).replace(".", ",")
                result.SetUpdateState(bvxThreadUpdatableObject.bvxUpdatableObjectStateList.UpToDate)
            End If
            Return result
        Catch ex As Exception
            result.ResetAskAndBidValues(bvxThreadUpdatableObject.bvxUpdatableObjectStateList.ErrorResponse)
            Return result
        End Try

    End Function


#End Region

#Region "Public_calls"

    'https://www.binance.com/api/v1/exchangeInfo
    Public Function GetExchangeInfo() As BinanceInfoResult

        Dim response = Request(Of BinanceInfoResult)(PublicURL + "/exchangeInfo")
        Return response

        'código antigo
        'Dim response = Request(Of BinanceResponseList(Of BinanceExchangeInfo))(PublicURL + "/exchangeInfo")
        ''Dim list As BindingList(Of BinanceExchangeInfo) = New BindingList(Of BinanceExchangeInfo)(response.Results)
        'Return response.Symbols

    End Function


    'https://www.binance.com/api/v1/depth?symbol=BNBBTC&limit=10
    Public Function GetDepth(ByVal pMarket As String, Optional ByVal pDepth As Integer = 5) As BinanceDepthResult
        Dim URL As String = PublicURL + "/depth?symbol=" + pMarket + "&limit=" + pDepth.ToString
        Dim response = Request(Of BinanceDepthResult)(URL)
        Return response
    End Function


#End Region

End Class