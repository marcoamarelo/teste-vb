﻿Public Class BleutradeAPI
    Inherits APIObject
    Implements IAPIDAO

    'Public Function SendPublicRequest(ByVal pCommand As String) As String

    '    Try
    '        Dim rawJson As String = WebAdapter.SendRequest(PublicURL + "/" + pCommand)
    '        Return rawJson
    '    Catch ex As Exception
    '        Return "***Error*** - " + ex.Message
    '    End Try

    'End Function

    Public Sub New(apiKey As String, apiSecret As String, publicURL As String, privateURL As String,
                   pExchangeName As String)
        MyBase.New(apiKey, apiSecret, publicURL, privateURL, pExchangeName)
    End Sub

#Region "Interface Methods"


    Public Function GetMarketNameList() As List(Of String) _
        Implements IAPIDAO.GetMarketNameList

        Dim list As List(Of BleutradeMarket) = GetMarkets()
        Return (From T1 In list.AsEnumerable Select T1.MarketName Order By MarketName).ToList()

    End Function


    Public Function GetAskBidForMarket(ByVal pMarketName As String) As bvxPairPrices _
                                  Implements IAPIDAO.GetAskBidForMarket
        'chamada http
        Dim obj As BleutradeMarketBookOrders = Nothing
        Dim result As bvxPairPrices = New bvxPairPrices(Nothing)
        Try
            obj = GetOrderBook(pMarketName, "ALL", 1)
            If Not obj Is Nothing Then
                result.Ask = obj.Sell(0).Rate
                result.AskVolume = obj.Sell(0).Quantity
                result.Bid = obj.Buy(0).Rate
                result.BidVolume = obj.Buy(0).Quantity
                result.SetUpdateState(bvxThreadUpdatableObject.bvxUpdatableObjectStateList.UpToDate)
            End If
            Return result
        Catch ex As Exception
            result.ResetAskAndBidValues(bvxThreadUpdatableObject.bvxUpdatableObjectStateList.ErrorResponse)
            Return result
        End Try
    End Function


    'Public Function GetFeeList() As List(Of bvxExchangeCoinFee) _
    '    Implements IAPIDAO.GetFeeList

    '    Dim list As List(Of bvxExchangeCoinFee) = New List(Of bvxExchangeCoinFee)

    '    Dim CurrencyList = GetCurrencies()
    '    For Each obj In CurrencyList
    '        Dim fee As bvxExchangeCoinFee = New bvxExchangeCoinFee()
    '        Double.TryParse(obj.TxFee.Replace(".", ","), fee.Fee)
    '        fee.Coin = obj.Currency
    '        fee.Exchange = Me.ExchangeName
    '        list.Add(fee)
    '    Next
    '    Return list

    'End Function

#End Region



#Region "Public_calls"

    Public Function GetCurrencies() As List(Of BleutradeCurrency)

        Dim response = Request(Of BleutradeResponseList(Of BleutradeCurrency))(PublicURL + "/getcurrencies")
        Dim list As List(Of BleutradeCurrency) = New List(Of BleutradeCurrency)(response.Results)
        Return list

    End Function


    Public Function GetMarkets() As List(Of BleutradeMarket)

        Dim response = Request(Of BleutradeResponseList(Of BleutradeMarket))(PublicURL + "/getmarkets")
        'Dim list As BindingList(Of BleutradeMarket) = New BindingList(Of BleutradeMarket)(response.Results)
        Return response.Results

    End Function


    Public Function GetMarketSummaries() As List(Of BleutradeMarketSummary)

        Dim response = Request(Of BleutradeResponseList(Of BleutradeMarketSummary))(PublicURL + "/getmarketsummaries")
        'Dim list As BindingList(Of BleutradeMarketSummary) = New BindingList(Of BleutradeMarketSummary)(response.Results)
        Return response.Results

    End Function


    Public Function GetMarketSummary(ByVal pMarket As String) As List(Of BleutradeMarketSummary)

        '/public/getmarketsummary?market=ETH_BTC)
        If String.IsNullOrEmpty(pMarket) Then
            Throw New Exception("pMarket parameter is mandatory.")
        End If
        Dim response = Request(Of BleutradeResponseList(Of BleutradeMarketSummary))(PublicURL + "/getmarketsummary?market=" + pMarket)
        'Dim list As BindingList(Of BleutradeMarketSummary) = New BindingList(Of BleutradeMarketSummary)(response.Results)
        Return response.Results

    End Function


    Public Function GetTicker(ByVal pMarket As String) As List(Of BleutradeTicker)

        'DOGE_BTC
        'market(Or markets)(ex.:  /public/getticker?market=ETH_BTC Or /public/getticker?market=ETH_BTC,HTML5_DOGE,DOGE_LTC)
        'https://bleutrade.com/api/v2/public/getticker?market=ETH_BTC
        Dim URL As String = PublicURL + "/getticker?market=" + pMarket

        Dim response = Request(Of BleutradeResponseList(Of BleutradeTicker))(URL)
        'Dim list As BindingList(Of BleutradeTicker) = New BindingList(Of BleutradeTicker)(response.Results)
        Return response.Results

    End Function


    'getorderbook
    'Required parameters
    'market 
    'type (BUY, SELL, ALL) 
    'depth (optional, default Is 20)
    'getbalances?apikey='.$apikey.'&nonce='.$nonce;
    'https://bleutrade.com/api/v2/public/getorderbook?market=ETH_BTC?type=ALL
    Public Function GetOrderBook(ByVal pMarket As String, ByVal pType As String,
                                 Optional ByVal pDepth As Integer = 20) As BleutradeMarketBookOrders

        Dim URL As String = PublicURL + "/getorderbook?market=" + pMarket + "&type=" + pType
        If pDepth <> 20 Then
            URL = URL + "&depth=" + pDepth.ToString()
        End If

        Dim response = Request(Of BleutradeResponseSingle(Of BleutradeMarketBookOrders))(URL)
        Return response.Result

    End Function

    'getmarkethistory
    Public Function GetMarketHistory(ByVal pMarket As String, Optional ByVal pCount As Integer = 20) As List(Of BleutradeMarketHistory)

        If pCount <= 0 Or pCount > 200 Then
            Throw New Exception("GetMarketHistory parameter pCount needs to be between 0 and 200.")
        End If
        Dim URL As String = PublicURL + "/getmarkethistory?market=" + pMarket
        If pCount <> 20 Then
            URL = URL + "&count=" + pCount.ToString()
        End If

        Dim response = Request(Of BleutradeResponseList(Of BleutradeMarketHistory))(URL)
        'Dim list As BindingList(Of BleutradeMarketHistory) = New BindingList(Of BleutradeMarketHistory)(response.Results)
        Return response.Results

    End Function


    'getcandles
    'https://bleutrade.com/api/v2/public/getcandles?market=HTML_BTC&period=1d
    'PARAMETERS
    'market 
    'period (1m, 2m, 3m, 4m, 5m, 6m, 10m, 12m, 15m, 20m, 30m, 1h, 2h, 3h, 4h, 6h, 8h, 12h, 1d) 
    'count (default: 1000, max: 999999) 
    'lasthours (default: 24, max: 2160)
    Public Function GetCandles(ByVal pMarket As String, ByVal pPeriod As String, Optional ByVal pCount As Integer = 1000,
                               Optional ByVal pLastHours As Integer = 24) As List(Of BleutradeCandle)

        Dim PeriodList As New List(Of String) _
            ({"1m", "2m", "3m", "4m", "5m", "6m", "10m", "12m", "15m", "20m", "30m", "1h", "2h", "3h", "4h", "6h", "8h", "12h", "1d"})

        If pCount <= 0 Or pCount > 999999 Then
            Throw New Exception("GetCandles parameter pCount needs to be between 0 and 999999.")
        End If
        If pLastHours <= 0 Or pCount > 2160 Then
            Throw New Exception("GetCandles parameter pLastHours needs to be between 0 and 2160.")
        End If
        If Not PeriodList.Contains(pPeriod.ToLower().Trim()) Then
            Throw New Exception("GetCandles parameter pPeriod invalid.")
        End If
        Dim URL As String = PublicURL + "/getcandles?market=" + pMarket.ToLower.Trim() + "&period=" + pPeriod.ToLower.Trim()
        If pCount <> 1000 Then
            URL = URL + "&count=" + pCount.ToString()
        End If
        If pLastHours <> 24 Then
            URL = URL + "&lasthours=" + pLastHours.ToString()
        End If

        Dim response = Request(Of BleutradeResponseList(Of BleutradeCandle))(URL)
        'Dim list As BindingList(Of BleutradeCandle) = New BindingList(Of BleutradeCandle)(response.Results)
        Return response.Results

    End Function



#End Region

#Region "Private calls"

    'finaliza o preenchimento da URI com os parametros de chave secreta e criptografia
    Private Function GetURIWithEncriptedPart(pPartialURI As String) As String
        Dim URI As String = pPartialURI +
                        "&apikey=" + APIKey +
                        "&nonce=" + bvxEncode.GetNonce
        Dim Sign As String = bvxEncode.HashStringHMAC_SHA512(URI, APISecret)
        URI = URI + "&apisign=" + Sign
        Return URI
    End Function


    '/account/getdepositaddress
    '/account/withdraw
    '/account/getorder
    '/account/getorders
    '/account/getorderhistory
    '/account/getdeposithistory
    '/account/getwithdrawhistory


    '/account/getbalances
    'https://bleutrade.com/api/v2/account/getbalances?currencies=DOGE;BTC
    'COINS NO PARAMETRO DEVEM SER PASSADAS SEPARADAS POR ; E SEM ESPAÇO
    Public Function Balances(pCoins As String) As BleutradeResponseList(Of BleutradeBalance)
        Dim URI As String = PrivateURL + "account/getbalances" +
            "?currencies=" + pCoins
        Dim response = Request(Of BleutradeResponseList(Of BleutradeBalance))(GetURIWithEncriptedPart(URI))
        Return response
    End Function


    Public Function Balance(pCoin As String) As BleutradeResponseSingle(Of BleutradeBalance)
        Dim URI As String = PrivateURL + "account/getbalance" +
            "?currency=" + pCoin
        Dim response = Request(Of BleutradeResponseSingle(Of BleutradeBalance))(GetURIWithEncriptedPart(URI))
        Return response

    End Function


    '/market/getopenorders
    Public Function GetOpenOrders() As BleutradeResponseList(Of BleutradeOrder)
        Dim URI As String = PrivateURL + "market/getopenorders?"
        Dim response = Request(Of BleutradeResponseList(Of BleutradeOrder))(GetURIWithEncriptedPart(URI))
        Return response
    End Function


    '/account/getorder
    'orderid
    Public Function GetOrder(pOrderId As String) As BleutradeResponseSingle(Of BleutradeOrder)
        Dim URI As String = PrivateURL + "market/getorder" +
            "?orderid=" + pOrderId
        Dim response = Request(Of BleutradeResponseSingle(Of BleutradeOrder))(GetURIWithEncriptedPart(URI))
        Return response
    End Function


    '/market/cancel
    Public Function CancelOrder(pOrderId As String) As BleutradeResponseSingle(Of List(Of String))
        Dim URI As String = PrivateURL + "market/cancel" +
            "?orderid=" + pOrderId
        Dim response = Request(Of BleutradeResponseSingle(Of List(Of String)))(GetURIWithEncriptedPart(URI))
        Return response
    End Function


    'market 
    'rate 
    'quantity 
    'comments
    '/market/buylimit 
    Public Function BuyLimit(pMarket As String, pRate As Double, pQuantity As Double, pComments As String) As _
        BleutradeResponseSingle(Of BleutradeOrderIdResult)

        Dim URI As String = PrivateURL + "market/buylimit" +
            "?market=" + pMarket +
            "&rate=" + pRate.ToString.Replace(",", ".") +
            "&quantity=" + pQuantity.ToString.Replace(",", ".")
        'If Not String.IsNullOrEmpty(pComments) Then
        '    URI = URI + "&comments=" + pComments
        'End If
        Dim response = Request(Of BleutradeResponseSingle(Of BleutradeOrderIdResult))(GetURIWithEncriptedPart(URI))
        Return response
    End Function


    'market 
    'rate 
    'quantity 
    'comments
    '/market/selllimit
    Public Function SellLimit(pMarket As String, pRate As Double, pQuantity As Double, pComments As String) As _
            BleutradeResponseSingle(Of BleutradeOrderIdResult)
        Dim URI As String = PrivateURL + "market/buylimit" +
            "?market=" + pMarket +
            "&rate=" + pRate.ToString +
            "&quantity=" + pQuantity.ToString
        'If Not String.IsNullOrEmpty(pComments) Then
        '    URI = URI + "&comments=" + pComments
        'End If
        Dim response = Request(Of BleutradeResponseSingle(Of BleutradeOrderIdResult))(GetURIWithEncriptedPart(URI))
        Return response
    End Function


#End Region

End Class


