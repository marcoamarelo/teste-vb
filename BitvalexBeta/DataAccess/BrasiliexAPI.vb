﻿
Imports System.Configuration
Imports System.Collections.Specialized

Public Class BrasiliexAPI
    Inherits APIObject

    Private command As New Dictionary(Of String, String)

    Public Sub New(apiKey As String, apiSecret As String, publicURL As String, privateURL As String, pExchangeName As String)
        MyBase.New(apiKey, apiSecret, publicURL, privateURL, pExchangeName)
    End Sub


    'post
    Public Function MountPostData(ByVal pCommand As Dictionary(Of String, String)) As String

        'nonce
        pCommand.Add("nonce", System.DateTime.Now.Ticks.ToString())
        'build query string
        Dim PostData As String = bvxEncode.HTTPEncode(pCommand)
        'extra headers
        Dim Headers As NameValueCollection = New NameValueCollection
        'Dim Headers As Dictionary(Of String, String) = New Dictionary(Of String, String)
        Headers.Add("Key: ", APIKey)
        Headers.Add("Sign: ", bvxEncode.HashStringHMAC_SHA512(PostData, APISecret))

        Return WebAdapter.SendRequest(PrivateURL, PostData, Headers)

    End Function





    Public Function Teste() As String

        Return bvxEncode.HashStringHMAC_SHA512("testestring", APISecret)


    End Function

    Public Function Balance() As String

        command.Add("command", "balance")
        Return MountPostData(command)

    End Function

    Public Function Ticker() As String

        Try
            Return WebAdapter.SendRequest(PublicURL + "/ticker")
        Catch ex As Exception
            Return "***Error*** - " + ex.Message
        End Try

    End Function

   

End Class
