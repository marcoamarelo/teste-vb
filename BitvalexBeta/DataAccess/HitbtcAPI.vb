﻿Imports Teste1

' Documentação da API
' http://hitbtc.com/api
' https://api.hitbtc.com/

Public Class HitbtcAPI
    Inherits APIObject
    Implements IAPIDAO

    Public Sub New(apiKey As String, apiSecret As String, publicURL As String, privateURL As String, pExchangeName As String)

        MyBase.New(apiKey, apiSecret, publicURL, privateURL, pExchangeName)

    End Sub

#Region "Interface Methods"

    Public Function GetMarketNameList() As List(Of String) Implements IAPIDAO.GetMarketNameList

        Dim list As List(Of HitbtcSymbol) = GetSymbols()
        Dim x = (From T1 In list.AsEnumerable Select T1.BaseCurrency + "_" + T1.QuoteCurrency).ToList()
        x.Sort()
        Return x

    End Function

    Public Function GetAskBidForMarket(pMarketName As String) As bvxPairPrices Implements IAPIDAO.GetAskBidForMarket
        'chamada http
        Dim obj As HitbtcMarketBookOrders = Nothing
        Dim result As bvxPairPrices = New bvxPairPrices(Nothing)
        Try
            obj = GetOrderBook(pMarketName.Replace("_", ""))
            If Not obj Is Nothing Then
                result.Ask = obj.Ask(0).Price
                result.AskVolume = obj.Ask(0).Size
                result.Bid = obj.Bid(0).Price
                result.BidVolume = obj.Bid(0).Size
                result.SetUpdateState(bvxThreadUpdatableObject.bvxUpdatableObjectStateList.UpToDate)
            End If
            Return result
        Catch ex As Exception
            result.ResetAskAndBidValues(bvxThreadUpdatableObject.bvxUpdatableObjectStateList.ErrorResponse)
            Return result
        End Try
    End Function
#End Region

#Region "Public Calls"

    'https://api.hitbtc.com/api/2/public/symbol
    Public Function GetSymbols() As List(Of HitbtcSymbol)
        Dim response = Request(Of List(Of HitbtcSymbol))(PublicURL + "/symbol")
        Return response
    End Function

    'https://api.hitbtc.com/api/2/public/orderbook/BCNBTC
    Public Function GetOrderBook(ByVal pMarket As String) As HitbtcMarketBookOrders
        Dim URL As String = PublicURL + "/orderbook/" + pMarket
        Dim response = Request(Of HitbtcMarketBookOrders)(URL)
        Return response
    End Function

#End Region



End Class
