﻿Imports MySql.Data.MySqlClient
Imports System.Data
Imports System.Data.Common
Imports System.Collections.Generic
Imports System.Collections

''' <summary>
''' Classe que gerencia o acesso ao banco de dados MySQL
''' </summary>
''' <remarks></remarks>
Public Class MySQLDataAccess
    Implements IDataAccess

    Private conn As MySqlConnection



    Public Sub Teste()



        Dim DatabaseName As String = "bitvalex"
        Dim server As String = "localhost"
        Dim userName As String = "bitvalex"
        Dim password As String = "f1792a"
        Dim Conn As MySqlConnection = New MySqlConnection()
        Conn.ConnectionString = String.Format("server={0}; user id={1}; password={2}; database={3}; pooling=false", server, userName, password, DatabaseName)
        Try
            conn.Open()
            MsgBox("Connected")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        conn.Close()


    End Sub


    Public Sub New()
        CreateConnection()
    End Sub

    Public Sub New(host As String, user As String, password As String, schema As String)
        CreateConnection(host, user, password, schema)
    End Sub

    ''' Inica a conexão com o banco de dados '''
    Public Sub OpenConnection() Implements IDataAccess.OpenConnection
        If Not conn Is Nothing And conn.State <> ConnectionState.Open Then
            conn.Open()
        End If
    End Sub

    ''' Termina a conexão com o banco de dados '''
    Public Sub CloseConnection() Implements IDataAccess.CloseConnection
        If Not conn Is Nothing And conn.State <> ConnectionState.Closed Then
            conn.Close()
        End If
    End Sub

    ''' Obtem o estado da conexão '''
    Public Function GetConnectionState() As ConnectionState Implements IDataAccess.GetConnectionState

        If Not conn Is Nothing Then
            Return conn.State
        End If

        Throw New InvalidOperationException("Object Connection is Null")

    End Function

    Private Sub CloseCommand(cmd As MySqlCommand)
        If Not cmd Is Nothing Then
            cmd.Dispose()
        End If
    End Sub

    ''' Inicia e retorna uma transação do banco de dados
    Public Function BeginTransaction() As DbTransaction Implements IDataAccess.BeginTransaction
        If Not conn Is Nothing And conn.State <> ConnectionState.Closed Then
            Return conn.BeginTransaction()
        Else
            Throw New Exception("Invalid operation call")
        End If
    End Function



    '' Cria uma conexão com o banco de dados
    Public Sub CreateConnection() Implements IDataAccess.CreateConnection

        conn = New MySqlConnection()

    End Sub

    Public Sub CreateConnection(host As String, user As String, password As String, schema As String)

        Dim s As String = "Server=" & host & "; Database=" & schema & "; Uid=" & user & "; Pwd=" & password & ";"

        conn = New MySqlConnection(s)

    End Sub


    ''' Execura uma comando SQL com retorno
    Public Function ExecuteQuery(sql As String, Optional parameters As Dictionary(Of String, Object) = Nothing, _
                          Optional transaction As DbTransaction = Nothing) _
    As DbDataReader Implements IDataAccess.ExecuteQuery

        Dim cmd As MySqlCommand
        Dim dr As MySqlDataReader
        cmd = CreateCommand(sql, parameters)
        Try
            dr = cmd.ExecuteReader()
        Finally
            'CloseCommand(cmd)
        End Try

        Return dr

    End Function


    Public Function GetDataTable(sql As String, Optional parameters As Dictionary(Of String, Object) = Nothing, _
                         Optional transaction As DbTransaction = Nothing) _
       As DataTable Implements IDataAccess.GetDataTable
        Dim oDt As DataTable = New DataTable()
        oDt.Load(ExecuteQuery(sql, parameters, transaction))
        Return oDt
    End Function


    ''' Executa uma comando sql (sql + parametros) e retorna o numero de linhas afetadas '''
    Public Function ExecuteNonQuery(sql As String, Optional parameters As Dictionary(Of String, Object) = Nothing, _
                          Optional transaction As DbTransaction = Nothing) _
        As Integer Implements IDataAccess.ExecuteNonQuery

        Dim cmd As MySqlCommand
        Dim result As Integer = -1

        cmd = CreateCommand(sql, parameters)
        cmd.Transaction = transaction
        Try
            result = cmd.ExecuteNonQuery()
        Finally
            CloseCommand(cmd)
        End Try

        Return result

    End Function

    ''' Configura o comando sql '''
    Private Function CreateCommand(sql As String, Optional parameters As Dictionary(Of String, Object) = Nothing) _
        As DbCommand

        Dim cmd As MySqlCommand
        Dim parameter As MySqlParameter

        cmd = New MySqlCommand(sql, conn)
        cmd.CommandType = CommandType.Text
        cmd.CommandTimeout = 10000

        If Not parameters Is Nothing Then

            For Each pair As KeyValuePair(Of String, Object) In parameters

                parameter = New MySqlParameter(pair.Key, pair.Value)
                cmd.Parameters.Add(parameter)

            Next
        End If

        Return cmd

    End Function

    Public Sub Dispose() Implements IDisposable.Dispose

        CloseConnection()

    End Sub



 
End Class
