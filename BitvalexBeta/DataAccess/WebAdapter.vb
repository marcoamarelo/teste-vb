﻿
Imports System.Net
Imports System.IO
Imports System.Collections.Specialized
Imports System.Text
Imports System.Runtime.Serialization





Public Class WebAdapter


    'get
    Public Function     (ByVal pURL As String) As String

        Dim oWebRequest As WebRequest
        Dim oWebResponse As WebResponse = Nothing
        Dim strBuffer As String = ""
        Dim objSR As StreamReader = Nothing

        Try
            LogService.Write("WebAdapter.SendRequest: " + pURL, LogService.Type.INFO)
            Dim TimeIni As DateTime = Now
            oWebRequest = HttpWebRequest.Create(pURL)
            oWebRequest.Method = "GET"
            oWebResponse = oWebRequest.GetResponse()
            'Le a resposta do web site e armazena em uma stream
            objSR = New StreamReader(oWebResponse.GetResponseStream)
            Dim TimeEnd As DateTime = Now
            strBuffer = objSR.ReadToEnd
            Dim EllapsedTime = TimeEnd - TimeIni
            LogService.Write("Request finished in : " + EllapsedTime.Seconds().ToString() + " seconds.", LogService.Type.INFO)
        Catch ex As Exception
            LogService.Write("***Error in WebAdapter.SendRequest. (URL: " + pURL + "). " + ex.Message, LogService.Type.FAILURE)
            Throw
        Finally
            If Not objSR Is Nothing Then objSR.Close()
            If Not oWebResponse Is Nothing Then oWebResponse.Close()
        End Try
        Return strBuffer

    End Function


    'get
    Public Function SendRequest(ByVal pURL As String, ByVal pPostData As String,
                                ByVal pHeader As NameValueCollection) As String

        Dim oWebRequest As WebRequest
        Dim oWebResponse As WebResponse = Nothing
        Dim strBuffer As String = ""
        Dim objSR As StreamReader = Nothing
        Dim encoding As New UTF8Encoding
        Dim byteData As Byte() = encoding.GetBytes(pPostData)

        Try
            oWebRequest = HttpWebRequest.Create(pURL)
            oWebRequest.Method = "POST"
            'oWebRequest.ContentType = "application/x-www-form-urlencoded"

            Dim myWebHeaderCollection As WebHeaderCollection = oWebRequest.Headers
            For i As Integer = 0 To pHeader.Count - 1
                myWebHeaderCollection.Add(pHeader.Keys(i) + ":" + pHeader.Item(i))
            Next


            'oWebRequest.Headers.Add(pHeader)

            oWebRequest.ContentLength = byteData.Length
            Dim postreqstream As Stream = oWebRequest.GetRequestStream()
            postreqstream.Write(byteData, 0, byteData.Length)
            postreqstream.Close()

            oWebResponse = oWebRequest.GetResponse()
            'Le a resposta do web site e armazena em uma stream
            objSR = New StreamReader(oWebResponse.GetResponseStream())
            strBuffer = objSR.ReadToEnd
        Catch ex As Exception
            Throw ex
        Finally
            objSR.Close()
            oWebResponse.Close()
        End Try
        Return strBuffer

    End Function





End Class
