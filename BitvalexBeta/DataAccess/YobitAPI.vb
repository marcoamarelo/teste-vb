﻿Imports System.Configuration
Imports System.Collections.Specialized
Imports System.ComponentModel
Imports Newtonsoft



Public Class YobitAPI
    Inherits APIObject
    Implements IAPIDAO

    Public Sub New(apiKey As String, apiSecret As String, publicURL As String, privateURL As String, pExchangeName As String)
        MyBase.New(apiKey, apiSecret, publicURL, privateURL, pExchangeName)
    End Sub

#Region "Interface Methods"



    Public Function GetMarketList() As List(Of String) _
        Implements IAPIDAO.GetMarketNameList

        Dim list As List(Of YobitInfoPair) = GetInfo()
        Return (From T1 In list.AsEnumerable Select T1.MarketName Order By MarketName).ToList()

    End Function


    Public Function GetAskBidForMarket(ByVal pMarketName As String) As bvxPairPrices _
                                  Implements IAPIDAO.GetAskBidForMarket
        'chamada http
        Dim obj As YobitDepth = Nothing
        Dim result As bvxPairPrices = New bvxPairPrices(Nothing)
        Try
            obj = GetDepth(pMarketName)
            If Not obj Is Nothing Then
                result.Ask = obj.Asks(0)(0)
                result.AskVolume = obj.Asks(0)(1)
                result.Bid = obj.Bids(0)(0)
                result.BidVolume = obj.Bids(0)(1)
                result.SetUpdateState(bvxThreadUpdatableObject.bvxUpdatableObjectStateList.UpToDate)
            End If
            Return result
        Catch ex As Exception
            result.ResetAskAndBidValues(bvxThreadUpdatableObject.bvxUpdatableObjectStateList.ErrorResponse)
            Return result
        End Try

    End Function


    'Public Function GetFeeList() As List(Of bvxExchangeCoinFee) _
    '    Implements IAPIDAO.GetFeeList

    '    '***TODO - verificar como trabalhar com esta lista que traz valores pa
    '    Dim list As List(Of bvxExchangeCoinFee) = New List(Of bvxExchangeCoinFee)
    '    Dim InfoPairList = GetInfo()
    '    For Each obj In InfoPairList
    '        Dim fee As bvxExchangeCoinFee = New bvxExchangeCoinFee()
    '        fee.Fee = obj.Fee
    '        'a primeira moeda do par é 
    '        fee.Coin = obj.MarketName.Substring(0, obj.MarketName.IndexOf("_"))
    '        fee.Exchange = Me.ExchangeName
    '        list.Add(fee)
    '        fee.Pair = obj.MarketName
    '    Next
    '    Return list

    'End Function

#End Region


    'ticker
    'https://yobit.net/api/3/ticker/ltc_btc
    Public Function GetTicker(ByVal pMarketName As String) As YobitTicker

        Dim pRawJson = WebAdapter.SendRequest(PublicURL + "/ticker/" + pMarketName)
        Dim x = Newtonsoft.Json.JsonConvert.DeserializeObject(pRawJson)
        Dim y = x.Last.Last

        'Tinha que ter Factory para essas coiass também
        Dim w = JsonFactory.CreateJsonObject(y.ToString).ToClass(Of YobitTicker)
        w.MarketName = pMarketName
        Return w

    End Function


    'https://yobit.net/api/3/info
    Public Function GetInfo() As List(Of YobitInfoPair)

        Dim pRawJson = WebAdapter.SendRequest(PublicURL + "/info")
        Dim x = Newtonsoft.Json.JsonConvert.DeserializeObject(pRawJson)
        Dim y = x.Last.Last
        Dim list As New List(Of YobitInfoPair)

        For Each z In y
            Dim w = JsonFactory.CreateJsonObject(z.First.ToString).ToClass(Of YobitInfoPair)
            w.MarketName = z.Name
            list.Add(w)
        Next
        Return list

    End Function


    'https://yobit.net/api/3/trades/ltc_btc
    Public Function GetTrades(ByVal pMarketName As String) As List(Of YobitTrade)

        Dim pRawJson = WebAdapter.SendRequest(PublicURL + "/trades/" + pMarketName)
        Dim x = Newtonsoft.Json.JsonConvert.DeserializeObject(pRawJson)
        Dim y = x.Last.Last
        Dim list As New List(Of YobitTrade)

        For Each z In y
            Dim w = JsonFactory.CreateJsonObject(z.ToString).ToClass(Of YobitTrade)
            w.MarketName = pMarketName
            list.Add(w)
        Next
        Return list

    End Function


    'https://yobit.net/api/3/depth/ltc_btc
    Public Function GetDepth(ByVal pMarketName As String) As YobitDepth

        Dim pRawJson = WebAdapter.SendRequest(PublicURL + "/depth/" + pMarketName)
        Dim x = Newtonsoft.Json.JsonConvert.DeserializeObject(pRawJson)
        Dim y = x.Last.Last

        Dim z = JsonFactory.CreateJsonObject(y.ToString).ToClass(Of YobitDepth)
        z.MarketName = pMarketName
        Return z

    End Function
End Class
