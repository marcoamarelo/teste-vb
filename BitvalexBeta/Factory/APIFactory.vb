﻿Imports System.Configuration

Public Class APIFactory
    Public Shared Function CreateAPIObject(exchange As String, config As System.Configuration.AppSettingsReader) As APIObject
        Select Case exchange
            Case "Binance"
                Return New BinanceAPI(
                    config.GetValue("API_KEY_BINANCE", GetType(String)),
                    config.GetValue("API_SECRET_BINANCE", GetType(String)),
                    config.GetValue("API_PUBLIC_URL_BINANCE", GetType(String)),
                    config.GetValue("API_PRIVATE_URL_BINANCE", GetType(String)),
                    "Binance")
            Case "Bleutrade"
                Return New BleutradeAPI(
                    config.GetValue("API_KEY_BLEUTRADE", GetType(String)),
                    config.GetValue("API_SECRET_BLEUTRADE", GetType(String)),
                    config.GetValue("API_PUBLIC_URL_BLEUTRADE", GetType(String)),
                    config.GetValue("API_PRIVATE_URL_BLEUTRADE", GetType(String)),
                    "Bleutrade")
            Case "Brasiliex"
                Return New BrasiliexAPI(
                    config.GetValue("API_KEY_BRASILIEX", GetType(String)),
                    config.GetValue("API_SECRET_BRASILIEX", GetType(String)),
                    config.GetValue("API_PUBLIC_URL_BRASILIEX", GetType(String)),
                    config.GetValue("API_PRIVATE_URL_BRASILIEX", GetType(String)),
                    "Brasiliex")
            Case "Hitbtc"
                Return New HitbtcAPI(config.GetValue("API_KEY_HITBTC", GetType(String)),
                    config.GetValue("API_SECRET_HITBTC", GetType(String)),
                    config.GetValue("API_PUBLIC_URL_HITBTC", GetType(String)),
                    config.GetValue("API_PRIVATE_URL_HITBTC", GetType(String)),
                    "Hitbtc")
            Case "Yobit"
                Return New YobitAPI(
                    config.GetValue("API_KEY_YOBIT", GetType(String)),
                    config.GetValue("API_SECRET_YOBIT", GetType(String)),
                    config.GetValue("API_PUBLIC_URL_YOBIT", GetType(String)),
                    config.GetValue("API_PRIVATE_URL_YOBIT", GetType(String)),
                    "Yobit")
        End Select
        Return Nothing
    End Function

    Public Shared Function CreateAPIObject(exchange As String)
        Return CreateAPIObject(exchange, AppSettingsReaderSingleton.Instance)
    End Function
End Class
