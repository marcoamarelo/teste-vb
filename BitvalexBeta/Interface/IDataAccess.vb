﻿Imports System.Data
Imports System.Collections.Generic
Imports System.Data.Common
Imports System.Collections

''' <summary>
''' Interface que define como deve ser a estrutura das classes de acesso aos dados da aplicação
''' </summary>
''' <remarks></remarks>
Public Interface IDataAccess
    Inherits IDisposable

    ''' <summary>
    ''' Cria conexão com o banco de dados
    ''' </summary>
    ''' <remarks></remarks>
    Sub CreateConnection()

    ''' <summary>
    ''' Abre conexão com o banco de dados
    ''' </summary>
    ''' <remarks></remarks>
    Sub OpenConnection()

    ''' <summary>
    ''' Fecha conexão com o banco de dados
    ''' </summary>
    ''' <remarks></remarks>
    Sub CloseConnection()

    ''' <summary>
    ''' Retorna o estado da conexão
    ''' </summary>
    ''' <remarks></remarks>
    Function GetConnectionState() As ConnectionState

    ''' <summary>
    ''' Executa comando SQL que retorna um conjunto de valores
    ''' </summary>
    ''' <param name="sql"></param>
    ''' <param name="parameters"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ExecuteQuery(sql As String, Optional parameters As Dictionary(Of String, Object) = Nothing, _
                          Optional transaction As DbTransaction = Nothing) As DbDataReader

    ''' <summary>
    ''' Retorna um datatable vindo de um sql passado como parâmetro
    ''' </summary>
    ''' <param name="sql"></param>
    ''' <param name="parameters"></param>
    ''' <param name="transaction"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GetDataTable(sql As String, Optional parameters As Dictionary(Of String, Object) = Nothing, _
                          Optional transaction As DbTransaction = Nothing) As DataTable



    ''' <summary>
    ''' Executa comando SQL que retorna o numero de linhas modificadas por ele
    ''' </summary>
    ''' <param name="sql"></param>
    ''' <param name="parameters"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ExecuteNonQuery(ByVal sql As String, Optional ByVal parameters As Dictionary(Of String, Object) = Nothing, _
                          Optional ByVal transaction As DbTransaction = Nothing) As Integer




    ''' <summary>
    ''' Inicia uma transação no banco de dados
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function BeginTransaction() As DbTransaction

End Interface
