﻿Imports Newtonsoft.Json

Public Class BinanceBuySell

    'Não tem Quantity, nem Rate

    <JsonProperty("")>
    Public Property Price As Double

    <JsonProperty("")>
    Public Property Amount As Double

    <JsonProperty("")>
    Public Property Indeterminado As List(Of String)

End Class
