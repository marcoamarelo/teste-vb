﻿Imports Newtonsoft.Json

Public Class BinanceDepthResult

    <JsonProperty("lastUpdateId")>
    Public Property LastUpdateId As Integer

    <JsonProperty("bids")>
    Public Property Bids As Object()()

    <JsonProperty("asks")>
    Public Property Asks As Object()()

End Class
