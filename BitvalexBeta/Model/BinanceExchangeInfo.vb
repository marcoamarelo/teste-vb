﻿Imports Newtonsoft.Json

Public Class BinanceExchangeInfo

    <JsonProperty("symbol")>
    Public Property symbol As String

    <JsonProperty("status")>
    Public Property status As String

    <JsonProperty("baseAsset")>
    Public Property baseAsset As String

    <JsonProperty("baseAssetPrecision")>
    Public Property baseAssetPrecision As Integer

    <JsonProperty("quoteAsset")>
    Public Property quoteAsset As String

    <JsonProperty("quotePrecision")>
    Public Property quotePrecision As Integer

    <JsonProperty("tickSize")>
    Public Property tickSize As Double

End Class
