﻿Imports Newtonsoft.Json

Public Class BinanceFilter

    <JsonProperty("filterType")>
    Public Property FilterType As String

    <JsonProperty("minPrice")>
    Public Property MinPrice As String

    <JsonProperty("maxPrice")>
    Public Property MaxPrice As String

    <JsonProperty("tickSize")>
    Public Property TickSize As String

    <JsonProperty("minQty")>
    Public Property MinQty As String

    <JsonProperty("maxQty")>
    Public Property MaxQty As String

    <JsonProperty("stepSize")>
    Public Property StepSize As String

    <JsonProperty("minNotional")>
    Public Property MinNotional As String

End Class
