﻿Imports Newtonsoft.Json

Public Class BinanceInfoResult

    <JsonProperty("timezone")>
    Public Property Timezone As String

    <JsonProperty("serverTime")>
    Public Property ServerTime As Long

    <JsonProperty("rateLimits")>
    Public Property RateLimits As BinanceRateLimit()

    <JsonProperty("exchangeFilters")>
    Public Property ExchangeFilters As Object()

    <JsonProperty("symbols")>
    Public Property Symbols As BinanceSymbol()

End Class
