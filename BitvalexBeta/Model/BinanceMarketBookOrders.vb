﻿Imports Newtonsoft.Json

'https://www.binance.com/api/v1/depth?symbol=BNBBTC&limit=10

Public Class BinanceMarketBookOrders

    <JsonProperty("bids")>
    Public Property Bids As List(Of BinanceBuySell)

    <JsonProperty("asks")>
    Public Property Asks As List(Of BinanceBuySell)

End Class
