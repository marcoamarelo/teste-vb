﻿Imports Newtonsoft.Json

Public Class BinanceRateLimit

    <JsonProperty("rateLimitType")>
    Public Property RateLimitType As String

    <JsonProperty("interval")>
    Public Property Interval As String

    <JsonProperty("limit")>
    Public Property Limit As Integer

End Class
