﻿Imports Newtonsoft.Json

Public Class BinanceSymbol

    <JsonProperty("symbol")>
    Public Property Symbol As String

    <JsonProperty("status")>
    Public Property Status As String

    <JsonProperty("baseAsset")>
    Public Property BaseAsset As String

    <JsonProperty("baseAssetPrecision")>
    Public Property BaseAssetPrecision As Integer

    <JsonProperty("quoteAsset")>
    Public Property QuoteAsset As String

    <JsonProperty("quotePrecision")>
    Public Property QuotePrecision As Integer

    <JsonProperty("orderTypes")>
    Public Property OrderTypes As String()

    <JsonProperty("icebergAllowed")>
    Public Property IcebergAllowed As Boolean

    <JsonProperty("filters")>
    Public Property Filters As BinanceFilter()

End Class
