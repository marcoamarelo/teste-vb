﻿Imports Newtonsoft.Json

Public Class BleutradeBalance


    <JsonProperty("Currency")>
    Public Property Currency As String

    <JsonProperty("Balance")>
    Public Property Balance As String

    <JsonProperty("Available")>
    Public Property Available As String

    <JsonProperty("Pending")>
    Public Property Pending As String

    <JsonProperty("CryptoAddress")>
    Public Property CryptoAddress As Object

    <JsonProperty("IsActive")>
    Public Property IsActive As String

    <JsonProperty("AllowDeposit")>
    Public Property AllowDeposit As String

    <JsonProperty("AllowWithdraw")>
    Public Property AllowWithdraw As String

End Class
