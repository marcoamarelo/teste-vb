﻿Imports Newtonsoft.Json

Public Class BleutradeCandle

    <JsonProperty("TimeStamp")>
    Public Property TimeStamp As String

    <JsonProperty("Open")>
    Public Property Open As String

    <JsonProperty("High")>
    Public Property High As String

    <JsonProperty("Low")>
    Public Property Low As String

    <JsonProperty("Close")>
    Public Property Close As String

    <JsonProperty("Volume")>
    Public Property Volume As String

    <JsonProperty("BaseVolume")>
    Public Property BaseVolume As String

End Class
