﻿Imports Newtonsoft.Json

Public Class BleutradeCurrency

    <JsonProperty("Currency")>
    Public Property Currency As String

    <JsonProperty("CurrencyLong")>
    Public Property CurrencyLong As String

    <JsonProperty("MinConfirmation")>
    Public Property MinConfirmation As String

    <JsonProperty("TxFee")>
    Public Property TxFee As String

    <JsonProperty("CoinType")>
    Public Property CoinType As String

    <JsonProperty("IsActive")>
    Public Property IsActive As String

    <JsonProperty("MaintenanceMode")>
    Public Property MaintenanceMode As String


End Class


