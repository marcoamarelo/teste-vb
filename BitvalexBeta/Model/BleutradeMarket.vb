﻿Imports Newtonsoft.Json

Public Class BleutradeMarket

    <JsonProperty("MarketCurrency")>
    Public Property MarketCurrency As String

    <JsonProperty("BaseCurrency")>
    Public Property BaseCurrency As String

    <JsonProperty("MarketCurrencyLong")>
    Public Property MarketCurrencyLong As String

    <JsonProperty("BaseCurrencyLong")>
    Public Property BaseCurrencyLong As String

    <JsonProperty("MinTradeSize")>
    Public Property MinTradeSize As Double

    <JsonProperty("MarketName")>
    Public Property MarketName As String

    <JsonProperty("IsActive")>
    Public Property IsActive As Boolean

End Class
