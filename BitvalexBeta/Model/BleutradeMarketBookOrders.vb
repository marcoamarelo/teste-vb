﻿Imports Newtonsoft.Json

Public Class BleutradeMarketBookOrders

    <JsonProperty("buy")>
    Public Property Buy As List(Of BleutrdeBuySell)

    <JsonProperty("sell")>
    Public Property Sell As List(Of BleutrdeBuySell)

End Class
