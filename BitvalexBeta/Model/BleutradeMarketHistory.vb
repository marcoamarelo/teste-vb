﻿Imports Newtonsoft.Json

Public Class BleutradeMarketHistory

    <JsonProperty("TimeStamp")>
    Public Property TimeStamp As String

    <JsonProperty("Quantity")>
    Public Property Quantity As Double

    <JsonProperty("Price")>
    Public Property Price As Double

    <JsonProperty("Total")>
    Public Property Total As Double

    <JsonProperty("OrderType")>
    Public Property OrderType As String

End Class
