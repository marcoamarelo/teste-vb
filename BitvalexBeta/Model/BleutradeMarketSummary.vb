﻿Imports Newtonsoft.Json

Public Class BleutradeMarketSummary

    <JsonProperty("MarketCurrency")>
    Public Property MarketCurrency As String

    <JsonProperty("BaseCurrency")>
    Public Property BaseCurrency As String

    <JsonProperty("MarketName")>
    Public Property MarketName As String

    <JsonProperty("PrevDay")>
    Public Property PrevDay As Double

    <JsonProperty("High")>
    Public Property High As Double

    <JsonProperty("Low")>
    Public Property Low As Double

    <JsonProperty("Last")>
    Public Property Last As Double

    <JsonProperty("Average")>
    Public Property Average As Double

    <JsonProperty("Volume")>
    Public Property Volume As Double

    <JsonProperty("BaseVolume")>
    Public Property BaseVolume As Double

    <JsonProperty("TimeStamp")>
    Public Property TimeStamp As String

    <JsonProperty("Bid")>
    Public Property Bid As Double

    <JsonProperty("Ask")>
    Public Property Ask As Double

    <JsonProperty("IsActive")>
    Public Property IsActive As Boolean

End Class
