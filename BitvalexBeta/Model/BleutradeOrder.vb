﻿Imports Newtonsoft.Json

Public Class BleutradeOrder

    <JsonProperty("OrderId")>
    Public Property OrderId As String

    <JsonProperty("Exchange")>
    Public Property Exchange As String

    <JsonProperty("Type")>
    Public Property Type As String

    <JsonProperty("Quantity")>
    Public Property Quantity As Double

    <JsonProperty("QuantityRemaining")>
    Public Property QuantityRemaining As Double

    <JsonProperty("QuantityBaseTraded")>
    Public Property QuantityBaseTraded As String

    <JsonProperty("Price")>
    Public Property Price As Double

    <JsonProperty("Status")>
    Public Property Status As String

    <JsonProperty("Created")>
    Public Property Created As String

    <JsonProperty("Comments")>
    Public Property Comments As String

End Class
