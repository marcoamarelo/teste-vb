﻿Imports Newtonsoft.Json

Public Class BleutradeOrderIdResult

    <JsonProperty("orderid")>
    Public Property Orderid As String

End Class
