﻿Imports Newtonsoft.Json

Public Class BleutradeResponseList(Of TModel As Class)

    <JsonProperty("success")>
    Public Property Success As String

    <JsonProperty("message")>
    Public Property Message As String

    <JsonProperty("result")>
    Public Property Results As List(Of TModel)


End Class

