﻿Imports Newtonsoft.Json


Public Class BleutradeResponseSingle(Of TModel As Class)

    <JsonProperty("success")>
    Public Property Success As String

    <JsonProperty("message")>
    Public Property Message As String

    <JsonProperty("result")>
    Public Property Result As TModel

End Class
