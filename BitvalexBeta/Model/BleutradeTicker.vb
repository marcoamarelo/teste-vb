﻿Imports Newtonsoft.Json

Public Class BleutradeTicker

    <JsonProperty("Bid")>
    Public Property Bid As Double

    <JsonProperty("Ask")>
    Public Property Ask As Double

    <JsonProperty("Last")>
    Public Property Last As Double

End Class
