﻿Imports Newtonsoft.Json

Public Class BleutrdeBuySell

    <JsonProperty("Quantity")>
    Public Property Quantity As Double

    <JsonProperty("Rate")>
    Public Property Rate As Double

End Class
