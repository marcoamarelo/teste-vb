﻿Imports Newtonsoft.Json

Public Class HitbtcAskBid

    <JsonProperty("price")>
    Public Property Price As Double

    <JsonProperty("size")>
    Public Property Size As Double

End Class
