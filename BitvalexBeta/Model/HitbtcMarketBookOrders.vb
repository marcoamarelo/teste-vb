﻿Imports Newtonsoft.Json

Public Class HitbtcMarketBookOrders

    <JsonProperty("ask")>
    Public Property Ask As List(Of HitbtcAskBid)

    <JsonProperty("bid")>
    Public Property Bid As List(Of HitbtcAskBid)

End Class
