﻿Imports Newtonsoft.Json

Public Class HitbtcSymbol

    <JsonProperty("id")>
    Public Property Id As String

    <JsonProperty("baseCurrency")>
    Public Property BaseCurrency As String

    <JsonProperty("quoteCurrency")>
    Public Property QuoteCurrency As String

    <JsonProperty("quantityIncrement")>
    Public Property QuantityIncrement As String

    <JsonProperty("tickSize")>
    Public Property TickSize As String

    <JsonProperty("takeLiquidityRate")>
    Public Property TakeLiquidityRate As String

    <JsonProperty("provideLiquidityRate")>
    Public Property ProvideLiquidityRate As String

    <JsonProperty("feeCurrency")>
    Public Property FeeCurrency As String


End Class
