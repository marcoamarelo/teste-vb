﻿Imports Newtonsoft.Json

Public Class YobitDepth

    Public Property MarketName As String

    <JsonProperty("asks")>
    Public Property Asks As Double()()

    <JsonProperty("bids")>
    Public Property Bids As Double()()

End Class
