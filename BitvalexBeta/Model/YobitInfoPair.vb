﻿Imports Newtonsoft.Json


Public Class YobitInfoPair

    Public Property MarketName As String

    <JsonProperty("decimal_places")>
    Public Property DecimalPlaces As Integer

    <JsonProperty("min_price")>
    Public Property MinPrice As Double

    <JsonProperty("max_price")>
    Public Property MaxPrice As Integer

    <JsonProperty("min_amount")>
    Public Property MinAmount As Double

    <JsonProperty("min_total")>
    Public Property MinTotal As Double

    <JsonProperty("hidden")>
    Public Property Hidden As Integer

    <JsonProperty("fee")>
    Public Property Fee As Double

    <JsonProperty("fee_buyer")>
    Public Property FeeBuyer As Double

    <JsonProperty("fee_seller")>
    Public Property FeeSeller As Double

End Class
