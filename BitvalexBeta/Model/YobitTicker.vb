﻿Imports Newtonsoft.Json

Public Class YobitTicker

    Public Property MarketName As String

    <JsonProperty("high")>
    Public Property High As Double

    <JsonProperty("low")>
    Public Property Low As Double

    <JsonProperty("avg")>
    Public Property Avg As Double

    <JsonProperty("vol")>
    Public Property Vol As Double

    <JsonProperty("vol_cur")>
    Public Property VolCur As Double

    <JsonProperty("last")>
    Public Property Last As Double

    <JsonProperty("buy")>
    Public Property Buy As Double

    <JsonProperty("sell")>
    Public Property Sell As Double

    <JsonProperty("updated")>
    Public Property Updated As Integer

    <JsonProperty("server_time")>
    Public Property ServerTime As Integer

End Class
