﻿Imports Newtonsoft.Json

Public Class YobitTickerResponse(Of TModel As Class)

    <JsonProperty("PAIR")>
    Public Property Ticker As TModel

End Class
