﻿Imports Newtonsoft.Json


Public Class YobitTrade

    Public Property MarketName As String

    <JsonProperty("type")>
    Public Property Type As String

    <JsonProperty("price")>
    Public Property Price As Double

    <JsonProperty("amount")>
    Public Property Amount As Double

    <JsonProperty("tid")>
    Public Property Tid As Integer

    <JsonProperty("timestamp")>
    Public Property Timestamp As Integer
End Class
