﻿Imports Newtonsoft.Json

Public Class bvxArbitBetween

    <JsonProperty("Exchange")>
    Public Property Exchange As String

    <JsonProperty("Fee")>
    Public Property Fee As Double

    <JsonProperty("Wallet")>
    Public Property Wallet As String

    Public Property Valid As Boolean = True
    Public Property InvalidReason As String

End Class
