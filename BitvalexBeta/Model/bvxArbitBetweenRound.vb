﻿Public Class bvxArbitBetweenRound
    Inherits bvxThreadUpdatableObject

    Property Exchange1 As bvxExchange
    Property Exchange2 As bvxExchange
    Property FeeExchange1 As Double
    Property FeeExchange2 As Double

    Property Wallet1 As String()
    Property Wallet2 As String()
    Public Property Legs As List(Of bvxArbitBetweenRoundLeg)
    Public Property Valid As Boolean = True
    Public Property InvalidReason As String
    Public Property HasOportunity As Boolean = False



    Public Sub New()
        Legs = New List(Of bvxArbitBetweenRoundLeg)
    End Sub

End Class
