﻿Public Class bvxArbitBetweenRoundLeg
    Inherits bvxThreadUpdatableObject

    Property MarketName As String
    Property Coin1 As String
    Property Coin2 As String

    Property PairPricesExchange1 As bvxPairPrices
    Property PairPricesExchange2 As bvxPairPrices

    Public Property Rate1To2 As Double
    Public Property Rate2To1 As Double

    Public Property LowerVolume As Double
    
    Public Property LowerVolumeExchange As Integer

    Public Property LogOperation As String

    Public Property IsOportunity As Boolean = False

    Public Property Valid As Boolean = True
    Public Property InvalidReason As String



End Class
