﻿Imports Newtonsoft.Json

Public Class bvxArbitIntra


    <JsonProperty("Exchange")>
    Public Property Exchange As String
    Public Property ObjExchange As bvxExchange
    <JsonProperty("Arbit_Intra")>
    Public Property MarketRounds_Text As String()

    <JsonProperty("OportunityPercent")>
    Public Property OportunityPercent As Double

    <JsonProperty("Fee")>
    Public Property Fee As Double

    Public Property Rounds As List(Of bvxArtbitIntraRound)
    Public Property HasOportunity As Boolean = False


    'lista de rounds distindos por market name, que será atualizada nas chamadas de refresh 
    'e propagará a informação de preço e volume para a lista Rounds, que possuem itens repetidos
    'esta propriedade visa diminuir o tempo de chamadas web, evitando chamadas duplicadas para mercados
    'Public Property MarketLegMaster As List(Of bvxArtbitIntraRoundLeg)

    Public Property Valid As Boolean = True
    Public Property InvalidReason As String

    'Public Property FeeList As List(Of bvxExchangeCoinFee)


    Public Sub New()
        Rounds = New List(Of bvxArtbitIntraRound)
    End Sub




End Class

