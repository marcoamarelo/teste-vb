﻿Public Class bvxArtbitIntraRound
    Inherits bvxThreadUpdatableObject

    Public Property Coins As String
    Public Property Rate As Double
    Public Property RateNoFee As Double
    Public Property Legs As List(Of bvxArbitIntraRoundLeg)

    Public Property Parent As bvxArbitIntra
    Public Property IsOportunity As Boolean = False
    Public Property Valid As Boolean = True
    Public Property InvalidReason As String
    Public Property LogOperation As String

    Public Property LowerVolume As Double
    Public Property LowerVolumeLegIndex As Integer


    Public Sub New(ByRef pParent As Object)

        Legs = New List(Of bvxArbitIntraRoundLeg)
        Parent = pParent

    End Sub


    Public Function GetClone() As bvxArtbitIntraRound

        Dim oNew = New bvxArtbitIntraRound(Me.Parent)
        oNew.Coins = Me.Coins
        oNew.Rate = Me.Rate
        oNew.RateNoFee = Me.RateNoFee
        oNew.Valid = Me.Valid
        oNew.InvalidReason = Me.InvalidReason
        oNew.LogOperation = Me.LogOperation
        oNew.LastUpdate = Me.LastUpdate
        oNew.ActualUpdateStatus = Me.ActualUpdateStatus

        For Each oLeg In Me.Legs
            oNew.Legs.Add(oLeg.GetClone(oNew))
        Next

        Return oNew

    End Function

End Class