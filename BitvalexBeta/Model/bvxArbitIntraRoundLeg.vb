﻿Public Class bvxArbitIntraRoundLeg
    Inherits bvxThreadUpdatableObject

    'informações carregadas no load inicial
    Public Property Parent As bvxArtbitIntraRound
    Public Property PairPrices As bvxPairPrices

    'propriedades para testes
    Public Property TesteBidAsk As Double
    Public Property TesteVK As Double
    Public Property TesteVolume As Double
    Public Property TesteVolumeWithFee As Double

    Public Order As bvxOrder



    Public Sub New(ByRef pParent As Object)
        Parent = pParent
    End Sub


    Public Function GetClone(ByVal pParent As bvxArtbitIntraRound) As bvxArbitIntraRoundLeg
        Dim oNew = New bvxArbitIntraRoundLeg(pParent)
        oNew.PairPrices.MarketName = Me.PairPrices.MarketName
        oNew.PairPrices.Coin1 = Me.PairPrices.Coin1
        oNew.PairPrices.Coin2 = Me.PairPrices.Coin2
        oNew.PairPrices.BaseCoin = Me.PairPrices.BaseCoin
        oNew.PairPrices.Ask = Me.PairPrices.Ask
        oNew.PairPrices.AskVolume = Me.PairPrices.AskVolume
        oNew.PairPrices.Bid = Me.PairPrices.Bid
        oNew.PairPrices.BidVolume = Me.PairPrices.BidVolume
        oNew.PairPrices.MarketNameInverted = Me.PairPrices.MarketNameInverted
        oNew.LastUpdate = Me.LastUpdate
        oNew.ActualUpdateStatus = Me.ActualUpdateStatus
        Return oNew
    End Function


    Public Sub SetAskAndBidValues(ByVal pAsk As Double, ByVal pBid As Double, ByVal pAskVolume As Double, ByVal pBidVolume As Double,
                                  ByVal pUpdateState As bvxUpdatableObjectStateList)
        PairPrices.Ask = pAsk
        PairPrices.Bid = pBid
        PairPrices.AskVolume = pAskVolume
        PairPrices.BidVolume = pBidVolume
        'Valid = pValidLeg
        SetUpdateState(pUpdateState)
    End Sub


    Public Sub SetAskAndBidValues(ByVal pLeg As bvxArbitIntraRoundLeg)
        SetAskAndBidValues(pLeg.PairPrices.Ask, pLeg.PairPrices.Bid, pLeg.PairPrices.AskVolume, pLeg.PairPrices.BidVolume, pLeg.ActualUpdateStatus)
    End Sub

    Public Sub ResetAskAndBidValues(pUpdateStatus As bvxUpdatableObjectStateList)

        SetAskAndBidValues(0, 0, 0, 0, pUpdateStatus)

    End Sub






End Class
