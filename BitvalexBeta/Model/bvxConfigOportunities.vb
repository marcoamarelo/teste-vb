﻿Imports Newtonsoft.Json


Public Class bvxConfigOportunities

    Public Enum bvxConfigOportunitiesState
        Starting = 0
        Ready = 1
        Updating = 2
    End Enum


    <JsonProperty("FileName")>
    Public Property FileName As String

    <JsonProperty("BetweenOportunityPercent")>
    Public Property BetweenOportunityPercent As Double

    <JsonProperty("Intra")>
    Public Property Intra As List(Of bvxArbitIntra)

    <JsonProperty("Between")>
    Public Property Between As List(Of bvxArbitBetween)
    Public Property BetweenRounds As List(Of bvxArbitBetweenRound)
    Public Property Exchanges As List(Of bvxExchange)

    Public Property State As bvxConfigOportunitiesState
    Public Property PersistInDatabase As Boolean

    Public Sub New()

        State = bvxConfigOportunitiesState.Starting
        PersistInDatabase = False
        Exchanges = New List(Of bvxExchange)
        BetweenRounds = New List(Of bvxArbitBetweenRound)

    End Sub









End Class
