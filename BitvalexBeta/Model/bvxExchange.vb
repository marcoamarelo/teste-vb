﻿Imports Newtonsoft.Json

Public Class bvxExchange

    Private _Name As String = ""

    <JsonProperty("Id")>
    Public Property Id As String
    <JsonProperty("Name")>
    Public Property Name As String
        Get
            Return _Name
        End Get
        Set(ByVal value As String)
            _Name = value
            API = APIFactory.CreateAPIObject(value)
        End Set
    End Property



    Public Property MarketList As List(Of String)
    Public Property API As IAPIDAO
    Public Property Valid As Boolean = True
    Public Property InvalidReason As String

    Public Property PairListMaster As List(Of bvxPairPrices)

    Public Sub New()

        'If Not String.IsNullOrEmpty(Name) Then
        '    API = APIFactory.CreateAPIObject(Name)
        'End If
        PairListMaster = New List(Of bvxPairPrices)

    End Sub

    Public Sub New(pId As Integer, pName As String)

        Id = pId
        Name = pName
        PairListMaster = New List(Of bvxPairPrices)
        'API = APIFactory.CreateAPIObject(pName)

    End Sub

End Class
