﻿Public Class bvxMarketSplited

    Public Property MarketName As String
    Public Property CoinA As String
    Public Property CoinB As String


    Public Sub New(pMarketName As String)

        MarketName = pMarketName
        Dim x() As String = pMarketName.Split("_")
        If x.Count = 2 Then
            CoinA = x(0)
            CoinB = x(1)
        End If

    End Sub

End Class
