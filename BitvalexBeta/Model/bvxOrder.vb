﻿Public Class bvxOrder


    Public Enum bvxBuyOrSellList
        Buy = 0
        Sell = 1
    End Enum

    Property MarketName As String
    Property Rate As Decimal
    Property Quantity As Decimal
    Property BuyOrSell As bvxBuyOrSellList


    Public Sub SetOrder(pMarketName As String, pRate As Decimal, pQuantity As Decimal, pBuyOrSell As bvxBuyOrSellList)
        MarketName = pMarketName
        Rate = pRate
        Quantity = pQuantity
        BuyOrSell = pBuyOrSell
    End Sub


End Class
