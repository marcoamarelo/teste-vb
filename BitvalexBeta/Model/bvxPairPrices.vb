﻿Public Class bvxPairPrices
    Inherits bvxThreadUpdatableObject


    Public Property Parent As bvxExchange
    Public Property MarketName As String
    Public Property Coin1 As String
    Public Property Coin2 As String
    Public Property BaseCoin As String
    'informações que são carregadas a cada refresh
    Public Property Ask As Double
    Public Property AskVolume As Double
    Public Property Bid As Double
    Public Property BidVolume As Double
    'propriedade que indica qual lado das ofertas deve ser usado no cálculo
    'TRUE - Usar ASK e * na operaçao
    'FALSE - Usar BID e / na operaçao
    Public Property MarketNameInverted As Boolean

    'orderm preparada para ser enviada
    Public Order As bvxOrder

    Public Property Valid As Boolean = True
    Public Property InvalidReason As String


    Public Sub New(ByRef pParent As Object)
        Parent = pParent
        Order = New bvxOrder

    End Sub


    Public Sub SetAskAndBidValues(ByVal pAsk As Double, ByVal pBid As Double, ByVal pAskVolume As Double, ByVal pBidVolume As Double,
                                  ByVal pUpdateState As bvxUpdatableObjectStateList)
        Ask = pAsk
        Bid = pBid
        AskVolume = pAskVolume
        BidVolume = pBidVolume

        'Order.SetOrder(MarketName, )
        'Valid = pValidLeg
        SetUpdateState(pUpdateState)
    End Sub


    Public Sub SetAskAndBidValues(ByVal pPair As bvxPairPrices)
        SetAskAndBidValues(pPair.Ask, pPair.Bid, pPair.AskVolume, pPair.BidVolume, pPair.ActualUpdateStatus)
    End Sub


    Public Sub ResetAskAndBidValues(pUpdateStatus As bvxUpdatableObjectStateList)
        SetAskAndBidValues(0, 0, 0, 0, pUpdateStatus)
    End Sub

End Class
