﻿Public Class bvxProcessManager

#Region "Events"

    Public Delegate Sub RefreshFinishedDelegate(ByVal pConfigOportunities As bvxConfigOportunities)
    Public Delegate Sub MessageEventDelegate(ByVal Sender As Object, ByVal pMessage As String, ByVal pMessageLevel As Integer)

    Public Event OnRefreshFinished As RefreshFinishedDelegate
    Public Event OnMessageEvent As MessageEventDelegate


    Private Sub RaiseRefreshFinished(ByVal objConfigOportunities As bvxConfigOportunities)

        RaiseEvent OnRefreshFinished(objConfigOportunities)

    End Sub

    Private Sub RaiseMessage(pMessage As String, pLevel As Integer)

        RaiseEvent OnMessageEvent(Me, pMessage, pLevel)

    End Sub

#End Region

#Region "Methods"



    'Public Sub Start_RefreshIntraOportunities(pController As bvxArbitrageController)

    '    Dim objConfigOportunities = ConfigOportunitiesSingleton.Instance

    '    If objConfigOportunities.State = bvxConfigOportunities.bvxConfigOportunitiesState.Ready Then
    '        Try
    '            objConfigOportunities.State = bvxConfigOportunities.bvxConfigOportunitiesState.Updating
    '            RaiseMessage("Preparing events.", 3)
    '            SetMasterLegsEvents(objConfigOportunities, False)
    '            RaiseMessage("", 3)
    '            pController.RefreshIntraOportunities_Fase1(objConfigOportunities)
    '        Catch ex As Exception
    '            LogService.Write("***Error on bvxProcessManager.Start_RefreshIntraOportunities. " + Environment.NewLine +
    '                             ex.ToString(), LogService.Type.FATAL)
    '            Throw
    '        Finally

    '        End Try
    '    End If

    'End Sub


    'Private Sub SetMasterLegsEvents(pConfigOportunities As bvxConfigOportunities, pRemove As Boolean)

    '    For Each objIntra In pConfigOportunities.Intra
    '        For Each objLeg As bvxArtbitIntraRoundLeg In objIntra.MarketLegMaster
    '            If pRemove Then
    '                RemoveHandler objLeg.OnStateChanged, AddressOf OnLegStateChangedEventHandler
    '            Else
    '                AddHandler objLeg.OnStateChanged, AddressOf OnLegStateChangedEventHandler
    '            End If
    '        Next
    '    Next

    'End Sub


    'Private Sub OnLegStateChangedEventHandler(ByVal Sender As Object)

    '    'checa se todas as legs da exchange estao atualizadas e dispara a fase dois do refresh
    '    Dim objLeg As bvxArtbitIntraRoundLeg = Sender
    '    If objLeg.GetActualUpdateStatus <> bvxThreadUpdatableObject.bvxUpdatableObjectStateList.Empty Then
    '        Dim Controller As bvxArbitrageController = New bvxArbitrageController()
    '        Dim oConfigOportunities = ConfigOportunitiesSingleton.Instance

    '        Dim iUpdatingLegs As Integer = 0
    '        Dim iTotalLegs As Integer = 0
    '        Dim iTotalExchanges As Integer = oConfigOportunities.Intra.Count
    '        Dim iTotalEmptyLegs As Integer = 0
    '        Dim iUptodateLegs As Integer = 0

    '        For Each oIntra In oConfigOportunities.Intra
    '            iTotalLegs = iTotalLegs + oIntra.MarketLegMaster.Count
    '            iTotalEmptyLegs = iTotalEmptyLegs +
    '            oIntra.MarketLegMaster.Where(
    '            Function(x) x.GetActualUpdateStatus = bvxThreadUpdatableObject.bvxUpdatableObjectStateList.Empty).Count

    '            iUpdatingLegs = iUpdatingLegs +
    '            oIntra.MarketLegMaster.Where(
    '            Function(x) x.GetActualUpdateStatus = bvxThreadUpdatableObject.bvxUpdatableObjectStateList.WaitingAPIResponse).Count

    '            iUptodateLegs = iUptodateLegs +
    '            oIntra.MarketLegMaster.Where(
    '            Function(x) x.GetActualUpdateStatus = bvxThreadUpdatableObject.bvxUpdatableObjectStateList.UpToDate).Count
    '        Next

    '        Dim Message As String = String.Format("Exchanges: {0} || Total Legs: {1} || Legs Waiting: {2} || Legs Empty: {3} || Legs Uptodate: {4}||",
    '                                          iTotalExchanges.ToString, iTotalLegs.ToString, iUpdatingLegs.ToString,
    '                                          iTotalEmptyLegs.ToString, iUptodateLegs.ToString)
    '        If iUpdatingLegs > 0 Then
    '            Message = Message + "Waiting API response..."
    '            RaiseMessage(Message, 3)
    '            'testes, descomentar
    '            'LogService.Write("***ProcessManager.LegStateHandler thread finished for: " +
    '            '                 objLeg.Parent.Parent.Exchange + " - " + objLeg.MarketName, LogService.Type.INFO)
    '            'LogService.Write("***ProcessManager.LegStateHandler: " + Message, LogService.Type.INFO)
    '        ElseIf iTotalEmptyLegs = 0 Then
    '            Message = Message + " Calculating oportunities..."
    '            RaiseMessage(Message, 3)
    '            'desliga a assinatura dos eventos de state changed das legs
    '            SetMasterLegsEvents(ConfigOportunitiesSingleton.Instance, True)
    '            LogService.Write("***Update Refresh Legs finished. Starting search for oportunities...", LogService.Type.INFO)
    '            Controller.RefreshIntraOportunities_Fase2(oConfigOportunities)
    '            LogService.Write("***Refresh finished...", LogService.Type.INFO)
    '            RaiseMessage("Finished", 3)
    '            RaiseRefreshFinished(oConfigOportunities)
    '        End If

    '    End If

    'End Sub

#End Region

End Class
