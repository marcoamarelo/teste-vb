﻿Public Class bvxRoundLegCreation

    Public Property Round As String
    Public Property Leg1A As String
    Public Property Leg1B As String
    Public Property Leg1C As String
    Public Property Leg2A As String
    Public Property Leg2B As String
    Public Property Leg2C As String



    Public Sub New(pRoundValue As String)

        Dim Coins() As String = pRoundValue.Split(",")
        Round = pRoundValue
        Leg1A = String.Format("{0}_{1}", Coins(0), Coins(1))
        Leg1B = String.Format("{0}_{1}", Coins(1), Coins(2))
        Leg1C = String.Format("{0}_{1}", Coins(2), Coins(0))
        Leg2A = String.Format("{0}_{1}", Coins(1), Coins(0))
        Leg2B = String.Format("{0}_{1}", Coins(2), Coins(1))
        Leg2C = String.Format("{0}_{1}", Coins(0), Coins(2))


    End Sub


End Class
