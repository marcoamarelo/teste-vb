﻿Public Class bvxThreadUpdatableObject


#Region "Enum"

    'estado dos objetos que podem ser atualizados por chamadas nas apis das exchanges
    Public Enum bvxUpdatableObjectStateList
        UpToDate = 0
        WaitingAPIResponse = 1
        Calculating = 2
        ErrorResponse = 3
        Empty = 4
    End Enum

    'acompanha os parametros do evento de threads
    Public Enum bvxThreadNotificationList
        ProcessStarting = 0
        ProcessRunning = 1
        ProcessFinished = 2
        ProcessError = 3
    End Enum

#End Region

#Region "Events"

    'utilizado quando um evento é disparado de uma trhead diferente da principal
    'e é necessário atualizar algo em tela
    Delegate Sub ThreadProgressEventDelegateCallBack(ByVal pSender As Object,
                                                     ByVal pState As bvxThreadUpdatableObject.bvxThreadNotificationList)

    Public Delegate Sub StateChanged(ByVal Sender As Object)

    Public Event OnStateChanged As StateChanged

    Private Sub RaiseStateChanged()
        RaiseEvent OnStateChanged(Me)
    End Sub

#End Region


#Region "Properties"

    Protected Property LastUpdate As DateTime
    Protected Property ActualUpdateStatus As bvxUpdatableObjectStateList

#End Region


#Region "Methods"

    Public Function GetActualUpdateStatus() As bvxUpdatableObjectStateList
        Return ActualUpdateStatus
    End Function

    Public Function GetLastUpdate() As DateTime
        Return LastUpdate
    End Function

    Public Sub SetUpdateState(ByVal pNewState As bvxUpdatableObjectStateList)
        ActualUpdateStatus = pNewState
        LastUpdate = Now
        RaiseStateChanged()
    End Sub

#End Region






End Class

