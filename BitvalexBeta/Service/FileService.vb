﻿Imports System.IO

Public Class FileService


    Public Shared Function ListToTextFile(pList As List(Of String), pFileName As String) As String

        Dim writer As StreamWriter = Nothing
        Try
            writer = New StreamWriter(pFileName)
            For Each s In pList
                writer.WriteLine(s)
            Next
            writer.Close()
            Return ""
        Catch ex As Exception
            LogService.Write("***Error writing file " + pFileName + " - " + ex.ToString(), LogService.Type.FATAL)
            Return ex.ToString()
        End Try

    End Function


    Public Shared Function TextFileToList(pFileName As String) As List(Of String)


        Dim list As List(Of String)
        Try
            list = System.IO.File.ReadLines(pFileName).ToList
            Return list
        Catch ex As Exception
            LogService.Write("***Error writing file " + pFileName + " - " + ex.ToString(), LogService.Type.FATAL)
            Throw
        End Try


    End Function


End Class
