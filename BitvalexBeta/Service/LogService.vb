﻿Imports log4net

''' <summary>
''' Classe que gerencia os Logs da aplicação
''' </summary>
''' <remarks></remarks>
Public Class LogService

    Private Shared isLog4netConfigured As Boolean = False

    ''' Instacia de usada para gerenciar o arquivo de Log '''
    Public Shared ReadOnly Property logger
        Get
            ConfigureLogger()
            Return LogManager.GetLogger("General")
        End Get
    End Property

    Public Shared ReadOnly Property secLogger
        Get
            ConfigureLogger()
            Return LogManager.GetLogger("AccessLog")
        End Get
    End Property

    ''' Enum de niveis de criticidade de Log '''
    Public Enum Type
        INFO
        DEBUG
        FATAL
        FAILURE
        ACCESS_CONTROL
    End Enum

    ''' Grava mensagens no arquivo Log.log de acordo com os parametros passados '''
    Public Shared Sub Write(message As String, type As Type)

        Select Case type
            Case LogService.Type.INFO
                logger.Info(message)
            Case LogService.Type.DEBUG
                logger.Debug(message)
            Case LogService.Type.FATAL
                logger.Fatal(message)
            Case LogService.Type.FAILURE
                logger.Error(message)
            Case LogService.Type.ACCESS_CONTROL
                secLogger.Info(message)
            Case Else
                Throw New Exception("Tipo inválido de Log.")
        End Select

    End Sub

    Public Shared Function GetCurrentPath() As String
        Dim path As String
        path = AppDomain.CurrentDomain.BaseDirectory

        If Not path.EndsWith("\") Then
            path += "\"
        End If

        Return path
    End Function

    Public Shared Sub ConfigureLogger()

        If Not isLog4netConfigured Then
            log4net.Config.XmlConfigurator.Configure(System.IO.File.OpenRead(GetCurrentPath() & _
                                                                             "Configuration\Log4net.xml"))
            isLog4netConfigured = True
        End If

    End Sub

End Class


