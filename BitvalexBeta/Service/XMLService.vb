﻿Imports System.Xml
Imports System.Reflection
Imports System.Collections.Generic
Imports System.IO


Public Class XMLService

    Public Const xmlPathSeparator As String = "/"

    Property SQL_FILE As String = "Configuration\Queries.xml"


    Public Sub New()

    End Sub


    Public Function ReadXML(pFileName As String) As XmlDocument

        Dim document As New XmlDocument
        'alterado para atender o DRG sendo usado como DLL do Site
        'os caminhos foram fixados no ini de ambos, site e drg
        'document.Load(AppDomain.CurrentDomain.BaseDirectory & pFileName)
        document.Load(pFileName)

        If document.HasChildNodes Then
            Return document
        Else
            Throw New Exception("Não foi possível ler o arquivo: " & pFileName)
        End If

    End Function

    Public Function ReadXML(pFileName As String, pStream As Stream) As XmlDocument
        Try
            If pStream Is Nothing Then
                Return Nothing
            End If
            Dim document As New XmlDocument
            document.Load(pStream)

            If document.HasChildNodes Then
                Return document
            Else
                Throw New Exception("Não foi possível ler o arquivo: " & pFileName)
            End If
        Catch ex As Exception
            Throw New Exception("Error on XMLService.ReadXML. (Error: " + ex.Message + ") (Filename: " + pFileName + ")")
        End Try
        

    End Function


    ''' Carrega a query sql presente no arquivo SQL.xml de acordo com o alias passado com parametro '''
    Protected Overridable Function GetSqlQueryFromResource(queryAlias As String, pType As Type, pFileName As String) As String

        Dim assembly As [Assembly] = pType.Assembly

        Dim sqlQuery As String = ""

        Dim document As New XmlDocument

        If Not File.Exists(pFileName) Then
            Throw New Exception(pFileName + " doesn`t exists.")
        End If
        document = ReadXML(pFileName, assembly.GetManifestResourceStream(pFileName))
        If document Is Nothing Then
            Return ""
        End If
        Dim root As XmlNode = document.ChildNodes(0)

        Dim querytNode As XmlNode = root.SelectSingleNode(xmlPathSeparator & root.Name & xmlPathSeparator & queryAlias)

        If Not querytNode Is Nothing Then
            sqlQuery = querytNode.InnerText
        End If

        Return sqlQuery

    End Function



    Public Overridable Function GetSqlQuery(queryAlias As String) As String

        Dim sqlQuery As String = ""
        Dim document As New XmlDocument
        document = ReadXML(SQL_FILE)
        Dim root As XmlNode = document.ChildNodes(0)
        Dim querytNode As XmlNode = root.SelectSingleNode _
                (xmlPathSeparator & root.Name & xmlPathSeparator & queryAlias)
        If Not querytNode Is Nothing Then
            sqlQuery = querytNode.InnerText
        End If

        Return sqlQuery

    End Function






End Class
