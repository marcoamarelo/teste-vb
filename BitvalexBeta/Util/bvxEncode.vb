﻿Imports System.ComponentModel
Imports System.IO
Imports System.Reflection
Imports System.Text

Friend NotInheritable Class bvxEncode

    Public Shared Function HTTPEncode(ByVal pCommand As Dictionary(Of String, String)) As String
        Dim s As String = ""
        For i As Integer = 0 To pCommand.Count - 1
            If i > 0 Then
                s = s + "&"
            End If
            s = s + pCommand.Keys(i) + "=" + pCommand.Values(i)
        Next
        Return s
    End Function


    Public Shared Function HashStringHMAC_SHA512(ByVal StringToHash As String, ByVal HashKey As String) As String
        Dim myEncoder As New System.Text.UTF8Encoding
        Dim Key() As Byte = myEncoder.GetBytes(HashKey)
        Dim Text() As Byte = myEncoder.GetBytes(StringToHash)
        Dim myHMACSHA1 As New System.Security.Cryptography.HMACSHA512(Key)
        Dim HashCode As Byte() = myHMACSHA1.ComputeHash(Text)
        Dim hash As String = Replace(BitConverter.ToString(HashCode), "-", "")
        Return hash.ToLower
    End Function


    Public Shared Function ListToDataTable(Of T)(pList As BindingList(Of T)) As DataTable
        Dim x = From T1 In pList.AsEnumerable
                Select T1
        Dim oDt As DataTable = LINQToDataTable(x, "List")
        Return oDt
    End Function

    Public Shared Function LINQToDataTable(Of T)(pVarList As IEnumerable(Of T), pTableName As String) As DataTable
        Dim oDtReturn As DataTable = New DataTable(pTableName)
        Dim oProps() As PropertyInfo = Nothing
        If pVarList Is Nothing Then
            Return oDtReturn
        End If
        For Each rec As T In pVarList
            'cria as colunas na primeira passagem
            If oProps Is Nothing Then
                oProps = rec.GetType().GetProperties()
                For Each pi As PropertyInfo In oProps
                    Dim ColType As Type = pi.PropertyType
                    If (ColType.IsGenericType) AndAlso IsNullableType(ColType) Then
                        ColType = ColType.GetGenericArguments(0)
                        oDtReturn.Columns.Add(New DataColumn(pi.Name, ColType))
                    Else
                        oDtReturn.Columns.Add(New DataColumn(pi.Name))
                    End If
                Next
            End If
            Dim oDr As DataRow = oDtReturn.NewRow()
            For Each pi As PropertyInfo In oProps
                oDr(pi.Name) = If(pi.GetValue(rec, Nothing) Is Nothing, DBNull.Value, pi.GetValue(rec, Nothing))
            Next
            oDtReturn.Rows.Add(oDr)
        Next
        Return oDtReturn
    End Function


    Private Shared Function IsNullableType(ByVal myType As Type) As Boolean
        Return (myType.IsGenericType) AndAlso (myType.GetGenericTypeDefinition() Is GetType(Nullable(Of )))
    End Function


    Public Shared Function GetNonce() As String
        Return System.DateTime.Now.Ticks.ToString()
    End Function




    Public Shared Function DataTableToString(ByVal pDataTable As DataTable) As String
        '--------Columns Name--------------------------------------------------------------------------- 
        Dim sb As StringBuilder = New StringBuilder()
        Dim intClmn As Integer = pDataTable.Columns.Count
        Dim i As Integer = 0
        For i = 0 To intClmn - 1 Step i + 1
            sb.Append("""" + pDataTable.Columns(i).ColumnName.ToString() + """")
            If i = intClmn - 1 Then
                sb.Append(Microsoft.VisualBasic.vbNewLine)
            Else
                sb.Append(Microsoft.VisualBasic.vbTab)
            End If
        Next

        '--------Data By  Columns--------------------------------------------------------------------------- 
        Dim row As DataRow
        For Each row In pDataTable.Rows
            Dim ir As Integer = 0
            For ir = 0 To intClmn - 1 Step ir + 1
                sb.Append("""" + row(ir).ToString().Replace("""", """""") + """")
                If ir = intClmn - 1 Then
                    sb.Append(Microsoft.VisualBasic.vbNewLine)
                Else
                    sb.Append(Microsoft.VisualBasic.vbTab)
                End If
            Next

        Next
        Dim Result As String = sb.ToString
        Return Result
    End Function


    Public Shared Function DatatableToCSV(ByVal pDataTable As DataTable, ByVal pFileName As String)

        Dim s As String = DataTableToString(pDataTable)
        Dim b As Byte() = System.Text.Encoding.UTF8.GetBytes(s)
        File.WriteAllBytes(pFileName, b)

    End Function




End Class
