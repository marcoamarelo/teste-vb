﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmBotTest
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.tabOportunities = New System.Windows.Forms.TabControl()
        Me.tabOport = New System.Windows.Forms.TabPage()
        Me.Button30 = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tabRounds = New System.Windows.Forms.TabPage()
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer()
        Me.Button38 = New System.Windows.Forms.Button()
        Me.grdRounds = New System.Windows.Forms.DataGridView()
        Me.radIntraOnlyOportunities = New System.Windows.Forms.RadioButton()
        Me.radIntraAllRounds = New System.Windows.Forms.RadioButton()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Button37 = New System.Windows.Forms.Button()
        Me.grdLegs = New System.Windows.Forms.DataGridView()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.tabBetweenRounds = New System.Windows.Forms.TabPage()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.radBetweenOnlyOportunities = New System.Windows.Forms.RadioButton()
        Me.radBetweenAllRounds = New System.Windows.Forms.RadioButton()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.grdRoundsBetween = New System.Windows.Forms.DataGridView()
        Me.radBetweenLegsAllLegs = New System.Windows.Forms.RadioButton()
        Me.radBetweenLegsOnlyOportunities = New System.Windows.Forms.RadioButton()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.grdLegsBetween = New System.Windows.Forms.DataGridView()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.chkTimer = New System.Windows.Forms.CheckBox()
        Me.cboRefreshTime = New System.Windows.Forms.DomainUpDown()
        Me.btnOportunities = New System.Windows.Forms.Button()
        Me.tabConfig = New System.Windows.Forms.TabPage()
        Me.chkUseMySql = New System.Windows.Forms.CheckBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.edConfigFilesOutput = New System.Windows.Forms.TextBox()
        Me.tabBleutrade = New System.Windows.Forms.TabPage()
        Me.Button35 = New System.Windows.Forms.Button()
        Me.Button36 = New System.Windows.Forms.Button()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.edCommentBleutrade = New System.Windows.Forms.TextBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.edQuantityBleutrade = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.edRateBleutrade = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.edMarketBleutrade = New System.Windows.Forms.TextBox()
        Me.Button34 = New System.Windows.Forms.Button()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.edOrderId = New System.Windows.Forms.TextBox()
        Me.Button33 = New System.Windows.Forms.Button()
        Me.Button32 = New System.Windows.Forms.Button()
        Me.Button31 = New System.Windows.Forms.Button()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.edCoins = New System.Windows.Forms.TextBox()
        Me.Button18 = New System.Windows.Forms.Button()
        Me.Button28 = New System.Windows.Forms.Button()
        Me.Button29 = New System.Windows.Forms.Button()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.edMarketHitbtc = New System.Windows.Forms.TextBox()
        Me.Button24 = New System.Windows.Forms.Button()
        Me.Button25 = New System.Windows.Forms.Button()
        Me.Button26 = New System.Windows.Forms.Button()
        Me.Button27 = New System.Windows.Forms.Button()
        Me.edBinancePair = New System.Windows.Forms.TextBox()
        Me.Button22 = New System.Windows.Forms.Button()
        Me.Button23 = New System.Windows.Forms.Button()
        Me.Button16 = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Button15 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.edYobitPair = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.edLastHours = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cboPeriod = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Button14 = New System.Windows.Forms.Button()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.cboMarketType = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.edDepth = New System.Windows.Forms.TextBox()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.edMarket = New System.Windows.Forms.TextBox()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.PropertyGrid1 = New System.Windows.Forms.PropertyGrid()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lstResults = New System.Windows.Forms.ListBox()
        Me.tabBrasiliex = New System.Windows.Forms.TabPage()
        Me.btnGo = New System.Windows.Forms.Button()
        Me.edUrl = New System.Windows.Forms.TextBox()
        Me.edResultBrasiliex = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.tabDetails = New System.Windows.Forms.TabPage()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.lstBetweenJson = New System.Windows.Forms.ListBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.lstBetweenExchanges = New System.Windows.Forms.ListBox()
        Me.prpExchanges = New System.Windows.Forms.PropertyGrid()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.lstIntraExchanges = New System.Windows.Forms.ListBox()
        Me.grdRoundsDetails = New System.Windows.Forms.DataGridView()
        Me.grdLegsDetails = New System.Windows.Forms.DataGridView()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.tabGenerateRounds = New System.Windows.Forms.TabPage()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.edListToFile = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.edPreferentialCoins = New System.Windows.Forms.TextBox()
        Me.Button21 = New System.Windows.Forms.Button()
        Me.btnExportTXT = New System.Windows.Forms.Button()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.edFileoutput = New System.Windows.Forms.TextBox()
        Me.lstMarkets = New System.Windows.Forms.ListBox()
        Me.cboExchangeName = New System.Windows.Forms.ComboBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Button19 = New System.Windows.Forms.Button()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.lblStatus1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblStatus2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblStatus3 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblStatusTime = New System.Windows.Forms.ToolStripStatusLabel()
        Me.timerRefresh = New System.Windows.Forms.Timer(Me.components)
        Me.timerWatch = New System.Windows.Forms.Timer(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.edIntraQtdRound1 = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.edIntraRateRound1 = New System.Windows.Forms.TextBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.edIntraMarketRound1 = New System.Windows.Forms.TextBox()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.edIntraQtdRound2 = New System.Windows.Forms.TextBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.edIntraRateRound2 = New System.Windows.Forms.TextBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.edIntraMarketRound2 = New System.Windows.Forms.TextBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.edIntraQtdRound3 = New System.Windows.Forms.TextBox()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.edIntraRateRound3 = New System.Windows.Forms.TextBox()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.edIntraMarketRound3 = New System.Windows.Forms.TextBox()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.btnIntraOrderRound1 = New System.Windows.Forms.Button()
        Me.btnIntraOrderRound2 = New System.Windows.Forms.Button()
        Me.btnIntraOrderRound3 = New System.Windows.Forms.Button()
        Me.tabOportunities.SuspendLayout()
        Me.tabOport.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.tabRounds.SuspendLayout()
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.Panel2.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        CType(Me.grdRounds, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdLegs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabBetweenRounds.SuspendLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.grdRoundsBetween, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdLegsBetween, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabConfig.SuspendLayout()
        Me.tabBleutrade.SuspendLayout()
        Me.tabBrasiliex.SuspendLayout()
        Me.tabDetails.SuspendLayout()
        CType(Me.grdRoundsDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdLegsDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabGenerateRounds.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'tabOportunities
        '
        Me.tabOportunities.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tabOportunities.Controls.Add(Me.tabOport)
        Me.tabOportunities.Controls.Add(Me.tabConfig)
        Me.tabOportunities.Controls.Add(Me.tabBleutrade)
        Me.tabOportunities.Controls.Add(Me.tabBrasiliex)
        Me.tabOportunities.Controls.Add(Me.tabDetails)
        Me.tabOportunities.Controls.Add(Me.tabGenerateRounds)
        Me.tabOportunities.Location = New System.Drawing.Point(24, 12)
        Me.tabOportunities.Name = "tabOportunities"
        Me.tabOportunities.SelectedIndex = 0
        Me.tabOportunities.Size = New System.Drawing.Size(955, 684)
        Me.tabOportunities.TabIndex = 2
        '
        'tabOport
        '
        Me.tabOport.Controls.Add(Me.Panel1)
        Me.tabOport.Controls.Add(Me.Button30)
        Me.tabOport.Controls.Add(Me.TabControl1)
        Me.tabOport.Controls.Add(Me.Label16)
        Me.tabOport.Controls.Add(Me.chkTimer)
        Me.tabOport.Controls.Add(Me.cboRefreshTime)
        Me.tabOport.Controls.Add(Me.btnOportunities)
        Me.tabOport.Location = New System.Drawing.Point(4, 22)
        Me.tabOport.Name = "tabOport"
        Me.tabOport.Padding = New System.Windows.Forms.Padding(3)
        Me.tabOport.Size = New System.Drawing.Size(947, 658)
        Me.tabOport.TabIndex = 3
        Me.tabOport.Text = "Main"
        Me.tabOport.UseVisualStyleBackColor = True
        '
        'Button30
        '
        Me.Button30.Location = New System.Drawing.Point(2, 51)
        Me.Button30.Name = "Button30"
        Me.Button30.Size = New System.Drawing.Size(123, 23)
        Me.Button30.TabIndex = 35
        Me.Button30.Text = "Refresh IntraBetween"
        Me.Button30.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.tabRounds)
        Me.TabControl1.Controls.Add(Me.tabBetweenRounds)
        Me.TabControl1.Location = New System.Drawing.Point(140, 6)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(811, 652)
        Me.TabControl1.TabIndex = 33
        '
        'tabRounds
        '
        Me.tabRounds.Controls.Add(Me.SplitContainer2)
        Me.tabRounds.Location = New System.Drawing.Point(4, 22)
        Me.tabRounds.Name = "tabRounds"
        Me.tabRounds.Padding = New System.Windows.Forms.Padding(3)
        Me.tabRounds.Size = New System.Drawing.Size(803, 626)
        Me.tabRounds.TabIndex = 0
        Me.tabRounds.Text = "Intra Rounds"
        Me.tabRounds.UseVisualStyleBackColor = True
        '
        'SplitContainer2
        '
        Me.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer2.Location = New System.Drawing.Point(3, 3)
        Me.SplitContainer2.Name = "SplitContainer2"
        Me.SplitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.Controls.Add(Me.Button38)
        Me.SplitContainer2.Panel1.Controls.Add(Me.grdRounds)
        Me.SplitContainer2.Panel1.Controls.Add(Me.radIntraOnlyOportunities)
        Me.SplitContainer2.Panel1.Controls.Add(Me.radIntraAllRounds)
        Me.SplitContainer2.Panel1.Controls.Add(Me.Label13)
        '
        'SplitContainer2.Panel2
        '
        Me.SplitContainer2.Panel2.Controls.Add(Me.Button37)
        Me.SplitContainer2.Panel2.Controls.Add(Me.grdLegs)
        Me.SplitContainer2.Panel2.Controls.Add(Me.Label15)
        Me.SplitContainer2.Size = New System.Drawing.Size(797, 620)
        Me.SplitContainer2.SplitterDistance = 333
        Me.SplitContainer2.TabIndex = 42
        '
        'Button38
        '
        Me.Button38.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button38.Location = New System.Drawing.Point(642, 307)
        Me.Button38.Name = "Button38"
        Me.Button38.Size = New System.Drawing.Size(152, 23)
        Me.Button38.TabIndex = 46
        Me.Button38.Text = "Grid To Clipboard"
        Me.Button38.UseVisualStyleBackColor = True
        '
        'grdRounds
        '
        Me.grdRounds.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdRounds.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdRounds.Location = New System.Drawing.Point(3, 20)
        Me.grdRounds.Name = "grdRounds"
        Me.grdRounds.Size = New System.Drawing.Size(791, 287)
        Me.grdRounds.TabIndex = 45
        '
        'radIntraOnlyOportunities
        '
        Me.radIntraOnlyOportunities.AutoSize = True
        Me.radIntraOnlyOportunities.Location = New System.Drawing.Point(98, 1)
        Me.radIntraOnlyOportunities.Name = "radIntraOnlyOportunities"
        Me.radIntraOnlyOportunities.Size = New System.Drawing.Size(105, 17)
        Me.radIntraOnlyOportunities.TabIndex = 44
        Me.radIntraOnlyOportunities.Text = "Only Oportunities"
        Me.radIntraOnlyOportunities.UseVisualStyleBackColor = True
        '
        'radIntraAllRounds
        '
        Me.radIntraAllRounds.AutoSize = True
        Me.radIntraAllRounds.Checked = True
        Me.radIntraAllRounds.Location = New System.Drawing.Point(241, 0)
        Me.radIntraAllRounds.Name = "radIntraAllRounds"
        Me.radIntraAllRounds.Size = New System.Drawing.Size(76, 17)
        Me.radIntraAllRounds.TabIndex = 43
        Me.radIntraAllRounds.TabStop = True
        Me.radIntraAllRounds.Text = "All Rounds"
        Me.radIntraAllRounds.UseVisualStyleBackColor = True
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(3, 4)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(50, 13)
        Me.Label13.TabIndex = 42
        Me.Label13.Text = "Rounds"
        '
        'Button37
        '
        Me.Button37.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button37.Location = New System.Drawing.Point(642, 257)
        Me.Button37.Name = "Button37"
        Me.Button37.Size = New System.Drawing.Size(152, 23)
        Me.Button37.TabIndex = 35
        Me.Button37.Text = "Grid To Clipboard"
        Me.Button37.UseVisualStyleBackColor = True
        '
        'grdLegs
        '
        Me.grdLegs.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdLegs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdLegs.Location = New System.Drawing.Point(6, 16)
        Me.grdLegs.Name = "grdLegs"
        Me.grdLegs.Size = New System.Drawing.Size(788, 241)
        Me.grdLegs.TabIndex = 34
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(0, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(34, 13)
        Me.Label15.TabIndex = 33
        Me.Label15.Text = "Legs"
        '
        'tabBetweenRounds
        '
        Me.tabBetweenRounds.Controls.Add(Me.SplitContainer1)
        Me.tabBetweenRounds.Location = New System.Drawing.Point(4, 22)
        Me.tabBetweenRounds.Name = "tabBetweenRounds"
        Me.tabBetweenRounds.Padding = New System.Windows.Forms.Padding(3)
        Me.tabBetweenRounds.Size = New System.Drawing.Size(803, 584)
        Me.tabBetweenRounds.TabIndex = 2
        Me.tabBetweenRounds.Text = "All Between Rounds"
        Me.tabBetweenRounds.UseVisualStyleBackColor = True
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(3, 3)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.radBetweenOnlyOportunities)
        Me.SplitContainer1.Panel1.Controls.Add(Me.radBetweenAllRounds)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label29)
        Me.SplitContainer1.Panel1.Controls.Add(Me.grdRoundsBetween)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.radBetweenLegsAllLegs)
        Me.SplitContainer1.Panel2.Controls.Add(Me.radBetweenLegsOnlyOportunities)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label30)
        Me.SplitContainer1.Panel2.Controls.Add(Me.grdLegsBetween)
        Me.SplitContainer1.Size = New System.Drawing.Size(797, 578)
        Me.SplitContainer1.SplitterDistance = 288
        Me.SplitContainer1.TabIndex = 36
        '
        'radBetweenOnlyOportunities
        '
        Me.radBetweenOnlyOportunities.AutoSize = True
        Me.radBetweenOnlyOportunities.Location = New System.Drawing.Point(113, 1)
        Me.radBetweenOnlyOportunities.Name = "radBetweenOnlyOportunities"
        Me.radBetweenOnlyOportunities.Size = New System.Drawing.Size(105, 17)
        Me.radBetweenOnlyOportunities.TabIndex = 39
        Me.radBetweenOnlyOportunities.Text = "Only Oportunities"
        Me.radBetweenOnlyOportunities.UseVisualStyleBackColor = True
        '
        'radBetweenAllRounds
        '
        Me.radBetweenAllRounds.AutoSize = True
        Me.radBetweenAllRounds.Checked = True
        Me.radBetweenAllRounds.Location = New System.Drawing.Point(256, 0)
        Me.radBetweenAllRounds.Name = "radBetweenAllRounds"
        Me.radBetweenAllRounds.Size = New System.Drawing.Size(76, 17)
        Me.radBetweenAllRounds.TabIndex = 38
        Me.radBetweenAllRounds.TabStop = True
        Me.radBetweenAllRounds.Text = "All Rounds"
        Me.radBetweenAllRounds.UseVisualStyleBackColor = True
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(4, 0)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(50, 13)
        Me.Label29.TabIndex = 37
        Me.Label29.Text = "Rounds"
        '
        'grdRoundsBetween
        '
        Me.grdRoundsBetween.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdRoundsBetween.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdRoundsBetween.Location = New System.Drawing.Point(6, 20)
        Me.grdRoundsBetween.Name = "grdRoundsBetween"
        Me.grdRoundsBetween.Size = New System.Drawing.Size(788, 268)
        Me.grdRoundsBetween.TabIndex = 36
        '
        'radBetweenLegsAllLegs
        '
        Me.radBetweenLegsAllLegs.AutoSize = True
        Me.radBetweenLegsAllLegs.Checked = True
        Me.radBetweenLegsAllLegs.Location = New System.Drawing.Point(256, 2)
        Me.radBetweenLegsAllLegs.Name = "radBetweenLegsAllLegs"
        Me.radBetweenLegsAllLegs.Size = New System.Drawing.Size(62, 17)
        Me.radBetweenLegsAllLegs.TabIndex = 41
        Me.radBetweenLegsAllLegs.TabStop = True
        Me.radBetweenLegsAllLegs.Text = "All Legs"
        Me.radBetweenLegsAllLegs.UseVisualStyleBackColor = True
        '
        'radBetweenLegsOnlyOportunities
        '
        Me.radBetweenLegsOnlyOportunities.AutoSize = True
        Me.radBetweenLegsOnlyOportunities.Location = New System.Drawing.Point(105, 2)
        Me.radBetweenLegsOnlyOportunities.Name = "radBetweenLegsOnlyOportunities"
        Me.radBetweenLegsOnlyOportunities.Size = New System.Drawing.Size(105, 17)
        Me.radBetweenLegsOnlyOportunities.TabIndex = 40
        Me.radBetweenLegsOnlyOportunities.Text = "Only Oportunities"
        Me.radBetweenLegsOnlyOportunities.UseVisualStyleBackColor = True
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(3, 0)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(34, 13)
        Me.Label30.TabIndex = 38
        Me.Label30.Text = "Legs"
        '
        'grdLegsBetween
        '
        Me.grdLegsBetween.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdLegsBetween.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdLegsBetween.Location = New System.Drawing.Point(4, 20)
        Me.grdLegsBetween.Name = "grdLegsBetween"
        Me.grdLegsBetween.Size = New System.Drawing.Size(790, 263)
        Me.grdLegsBetween.TabIndex = 36
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(75, 109)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(47, 13)
        Me.Label16.TabIndex = 32
        Me.Label16.Text = "seconds"
        '
        'chkTimer
        '
        Me.chkTimer.AutoSize = True
        Me.chkTimer.Location = New System.Drawing.Point(19, 84)
        Me.chkTimer.Name = "chkTimer"
        Me.chkTimer.Size = New System.Drawing.Size(83, 17)
        Me.chkTimer.TabIndex = 31
        Me.chkTimer.Text = "Auto refresh"
        Me.chkTimer.UseVisualStyleBackColor = True
        '
        'cboRefreshTime
        '
        Me.cboRefreshTime.Items.Add("2")
        Me.cboRefreshTime.Items.Add("4")
        Me.cboRefreshTime.Items.Add("6")
        Me.cboRefreshTime.Items.Add("8")
        Me.cboRefreshTime.Items.Add("10")
        Me.cboRefreshTime.Items.Add("15")
        Me.cboRefreshTime.Items.Add("20")
        Me.cboRefreshTime.Items.Add("30")
        Me.cboRefreshTime.Items.Add("60")
        Me.cboRefreshTime.Items.Add("120")
        Me.cboRefreshTime.Items.Add("240")
        Me.cboRefreshTime.Items.Add("600")
        Me.cboRefreshTime.Location = New System.Drawing.Point(19, 107)
        Me.cboRefreshTime.Name = "cboRefreshTime"
        Me.cboRefreshTime.Size = New System.Drawing.Size(50, 20)
        Me.cboRefreshTime.TabIndex = 30
        Me.cboRefreshTime.Text = "10"
        '
        'btnOportunities
        '
        Me.btnOportunities.Location = New System.Drawing.Point(3, 22)
        Me.btnOportunities.Name = "btnOportunities"
        Me.btnOportunities.Size = New System.Drawing.Size(75, 23)
        Me.btnOportunities.TabIndex = 1
        Me.btnOportunities.Text = "Start"
        Me.btnOportunities.UseVisualStyleBackColor = True
        '
        'tabConfig
        '
        Me.tabConfig.Controls.Add(Me.chkUseMySql)
        Me.tabConfig.Controls.Add(Me.Label11)
        Me.tabConfig.Controls.Add(Me.edConfigFilesOutput)
        Me.tabConfig.Location = New System.Drawing.Point(4, 22)
        Me.tabConfig.Name = "tabConfig"
        Me.tabConfig.Padding = New System.Windows.Forms.Padding(3)
        Me.tabConfig.Size = New System.Drawing.Size(947, 658)
        Me.tabConfig.TabIndex = 2
        Me.tabConfig.Text = "Config"
        Me.tabConfig.UseVisualStyleBackColor = True
        '
        'chkUseMySql
        '
        Me.chkUseMySql.AutoSize = True
        Me.chkUseMySql.Location = New System.Drawing.Point(192, 97)
        Me.chkUseMySql.Name = "chkUseMySql"
        Me.chkUseMySql.Size = New System.Drawing.Size(126, 17)
        Me.chkUseMySql.TabIndex = 26
        Me.chkUseMySql.Text = "Use MySql Database"
        Me.chkUseMySql.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(80, 33)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(88, 13)
        Me.Label11.TabIndex = 23
        Me.Label11.Text = "Files output path:"
        '
        'edConfigFilesOutput
        '
        Me.edConfigFilesOutput.Location = New System.Drawing.Point(192, 30)
        Me.edConfigFilesOutput.Name = "edConfigFilesOutput"
        Me.edConfigFilesOutput.Size = New System.Drawing.Size(351, 20)
        Me.edConfigFilesOutput.TabIndex = 22
        Me.edConfigFilesOutput.Text = "c:\Teste\"
        '
        'tabBleutrade
        '
        Me.tabBleutrade.Controls.Add(Me.Button35)
        Me.tabBleutrade.Controls.Add(Me.Button36)
        Me.tabBleutrade.Controls.Add(Me.Label33)
        Me.tabBleutrade.Controls.Add(Me.edCommentBleutrade)
        Me.tabBleutrade.Controls.Add(Me.Label32)
        Me.tabBleutrade.Controls.Add(Me.edQuantityBleutrade)
        Me.tabBleutrade.Controls.Add(Me.Label31)
        Me.tabBleutrade.Controls.Add(Me.edRateBleutrade)
        Me.tabBleutrade.Controls.Add(Me.Label20)
        Me.tabBleutrade.Controls.Add(Me.edMarketBleutrade)
        Me.tabBleutrade.Controls.Add(Me.Button34)
        Me.tabBleutrade.Controls.Add(Me.Label19)
        Me.tabBleutrade.Controls.Add(Me.edOrderId)
        Me.tabBleutrade.Controls.Add(Me.Button33)
        Me.tabBleutrade.Controls.Add(Me.Button32)
        Me.tabBleutrade.Controls.Add(Me.Button31)
        Me.tabBleutrade.Controls.Add(Me.Label12)
        Me.tabBleutrade.Controls.Add(Me.edCoins)
        Me.tabBleutrade.Controls.Add(Me.Button18)
        Me.tabBleutrade.Controls.Add(Me.Button28)
        Me.tabBleutrade.Controls.Add(Me.Button29)
        Me.tabBleutrade.Controls.Add(Me.Label26)
        Me.tabBleutrade.Controls.Add(Me.Label25)
        Me.tabBleutrade.Controls.Add(Me.edMarketHitbtc)
        Me.tabBleutrade.Controls.Add(Me.Button24)
        Me.tabBleutrade.Controls.Add(Me.Button25)
        Me.tabBleutrade.Controls.Add(Me.Button26)
        Me.tabBleutrade.Controls.Add(Me.Button27)
        Me.tabBleutrade.Controls.Add(Me.edBinancePair)
        Me.tabBleutrade.Controls.Add(Me.Button22)
        Me.tabBleutrade.Controls.Add(Me.Button23)
        Me.tabBleutrade.Controls.Add(Me.Button16)
        Me.tabBleutrade.Controls.Add(Me.Label8)
        Me.tabBleutrade.Controls.Add(Me.Label9)
        Me.tabBleutrade.Controls.Add(Me.Button15)
        Me.tabBleutrade.Controls.Add(Me.Button4)
        Me.tabBleutrade.Controls.Add(Me.Button6)
        Me.tabBleutrade.Controls.Add(Me.Label10)
        Me.tabBleutrade.Controls.Add(Me.edYobitPair)
        Me.tabBleutrade.Controls.Add(Me.Button5)
        Me.tabBleutrade.Controls.Add(Me.edLastHours)
        Me.tabBleutrade.Controls.Add(Me.Label7)
        Me.tabBleutrade.Controls.Add(Me.cboPeriod)
        Me.tabBleutrade.Controls.Add(Me.Label6)
        Me.tabBleutrade.Controls.Add(Me.Button14)
        Me.tabBleutrade.Controls.Add(Me.Button13)
        Me.tabBleutrade.Controls.Add(Me.cboMarketType)
        Me.tabBleutrade.Controls.Add(Me.Label5)
        Me.tabBleutrade.Controls.Add(Me.Label4)
        Me.tabBleutrade.Controls.Add(Me.edDepth)
        Me.tabBleutrade.Controls.Add(Me.Button12)
        Me.tabBleutrade.Controls.Add(Me.Button11)
        Me.tabBleutrade.Controls.Add(Me.Label3)
        Me.tabBleutrade.Controls.Add(Me.edMarket)
        Me.tabBleutrade.Controls.Add(Me.Button10)
        Me.tabBleutrade.Controls.Add(Me.PropertyGrid1)
        Me.tabBleutrade.Controls.Add(Me.Button9)
        Me.tabBleutrade.Controls.Add(Me.Button8)
        Me.tabBleutrade.Controls.Add(Me.Button7)
        Me.tabBleutrade.Controls.Add(Me.Label2)
        Me.tabBleutrade.Controls.Add(Me.Label1)
        Me.tabBleutrade.Controls.Add(Me.lstResults)
        Me.tabBleutrade.Location = New System.Drawing.Point(4, 22)
        Me.tabBleutrade.Name = "tabBleutrade"
        Me.tabBleutrade.Padding = New System.Windows.Forms.Padding(3)
        Me.tabBleutrade.Size = New System.Drawing.Size(947, 658)
        Me.tabBleutrade.TabIndex = 1
        Me.tabBleutrade.Text = "API Test"
        Me.tabBleutrade.UseVisualStyleBackColor = True
        '
        'Button35
        '
        Me.Button35.Location = New System.Drawing.Point(115, 568)
        Me.Button35.Name = "Button35"
        Me.Button35.Size = New System.Drawing.Size(108, 25)
        Me.Button35.TabIndex = 79
        Me.Button35.Text = "Sell Order"
        Me.Button35.UseVisualStyleBackColor = True
        '
        'Button36
        '
        Me.Button36.Location = New System.Drawing.Point(115, 599)
        Me.Button36.Name = "Button36"
        Me.Button36.Size = New System.Drawing.Size(108, 25)
        Me.Button36.TabIndex = 78
        Me.Button36.Text = "Buy Order"
        Me.Button36.UseVisualStyleBackColor = True
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(56, 545)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(53, 13)
        Me.Label33.TabIndex = 77
        Me.Label33.Text = "comment:"
        '
        'edCommentBleutrade
        '
        Me.edCommentBleutrade.Location = New System.Drawing.Point(115, 542)
        Me.edCommentBleutrade.Name = "edCommentBleutrade"
        Me.edCommentBleutrade.Size = New System.Drawing.Size(112, 20)
        Me.edCommentBleutrade.TabIndex = 76
        Me.edCommentBleutrade.Text = "Ordem Teste"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(60, 520)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(49, 13)
        Me.Label32.TabIndex = 75
        Me.Label32.Text = "Quantity:"
        '
        'edQuantityBleutrade
        '
        Me.edQuantityBleutrade.Location = New System.Drawing.Point(115, 520)
        Me.edQuantityBleutrade.Name = "edQuantityBleutrade"
        Me.edQuantityBleutrade.Size = New System.Drawing.Size(112, 20)
        Me.edQuantityBleutrade.TabIndex = 74
        Me.edQuantityBleutrade.Text = "0,0003623"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(76, 497)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(33, 13)
        Me.Label31.TabIndex = 73
        Me.Label31.Text = "Rate:"
        '
        'edRateBleutrade
        '
        Me.edRateBleutrade.Location = New System.Drawing.Point(115, 497)
        Me.edRateBleutrade.Name = "edRateBleutrade"
        Me.edRateBleutrade.Size = New System.Drawing.Size(112, 20)
        Me.edRateBleutrade.TabIndex = 72
        Me.edRateBleutrade.Text = "0,069"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(66, 475)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(43, 13)
        Me.Label20.TabIndex = 71
        Me.Label20.Text = "Market:"
        '
        'edMarketBleutrade
        '
        Me.edMarketBleutrade.Location = New System.Drawing.Point(115, 475)
        Me.edMarketBleutrade.Name = "edMarketBleutrade"
        Me.edMarketBleutrade.Size = New System.Drawing.Size(112, 20)
        Me.edMarketBleutrade.TabIndex = 70
        Me.edMarketBleutrade.Text = "ETH_BTC"
        '
        'Button34
        '
        Me.Button34.Location = New System.Drawing.Point(115, 435)
        Me.Button34.Name = "Button34"
        Me.Button34.Size = New System.Drawing.Size(108, 25)
        Me.Button34.TabIndex = 69
        Me.Button34.Text = "Cancel Order"
        Me.Button34.UseVisualStyleBackColor = True
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(20, 413)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(50, 13)
        Me.Label19.TabIndex = 68
        Me.Label19.Text = "Order ID:"
        '
        'edOrderId
        '
        Me.edOrderId.Location = New System.Drawing.Point(69, 409)
        Me.edOrderId.Name = "edOrderId"
        Me.edOrderId.Size = New System.Drawing.Size(112, 20)
        Me.edOrderId.TabIndex = 67
        Me.edOrderId.Text = "1452"
        '
        'Button33
        '
        Me.Button33.Location = New System.Drawing.Point(4, 435)
        Me.Button33.Name = "Button33"
        Me.Button33.Size = New System.Drawing.Size(108, 25)
        Me.Button33.TabIndex = 66
        Me.Button33.Text = "Get Order"
        Me.Button33.UseVisualStyleBackColor = True
        '
        'Button32
        '
        Me.Button32.Location = New System.Drawing.Point(8, 347)
        Me.Button32.Name = "Button32"
        Me.Button32.Size = New System.Drawing.Size(108, 25)
        Me.Button32.TabIndex = 65
        Me.Button32.Text = "Balance"
        Me.Button32.UseVisualStyleBackColor = True
        '
        'Button31
        '
        Me.Button31.Location = New System.Drawing.Point(6, 378)
        Me.Button31.Name = "Button31"
        Me.Button31.Size = New System.Drawing.Size(108, 25)
        Me.Button31.TabIndex = 64
        Me.Button31.Text = "Open Orders"
        Me.Button31.UseVisualStyleBackColor = True
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(19, 332)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(36, 13)
        Me.Label12.TabIndex = 63
        Me.Label12.Text = "Coins:"
        '
        'edCoins
        '
        Me.edCoins.Location = New System.Drawing.Point(68, 328)
        Me.edCoins.Name = "edCoins"
        Me.edCoins.Size = New System.Drawing.Size(112, 20)
        Me.edCoins.TabIndex = 62
        Me.edCoins.Text = "BTC"
        '
        'Button18
        '
        Me.Button18.Location = New System.Drawing.Point(119, 347)
        Me.Button18.Name = "Button18"
        Me.Button18.Size = New System.Drawing.Size(108, 25)
        Me.Button18.TabIndex = 61
        Me.Button18.Text = "Balances"
        Me.Button18.UseVisualStyleBackColor = True
        '
        'Button28
        '
        Me.Button28.Location = New System.Drawing.Point(249, 264)
        Me.Button28.Name = "Button28"
        Me.Button28.Size = New System.Drawing.Size(174, 23)
        Me.Button28.TabIndex = 60
        Me.Button28.Text = "Info"
        Me.Button28.UseVisualStyleBackColor = True
        '
        'Button29
        '
        Me.Button29.Location = New System.Drawing.Point(249, 239)
        Me.Button29.Name = "Button29"
        Me.Button29.Size = New System.Drawing.Size(174, 23)
        Me.Button29.TabIndex = 59
        Me.Button29.Text = "Depth"
        Me.Button29.UseVisualStyleBackColor = True
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(246, 175)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(53, 13)
        Me.Label26.TabIndex = 58
        Me.Label26.Text = "Binance"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(246, 321)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(41, 13)
        Me.Label25.TabIndex = 57
        Me.Label25.Text = "Hitbtc"
        '
        'edMarketHitbtc
        '
        Me.edMarketHitbtc.Location = New System.Drawing.Point(293, 441)
        Me.edMarketHitbtc.Name = "edMarketHitbtc"
        Me.edMarketHitbtc.Size = New System.Drawing.Size(120, 20)
        Me.edMarketHitbtc.TabIndex = 56
        Me.edMarketHitbtc.Text = "BCNBTC"
        '
        'Button24
        '
        Me.Button24.Location = New System.Drawing.Point(248, 412)
        Me.Button24.Name = "Button24"
        Me.Button24.Size = New System.Drawing.Size(174, 23)
        Me.Button24.TabIndex = 54
        Me.Button24.Text = "AskAndBid"
        Me.Button24.UseVisualStyleBackColor = True
        '
        'Button25
        '
        Me.Button25.Location = New System.Drawing.Point(248, 387)
        Me.Button25.Name = "Button25"
        Me.Button25.Size = New System.Drawing.Size(174, 23)
        Me.Button25.TabIndex = 53
        Me.Button25.Text = "OrderBook"
        Me.Button25.UseVisualStyleBackColor = True
        '
        'Button26
        '
        Me.Button26.Location = New System.Drawing.Point(248, 362)
        Me.Button26.Name = "Button26"
        Me.Button26.Size = New System.Drawing.Size(174, 23)
        Me.Button26.TabIndex = 52
        Me.Button26.Text = "Info"
        Me.Button26.UseVisualStyleBackColor = True
        '
        'Button27
        '
        Me.Button27.Location = New System.Drawing.Point(248, 337)
        Me.Button27.Name = "Button27"
        Me.Button27.Size = New System.Drawing.Size(174, 23)
        Me.Button27.TabIndex = 51
        Me.Button27.Text = "Markets"
        Me.Button27.UseVisualStyleBackColor = True
        '
        'edBinancePair
        '
        Me.edBinancePair.Location = New System.Drawing.Point(294, 295)
        Me.edBinancePair.Name = "edBinancePair"
        Me.edBinancePair.Size = New System.Drawing.Size(120, 20)
        Me.edBinancePair.TabIndex = 49
        Me.edBinancePair.Text = "ADXBNB"
        '
        'Button22
        '
        Me.Button22.Location = New System.Drawing.Point(249, 216)
        Me.Button22.Name = "Button22"
        Me.Button22.Size = New System.Drawing.Size(174, 23)
        Me.Button22.TabIndex = 45
        Me.Button22.Text = "Info"
        Me.Button22.UseVisualStyleBackColor = True
        '
        'Button23
        '
        Me.Button23.Location = New System.Drawing.Point(249, 191)
        Me.Button23.Name = "Button23"
        Me.Button23.Size = New System.Drawing.Size(174, 23)
        Me.Button23.TabIndex = 44
        Me.Button23.Text = "Markets"
        Me.Button23.UseVisualStyleBackColor = True
        '
        'Button16
        '
        Me.Button16.Location = New System.Drawing.Point(249, 149)
        Me.Button16.Name = "Button16"
        Me.Button16.Size = New System.Drawing.Size(174, 23)
        Me.Button16.TabIndex = 43
        Me.Button16.Text = "Datatable"
        Me.Button16.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(245, 2)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(36, 13)
        Me.Label8.TabIndex = 42
        Me.Label8.Text = "Yobit"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(4, 3)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(61, 13)
        Me.Label9.TabIndex = 41
        Me.Label9.Text = "Bleutrade"
        '
        'Button15
        '
        Me.Button15.Location = New System.Drawing.Point(248, 93)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(174, 23)
        Me.Button15.TabIndex = 40
        Me.Button15.Text = "Trades"
        Me.Button15.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(248, 68)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(174, 23)
        Me.Button4.TabIndex = 39
        Me.Button4.Text = "Depth"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(248, 43)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(174, 23)
        Me.Button6.TabIndex = 38
        Me.Button6.Text = "Info"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(254, 128)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(43, 13)
        Me.Label10.TabIndex = 37
        Me.Label10.Text = "Market:"
        '
        'edYobitPair
        '
        Me.edYobitPair.Location = New System.Drawing.Point(303, 123)
        Me.edYobitPair.Name = "edYobitPair"
        Me.edYobitPair.Size = New System.Drawing.Size(120, 20)
        Me.edYobitPair.TabIndex = 36
        Me.edYobitPair.Text = "ltc_btc"
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(248, 18)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(174, 23)
        Me.Button5.TabIndex = 35
        Me.Button5.Text = "YobitTicker"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'edLastHours
        '
        Me.edLastHours.Location = New System.Drawing.Point(69, 302)
        Me.edLastHours.Name = "edLastHours"
        Me.edLastHours.Size = New System.Drawing.Size(112, 20)
        Me.edLastHours.TabIndex = 34
        Me.edLastHours.Text = "24"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(5, 305)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(58, 13)
        Me.Label7.TabIndex = 33
        Me.Label7.Text = "LastHours:"
        '
        'cboPeriod
        '
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Items.AddRange(New Object() {"1m ", "2m ", "3m ", "4m ", "5m ", "6m ", "10m ", "12m ", "15m ", "20m ", "30m ", "1h ", "2h ", "3h ", "4h ", "6h ", "8h ", "12h ", "1d"})
        Me.cboPeriod.Location = New System.Drawing.Point(68, 282)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(113, 21)
        Me.cboPeriod.TabIndex = 32
        Me.cboPeriod.Text = "1d"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(23, 285)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(40, 13)
        Me.Label6.TabIndex = 31
        Me.Label6.Text = "Period:"
        '
        'Button14
        '
        Me.Button14.Location = New System.Drawing.Point(6, 193)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(108, 25)
        Me.Button14.TabIndex = 30
        Me.Button14.Text = "GetCandles"
        Me.Button14.UseVisualStyleBackColor = True
        '
        'Button13
        '
        Me.Button13.Location = New System.Drawing.Point(6, 168)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(108, 25)
        Me.Button13.TabIndex = 29
        Me.Button13.Text = "MarketHistory"
        Me.Button13.UseVisualStyleBackColor = True
        '
        'cboMarketType
        '
        Me.cboMarketType.FormattingEnabled = True
        Me.cboMarketType.Items.AddRange(New Object() {"ALL", "BUY", "SELL"})
        Me.cboMarketType.Location = New System.Drawing.Point(68, 242)
        Me.cboMarketType.Name = "cboMarketType"
        Me.cboMarketType.Size = New System.Drawing.Size(113, 21)
        Me.cboMarketType.TabIndex = 28
        Me.cboMarketType.Text = "ALL"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(29, 245)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(34, 13)
        Me.Label5.TabIndex = 27
        Me.Label5.Text = "Type:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(24, 265)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(39, 13)
        Me.Label4.TabIndex = 25
        Me.Label4.Text = "Depth:"
        '
        'edDepth
        '
        Me.edDepth.Location = New System.Drawing.Point(69, 262)
        Me.edDepth.Name = "edDepth"
        Me.edDepth.Size = New System.Drawing.Size(112, 20)
        Me.edDepth.TabIndex = 24
        Me.edDepth.Text = "20"
        '
        'Button12
        '
        Me.Button12.Location = New System.Drawing.Point(6, 143)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(108, 25)
        Me.Button12.TabIndex = 23
        Me.Button12.Text = "Order Book"
        Me.Button12.UseVisualStyleBackColor = True
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(6, 118)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(108, 25)
        Me.Button11.TabIndex = 22
        Me.Button11.Text = "Ticker"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(20, 225)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(43, 13)
        Me.Label3.TabIndex = 21
        Me.Label3.Text = "Market:"
        '
        'edMarket
        '
        Me.edMarket.Location = New System.Drawing.Point(69, 221)
        Me.edMarket.Name = "edMarket"
        Me.edMarket.Size = New System.Drawing.Size(112, 20)
        Me.edMarket.TabIndex = 20
        Me.edMarket.Text = "ETH_BTC"
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(6, 93)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(108, 25)
        Me.Button10.TabIndex = 19
        Me.Button10.Text = "MarketSummary"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'PropertyGrid1
        '
        Me.PropertyGrid1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PropertyGrid1.Location = New System.Drawing.Point(616, 16)
        Me.PropertyGrid1.Name = "PropertyGrid1"
        Me.PropertyGrid1.Size = New System.Drawing.Size(323, 565)
        Me.PropertyGrid1.TabIndex = 18
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(6, 68)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(108, 25)
        Me.Button9.TabIndex = 17
        Me.Button9.Text = "Summaries"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(6, 43)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(108, 25)
        Me.Button8.TabIndex = 16
        Me.Button8.Text = "Markets"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(6, 18)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(108, 25)
        Me.Button7.TabIndex = 15
        Me.Button7.Text = "Currency"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(613, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 13)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "Detail"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(428, 2)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(49, 13)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Results"
        '
        'lstResults
        '
        Me.lstResults.FormattingEnabled = True
        Me.lstResults.Location = New System.Drawing.Point(428, 18)
        Me.lstResults.Name = "lstResults"
        Me.lstResults.Size = New System.Drawing.Size(177, 563)
        Me.lstResults.TabIndex = 1
        '
        'tabBrasiliex
        '
        Me.tabBrasiliex.Controls.Add(Me.btnGo)
        Me.tabBrasiliex.Controls.Add(Me.edUrl)
        Me.tabBrasiliex.Controls.Add(Me.edResultBrasiliex)
        Me.tabBrasiliex.Controls.Add(Me.Button2)
        Me.tabBrasiliex.Controls.Add(Me.Button3)
        Me.tabBrasiliex.Controls.Add(Me.Button1)
        Me.tabBrasiliex.Location = New System.Drawing.Point(4, 22)
        Me.tabBrasiliex.Name = "tabBrasiliex"
        Me.tabBrasiliex.Padding = New System.Windows.Forms.Padding(3)
        Me.tabBrasiliex.Size = New System.Drawing.Size(947, 658)
        Me.tabBrasiliex.TabIndex = 0
        Me.tabBrasiliex.Text = "Brasiliex"
        Me.tabBrasiliex.UseVisualStyleBackColor = True
        '
        'btnGo
        '
        Me.btnGo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnGo.Location = New System.Drawing.Point(852, 7)
        Me.btnGo.Name = "btnGo"
        Me.btnGo.Size = New System.Drawing.Size(33, 20)
        Me.btnGo.TabIndex = 10
        Me.btnGo.Text = "Go"
        Me.btnGo.UseVisualStyleBackColor = True
        '
        'edUrl
        '
        Me.edUrl.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.edUrl.Location = New System.Drawing.Point(9, 6)
        Me.edUrl.Name = "edUrl"
        Me.edUrl.Size = New System.Drawing.Size(837, 20)
        Me.edUrl.TabIndex = 9
        Me.edUrl.Text = "http://www.macoratti.net/12/01/vbn_webr1.htm"
        '
        'edResultBrasiliex
        '
        Me.edResultBrasiliex.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.edResultBrasiliex.Location = New System.Drawing.Point(105, 32)
        Me.edResultBrasiliex.Multiline = True
        Me.edResultBrasiliex.Name = "edResultBrasiliex"
        Me.edResultBrasiliex.Size = New System.Drawing.Size(780, 422)
        Me.edResultBrasiliex.TabIndex = 8
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(24, 92)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 7
        Me.Button2.Text = "teste"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(24, 63)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 6
        Me.Button3.Text = "Balance"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(24, 34)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Ticker"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'tabDetails
        '
        Me.tabDetails.Controls.Add(Me.Label28)
        Me.tabDetails.Controls.Add(Me.lstBetweenJson)
        Me.tabDetails.Controls.Add(Me.Label27)
        Me.tabDetails.Controls.Add(Me.lstBetweenExchanges)
        Me.tabDetails.Controls.Add(Me.prpExchanges)
        Me.tabDetails.Controls.Add(Me.Label14)
        Me.tabDetails.Controls.Add(Me.lstIntraExchanges)
        Me.tabDetails.Controls.Add(Me.grdRoundsDetails)
        Me.tabDetails.Controls.Add(Me.grdLegsDetails)
        Me.tabDetails.Controls.Add(Me.Label17)
        Me.tabDetails.Controls.Add(Me.Label18)
        Me.tabDetails.Location = New System.Drawing.Point(4, 22)
        Me.tabDetails.Name = "tabDetails"
        Me.tabDetails.Padding = New System.Windows.Forms.Padding(3)
        Me.tabDetails.Size = New System.Drawing.Size(947, 658)
        Me.tabDetails.TabIndex = 4
        Me.tabDetails.Text = "Details"
        Me.tabDetails.UseVisualStyleBackColor = True
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(341, 7)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(82, 13)
        Me.Label28.TabIndex = 42
        Me.Label28.Text = "BetweenJson"
        '
        'lstBetweenJson
        '
        Me.lstBetweenJson.FormattingEnabled = True
        Me.lstBetweenJson.Location = New System.Drawing.Point(341, 23)
        Me.lstBetweenJson.Name = "lstBetweenJson"
        Me.lstBetweenJson.Size = New System.Drawing.Size(117, 147)
        Me.lstBetweenJson.TabIndex = 41
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Location = New System.Drawing.Point(218, 7)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(56, 13)
        Me.Label27.TabIndex = 40
        Me.Label27.Text = "Between"
        '
        'lstBetweenExchanges
        '
        Me.lstBetweenExchanges.FormattingEnabled = True
        Me.lstBetweenExchanges.Location = New System.Drawing.Point(218, 23)
        Me.lstBetweenExchanges.Name = "lstBetweenExchanges"
        Me.lstBetweenExchanges.Size = New System.Drawing.Size(117, 147)
        Me.lstBetweenExchanges.TabIndex = 39
        '
        'prpExchanges
        '
        Me.prpExchanges.Location = New System.Drawing.Point(542, 7)
        Me.prpExchanges.Name = "prpExchanges"
        Me.prpExchanges.Size = New System.Drawing.Size(270, 203)
        Me.prpExchanges.TabIndex = 38
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(83, 7)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(33, 13)
        Me.Label14.TabIndex = 37
        Me.Label14.Text = "Intra"
        '
        'lstIntraExchanges
        '
        Me.lstIntraExchanges.FormattingEnabled = True
        Me.lstIntraExchanges.Location = New System.Drawing.Point(83, 23)
        Me.lstIntraExchanges.Name = "lstIntraExchanges"
        Me.lstIntraExchanges.Size = New System.Drawing.Size(129, 147)
        Me.lstIntraExchanges.TabIndex = 36
        '
        'grdRoundsDetails
        '
        Me.grdRoundsDetails.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdRoundsDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdRoundsDetails.Location = New System.Drawing.Point(83, 213)
        Me.grdRoundsDetails.Name = "grdRoundsDetails"
        Me.grdRoundsDetails.Size = New System.Drawing.Size(729, 112)
        Me.grdRoundsDetails.TabIndex = 35
        '
        'grdLegsDetails
        '
        Me.grdLegsDetails.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdLegsDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdLegsDetails.Location = New System.Drawing.Point(83, 344)
        Me.grdLegsDetails.Name = "grdLegsDetails"
        Me.grdLegsDetails.Size = New System.Drawing.Size(729, 112)
        Me.grdLegsDetails.TabIndex = 34
        '
        'Label17
        '
        Me.Label17.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(80, 328)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(34, 13)
        Me.Label17.TabIndex = 32
        Me.Label17.Text = "Legs"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(83, 192)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(50, 13)
        Me.Label18.TabIndex = 31
        Me.Label18.Text = "Rounds"
        '
        'tabGenerateRounds
        '
        Me.tabGenerateRounds.Controls.Add(Me.Label24)
        Me.tabGenerateRounds.Controls.Add(Me.edListToFile)
        Me.tabGenerateRounds.Controls.Add(Me.Label23)
        Me.tabGenerateRounds.Controls.Add(Me.edPreferentialCoins)
        Me.tabGenerateRounds.Controls.Add(Me.Button21)
        Me.tabGenerateRounds.Controls.Add(Me.btnExportTXT)
        Me.tabGenerateRounds.Controls.Add(Me.Label22)
        Me.tabGenerateRounds.Controls.Add(Me.edFileoutput)
        Me.tabGenerateRounds.Controls.Add(Me.lstMarkets)
        Me.tabGenerateRounds.Controls.Add(Me.cboExchangeName)
        Me.tabGenerateRounds.Controls.Add(Me.Label21)
        Me.tabGenerateRounds.Controls.Add(Me.Button19)
        Me.tabGenerateRounds.Location = New System.Drawing.Point(4, 22)
        Me.tabGenerateRounds.Name = "tabGenerateRounds"
        Me.tabGenerateRounds.Padding = New System.Windows.Forms.Padding(3)
        Me.tabGenerateRounds.Size = New System.Drawing.Size(947, 658)
        Me.tabGenerateRounds.TabIndex = 5
        Me.tabGenerateRounds.Text = "Gerar Rounds para Exchange"
        Me.tabGenerateRounds.UseVisualStyleBackColor = True
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(227, 221)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(88, 13)
        Me.Label24.TabIndex = 39
        Me.Label24.Text = "File output name:"
        '
        'edListToFile
        '
        Me.edListToFile.Location = New System.Drawing.Point(321, 221)
        Me.edListToFile.Name = "edListToFile"
        Me.edListToFile.Size = New System.Drawing.Size(351, 20)
        Me.edListToFile.TabIndex = 38
        Me.edListToFile.Text = "Market.txt"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(223, 21)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(92, 13)
        Me.Label23.TabIndex = 37
        Me.Label23.Text = "Preferential Coins:"
        '
        'edPreferentialCoins
        '
        Me.edPreferentialCoins.Location = New System.Drawing.Point(321, 18)
        Me.edPreferentialCoins.Name = "edPreferentialCoins"
        Me.edPreferentialCoins.Size = New System.Drawing.Size(351, 20)
        Me.edPreferentialCoins.TabIndex = 36
        Me.edPreferentialCoins.Text = "PreferentialCoins.txt"
        '
        'Button21
        '
        Me.Button21.Location = New System.Drawing.Point(321, 159)
        Me.Button21.Name = "Button21"
        Me.Button21.Size = New System.Drawing.Size(156, 23)
        Me.Button21.TabIndex = 35
        Me.Button21.Text = "Generate Rounds to File"
        Me.Button21.UseVisualStyleBackColor = True
        '
        'btnExportTXT
        '
        Me.btnExportTXT.Location = New System.Drawing.Point(321, 247)
        Me.btnExportTXT.Name = "btnExportTXT"
        Me.btnExportTXT.Size = New System.Drawing.Size(156, 23)
        Me.btnExportTXT.TabIndex = 34
        Me.btnExportTXT.Text = "Save List to File"
        Me.btnExportTXT.UseVisualStyleBackColor = True
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(214, 136)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(101, 13)
        Me.Label22.TabIndex = 33
        Me.Label22.Text = "Rounds Output File:"
        '
        'edFileoutput
        '
        Me.edFileoutput.Location = New System.Drawing.Point(321, 133)
        Me.edFileoutput.Name = "edFileoutput"
        Me.edFileoutput.Size = New System.Drawing.Size(351, 20)
        Me.edFileoutput.TabIndex = 32
        Me.edFileoutput.Text = "Rounds.txt"
        '
        'lstMarkets
        '
        Me.lstMarkets.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lstMarkets.FormattingEnabled = True
        Me.lstMarkets.Location = New System.Drawing.Point(25, 19)
        Me.lstMarkets.Name = "lstMarkets"
        Me.lstMarkets.Size = New System.Drawing.Size(177, 433)
        Me.lstMarkets.TabIndex = 31
        '
        'cboExchangeName
        '
        Me.cboExchangeName.FormattingEnabled = True
        Me.cboExchangeName.Items.AddRange(New Object() {"Bleutrade", "Binance", "Hitbtc", "Yobit"})
        Me.cboExchangeName.Location = New System.Drawing.Point(321, 44)
        Me.cboExchangeName.Name = "cboExchangeName"
        Me.cboExchangeName.Size = New System.Drawing.Size(113, 21)
        Me.cboExchangeName.TabIndex = 30
        Me.cboExchangeName.Text = "Yobit"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(260, 47)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(55, 13)
        Me.Label21.TabIndex = 29
        Me.Label21.Text = "Exchange"
        '
        'Button19
        '
        Me.Button19.Location = New System.Drawing.Point(321, 71)
        Me.Button19.Name = "Button19"
        Me.Button19.Size = New System.Drawing.Size(156, 23)
        Me.Button19.TabIndex = 0
        Me.Button19.Text = "List Markets to Screen"
        Me.Button19.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblStatus1, Me.lblStatus2, Me.lblStatus3, Me.lblStatusTime})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 712)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(979, 22)
        Me.StatusStrip1.TabIndex = 7
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'lblStatus1
        '
        Me.lblStatus1.Name = "lblStatus1"
        Me.lblStatus1.Size = New System.Drawing.Size(0, 17)
        '
        'lblStatus2
        '
        Me.lblStatus2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.lblStatus2.Name = "lblStatus2"
        Me.lblStatus2.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.lblStatus2.Size = New System.Drawing.Size(0, 17)
        '
        'lblStatus3
        '
        Me.lblStatus3.Name = "lblStatus3"
        Me.lblStatus3.Size = New System.Drawing.Size(0, 17)
        '
        'lblStatusTime
        '
        Me.lblStatusTime.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.lblStatusTime.Name = "lblStatusTime"
        Me.lblStatusTime.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.lblStatusTime.Size = New System.Drawing.Size(0, 17)
        Me.lblStatusTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'timerRefresh
        '
        '
        'timerWatch
        '
        Me.timerWatch.Interval = 1000
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Controls.Add(Me.btnIntraOrderRound3)
        Me.Panel1.Controls.Add(Me.btnIntraOrderRound2)
        Me.Panel1.Controls.Add(Me.btnIntraOrderRound1)
        Me.Panel1.Controls.Add(Me.Label45)
        Me.Panel1.Controls.Add(Me.Label44)
        Me.Panel1.Controls.Add(Me.Label43)
        Me.Panel1.Controls.Add(Me.Label40)
        Me.Panel1.Controls.Add(Me.edIntraQtdRound3)
        Me.Panel1.Controls.Add(Me.Label41)
        Me.Panel1.Controls.Add(Me.edIntraRateRound3)
        Me.Panel1.Controls.Add(Me.Label42)
        Me.Panel1.Controls.Add(Me.edIntraMarketRound3)
        Me.Panel1.Controls.Add(Me.Label37)
        Me.Panel1.Controls.Add(Me.edIntraQtdRound2)
        Me.Panel1.Controls.Add(Me.Label38)
        Me.Panel1.Controls.Add(Me.edIntraRateRound2)
        Me.Panel1.Controls.Add(Me.Label39)
        Me.Panel1.Controls.Add(Me.edIntraMarketRound2)
        Me.Panel1.Controls.Add(Me.Label34)
        Me.Panel1.Controls.Add(Me.edIntraQtdRound1)
        Me.Panel1.Controls.Add(Me.Label35)
        Me.Panel1.Controls.Add(Me.edIntraRateRound1)
        Me.Panel1.Controls.Add(Me.Label36)
        Me.Panel1.Controls.Add(Me.edIntraMarketRound1)
        Me.Panel1.Location = New System.Drawing.Point(3, 133)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(125, 519)
        Me.Panel1.TabIndex = 37
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(13, 101)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(49, 13)
        Me.Label34.TabIndex = 81
        Me.Label34.Text = "Quantity:"
        '
        'edIntraQtdRound1
        '
        Me.edIntraQtdRound1.Location = New System.Drawing.Point(13, 117)
        Me.edIntraQtdRound1.Name = "edIntraQtdRound1"
        Me.edIntraQtdRound1.Size = New System.Drawing.Size(108, 20)
        Me.edIntraQtdRound1.TabIndex = 80
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(13, 62)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(33, 13)
        Me.Label35.TabIndex = 79
        Me.Label35.Text = "Rate:"
        '
        'edIntraRateRound1
        '
        Me.edIntraRateRound1.Location = New System.Drawing.Point(13, 78)
        Me.edIntraRateRound1.Name = "edIntraRateRound1"
        Me.edIntraRateRound1.Size = New System.Drawing.Size(108, 20)
        Me.edIntraRateRound1.TabIndex = 78
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(13, 23)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(43, 13)
        Me.Label36.TabIndex = 77
        Me.Label36.Text = "Market:"
        '
        'edIntraMarketRound1
        '
        Me.edIntraMarketRound1.Location = New System.Drawing.Point(13, 39)
        Me.edIntraMarketRound1.Name = "edIntraMarketRound1"
        Me.edIntraMarketRound1.Size = New System.Drawing.Size(108, 20)
        Me.edIntraMarketRound1.TabIndex = 76
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(13, 272)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(49, 13)
        Me.Label37.TabIndex = 87
        Me.Label37.Text = "Quantity:"
        '
        'edIntraQtdRound2
        '
        Me.edIntraQtdRound2.Location = New System.Drawing.Point(13, 288)
        Me.edIntraQtdRound2.Name = "edIntraQtdRound2"
        Me.edIntraQtdRound2.Size = New System.Drawing.Size(108, 20)
        Me.edIntraQtdRound2.TabIndex = 86
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(13, 233)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(33, 13)
        Me.Label38.TabIndex = 85
        Me.Label38.Text = "Rate:"
        '
        'edIntraRateRound2
        '
        Me.edIntraRateRound2.Location = New System.Drawing.Point(13, 249)
        Me.edIntraRateRound2.Name = "edIntraRateRound2"
        Me.edIntraRateRound2.Size = New System.Drawing.Size(108, 20)
        Me.edIntraRateRound2.TabIndex = 84
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(13, 194)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(43, 13)
        Me.Label39.TabIndex = 83
        Me.Label39.Text = "Market:"
        '
        'edIntraMarketRound2
        '
        Me.edIntraMarketRound2.Location = New System.Drawing.Point(13, 210)
        Me.edIntraMarketRound2.Name = "edIntraMarketRound2"
        Me.edIntraMarketRound2.Size = New System.Drawing.Size(108, 20)
        Me.edIntraMarketRound2.TabIndex = 82
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(15, 445)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(49, 13)
        Me.Label40.TabIndex = 93
        Me.Label40.Text = "Quantity:"
        '
        'edIntraQtdRound3
        '
        Me.edIntraQtdRound3.Location = New System.Drawing.Point(15, 461)
        Me.edIntraQtdRound3.Name = "edIntraQtdRound3"
        Me.edIntraQtdRound3.Size = New System.Drawing.Size(108, 20)
        Me.edIntraQtdRound3.TabIndex = 92
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(15, 406)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(33, 13)
        Me.Label41.TabIndex = 91
        Me.Label41.Text = "Rate:"
        '
        'edIntraRateRound3
        '
        Me.edIntraRateRound3.Location = New System.Drawing.Point(15, 422)
        Me.edIntraRateRound3.Name = "edIntraRateRound3"
        Me.edIntraRateRound3.Size = New System.Drawing.Size(108, 20)
        Me.edIntraRateRound3.TabIndex = 90
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(15, 367)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(43, 13)
        Me.Label42.TabIndex = 89
        Me.Label42.Text = "Market:"
        '
        'edIntraMarketRound3
        '
        Me.edIntraMarketRound3.Location = New System.Drawing.Point(15, 383)
        Me.edIntraMarketRound3.Name = "edIntraMarketRound3"
        Me.edIntraMarketRound3.Size = New System.Drawing.Size(108, 20)
        Me.edIntraMarketRound3.TabIndex = 88
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.Location = New System.Drawing.Point(0, 6)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(49, 13)
        Me.Label43.TabIndex = 94
        Me.Label43.Text = "Order 1"
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label44.Location = New System.Drawing.Point(0, 176)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(49, 13)
        Me.Label44.TabIndex = 95
        Me.Label44.Text = "Order 2"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.Location = New System.Drawing.Point(2, 347)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(49, 13)
        Me.Label45.TabIndex = 96
        Me.Label45.Text = "Order 3"
        '
        'btnIntraOrderRound1
        '
        Me.btnIntraOrderRound1.Location = New System.Drawing.Point(3, 143)
        Me.btnIntraOrderRound1.Name = "btnIntraOrderRound1"
        Me.btnIntraOrderRound1.Size = New System.Drawing.Size(114, 20)
        Me.btnIntraOrderRound1.TabIndex = 97
        Me.btnIntraOrderRound1.Text = "Send Order"
        Me.btnIntraOrderRound1.UseVisualStyleBackColor = True
        '
        'btnIntraOrderRound2
        '
        Me.btnIntraOrderRound2.Location = New System.Drawing.Point(3, 314)
        Me.btnIntraOrderRound2.Name = "btnIntraOrderRound2"
        Me.btnIntraOrderRound2.Size = New System.Drawing.Size(114, 20)
        Me.btnIntraOrderRound2.TabIndex = 98
        Me.btnIntraOrderRound2.Text = "Send Order"
        Me.btnIntraOrderRound2.UseVisualStyleBackColor = True
        '
        'btnIntraOrderRound3
        '
        Me.btnIntraOrderRound3.Location = New System.Drawing.Point(6, 487)
        Me.btnIntraOrderRound3.Name = "btnIntraOrderRound3"
        Me.btnIntraOrderRound3.Size = New System.Drawing.Size(114, 20)
        Me.btnIntraOrderRound3.TabIndex = 99
        Me.btnIntraOrderRound3.Text = "Send Order"
        Me.btnIntraOrderRound3.UseVisualStyleBackColor = True
        '
        'frmBotTest
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(979, 734)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.tabOportunities)
        Me.Name = "frmBotTest"
        Me.Text = "Bitvalex BotTest"
        Me.tabOportunities.ResumeLayout(False)
        Me.tabOport.ResumeLayout(False)
        Me.tabOport.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.tabRounds.ResumeLayout(False)
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        Me.SplitContainer2.Panel1.PerformLayout()
        Me.SplitContainer2.Panel2.ResumeLayout(False)
        Me.SplitContainer2.Panel2.PerformLayout()
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer2.ResumeLayout(False)
        CType(Me.grdRounds, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdLegs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabBetweenRounds.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        CType(Me.grdRoundsBetween, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdLegsBetween, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabConfig.ResumeLayout(False)
        Me.tabConfig.PerformLayout()
        Me.tabBleutrade.ResumeLayout(False)
        Me.tabBleutrade.PerformLayout()
        Me.tabBrasiliex.ResumeLayout(False)
        Me.tabBrasiliex.PerformLayout()
        Me.tabDetails.ResumeLayout(False)
        Me.tabDetails.PerformLayout()
        CType(Me.grdRoundsDetails, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdLegsDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabGenerateRounds.ResumeLayout(False)
        Me.tabGenerateRounds.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tabOportunities As System.Windows.Forms.TabControl
    Friend WithEvents tabBrasiliex As System.Windows.Forms.TabPage
    Friend WithEvents edResultBrasiliex As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents tabBleutrade As System.Windows.Forms.TabPage
    Friend WithEvents lstResults As System.Windows.Forms.ListBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button7 As Button
    Friend WithEvents Button8 As Button
    Friend WithEvents Button9 As Button
    Friend WithEvents PropertyGrid1 As PropertyGrid
    Friend WithEvents Label3 As Label
    Friend WithEvents edMarket As TextBox
    Friend WithEvents Button10 As Button
    Friend WithEvents Button11 As Button
    Friend WithEvents cboMarketType As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents edDepth As TextBox
    Friend WithEvents Button12 As Button
    Friend WithEvents Button13 As Button
    Friend WithEvents Button14 As Button
    Friend WithEvents cboPeriod As ComboBox
    Friend WithEvents Label6 As Label
    Friend WithEvents edLastHours As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents edYobitPair As TextBox
    Friend WithEvents Button5 As Button
    Friend WithEvents Button6 As Button
    Friend WithEvents Button15 As Button
    Friend WithEvents Button4 As Button
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents btnGo As Button
    Friend WithEvents edUrl As TextBox
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents lblStatus1 As ToolStripStatusLabel
    Friend WithEvents lblStatus2 As ToolStripStatusLabel
    Friend WithEvents Button16 As Button
    Friend WithEvents tabConfig As TabPage
    Friend WithEvents Label11 As Label
    Friend WithEvents edConfigFilesOutput As TextBox
    Friend WithEvents tabOport As TabPage
    Friend WithEvents btnOportunities As Button
    Friend WithEvents timerRefresh As Timer
    Friend WithEvents chkTimer As CheckBox
    Friend WithEvents cboRefreshTime As DomainUpDown
    Friend WithEvents Label16 As Label
    Friend WithEvents lblStatus3 As ToolStripStatusLabel
    Friend WithEvents tabDetails As TabPage
    Friend WithEvents prpExchanges As PropertyGrid
    Friend WithEvents Label14 As Label
    Friend WithEvents lstIntraExchanges As ListBox
    Friend WithEvents grdRoundsDetails As DataGridView
    Friend WithEvents grdLegsDetails As DataGridView
    Friend WithEvents Label17 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents tabRounds As TabPage


















    Friend WithEvents Label21 As Label
    Friend WithEvents Button19 As Button
    Friend WithEvents Button21 As Button
    Friend WithEvents Button22 As Button
    Friend WithEvents Button23 As Button
    Friend WithEvents Label22 As Label
    Friend WithEvents edBinancePair As TextBox
    Friend WithEvents Label23 As Label
    Friend WithEvents edMarketHitbtc As TextBox
    Friend WithEvents Label24 As Label
    Friend WithEvents Button24 As Button
    Friend WithEvents Button25 As Button
    Friend WithEvents Button26 As Button
    Friend WithEvents Button27 As Button

    Friend WithEvents lblStatusTime As ToolStripStatusLabel
    Friend WithEvents timerWatch As Timer
    Friend WithEvents tabGenerateRounds As TabPage
    Friend WithEvents cboExchangeName As ComboBox
    Friend WithEvents lstMarkets As ListBox
    Friend WithEvents btnExportTXT As Button


    Friend WithEvents edFileoutput As TextBox



    Friend WithEvents edPreferentialCoins As TextBox
    Friend WithEvents edListToFile As TextBox
    Friend WithEvents Label26 As Label
    Friend WithEvents Label25 As Label
    Friend WithEvents Button28 As Button
    Friend WithEvents Button29 As Button
    Friend WithEvents chkUseMySql As CheckBox
    Friend WithEvents Label27 As Label
    Friend WithEvents lstBetweenExchanges As ListBox
    Friend WithEvents Label28 As Label
    Friend WithEvents lstBetweenJson As ListBox
    Friend WithEvents Button30 As Button
    Friend WithEvents tabBetweenRounds As TabPage
    Friend WithEvents SplitContainer1 As SplitContainer
    Friend WithEvents Label29 As Label
    Friend WithEvents grdRoundsBetween As DataGridView
    Friend WithEvents Label30 As Label
    Friend WithEvents grdLegsBetween As DataGridView
    Friend WithEvents radBetweenOnlyOportunities As RadioButton
    Friend WithEvents radBetweenAllRounds As RadioButton
    Friend WithEvents radBetweenLegsAllLegs As RadioButton
    Friend WithEvents radBetweenLegsOnlyOportunities As RadioButton
    Friend WithEvents SplitContainer2 As SplitContainer
    Friend WithEvents grdRounds As DataGridView
    Friend WithEvents radIntraOnlyOportunities As RadioButton
    Friend WithEvents radIntraAllRounds As RadioButton
    Friend WithEvents Label13 As Label
    Friend WithEvents grdLegs As DataGridView
    Friend WithEvents Label15 As Label
    Friend WithEvents Button18 As Button
    Friend WithEvents Label12 As Label
    Friend WithEvents edCoins As TextBox
    Friend WithEvents Button31 As Button
    Friend WithEvents Button32 As Button
    Friend WithEvents Label19 As Label
    Friend WithEvents edOrderId As TextBox
    Friend WithEvents Button33 As Button
    Friend WithEvents Button34 As Button
    Friend WithEvents Label32 As Label
    Friend WithEvents edQuantityBleutrade As TextBox
    Friend WithEvents Label31 As Label
    Friend WithEvents edRateBleutrade As TextBox
    Friend WithEvents Label20 As Label
    Friend WithEvents edMarketBleutrade As TextBox
    Friend WithEvents Label33 As Label
    Friend WithEvents edCommentBleutrade As TextBox
    Friend WithEvents Button35 As Button
    Friend WithEvents Button36 As Button
    Friend WithEvents Button37 As Button
    Friend WithEvents Button38 As Button
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label34 As Label
    Friend WithEvents edIntraQtdRound1 As TextBox
    Friend WithEvents Label35 As Label
    Friend WithEvents edIntraRateRound1 As TextBox
    Friend WithEvents Label36 As Label
    Friend WithEvents edIntraMarketRound1 As TextBox
    Friend WithEvents Label45 As Label
    Friend WithEvents Label44 As Label
    Friend WithEvents Label43 As Label
    Friend WithEvents Label40 As Label
    Friend WithEvents edIntraQtdRound3 As TextBox
    Friend WithEvents Label41 As Label
    Friend WithEvents edIntraRateRound3 As TextBox
    Friend WithEvents Label42 As Label
    Friend WithEvents edIntraMarketRound3 As TextBox
    Friend WithEvents Label37 As Label
    Friend WithEvents edIntraQtdRound2 As TextBox
    Friend WithEvents Label38 As Label
    Friend WithEvents edIntraRateRound2 As TextBox
    Friend WithEvents Label39 As Label
    Friend WithEvents edIntraMarketRound2 As TextBox
    Friend WithEvents btnIntraOrderRound3 As Button
    Friend WithEvents btnIntraOrderRound2 As Button
    Friend WithEvents btnIntraOrderRound1 As Button
End Class
