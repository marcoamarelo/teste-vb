﻿Imports System.ComponentModel
Imports System.IO
Imports System.Threading
Imports MySql.Data
Imports MySql.Data.MySqlClient


Public Class frmBotTest


    Delegate Sub MessageEventDelegateCallBack(ByVal Sender As Object, ByVal pMessage As String, ByVal pMessageLevel As Integer)
    Delegate Sub RefreshFinishedDelegateCallBack(ByVal pConfigOportunities As bvxConfigOportunities)

    Private Property StartProcessTime As DateTime
    'usado para armazenar a instancia do gerador de rounds
    Private Property ConfigOportunitiesForAutoGenerateRounds As bvxConfigOportunities


    Private Sub OnLoadFinishedHandler(ByVal pConfigOportunities As bvxConfigOportunities)


        If lstIntraExchanges.InvokeRequired Then
            Dim e As RefreshFinishedDelegateCallBack = New RefreshFinishedDelegateCallBack(AddressOf OnLoadFinishedHandler)
            Me.Invoke(e, pConfigOportunities)
        Else
            ShowStatus(lblStatus1, "Showing rounds.")
            StopTimeWatch()
            grdRounds.DataSource = Nothing
            grdLegs.DataSource = Nothing
            ShowIntraObjects(ConfigOportunitiesSingleton.Instance)
            ShowBetweenObjects(ConfigOportunitiesSingleton.Instance)
            ShowStatus(lblStatus1, "Finished.")
        End If

    End Sub

    Private Sub OnRefreshFinishedHandler(ByVal pConfigOportunities As bvxConfigOportunities)

        StopTimeWatch()
        If lstIntraExchanges.InvokeRequired Then
            Dim e As RefreshFinishedDelegateCallBack = New RefreshFinishedDelegateCallBack(AddressOf OnRefreshFinishedHandler)
            Me.Invoke(e, pConfigOportunities)
        Else
            ShowIntraAndBetweenRounds(pConfigOportunities)
            timerRefresh.Enabled = chkTimer.Checked
        End If

    End Sub


    Private Sub OnMessageEventHandler(ByVal Sender As Object, ByVal pMessage As String, ByVal pMessageLevel As Integer)

        Try
            If lstIntraExchanges.InvokeRequired Then
                Dim e As MessageEventDelegateCallBack = New MessageEventDelegateCallBack(AddressOf OnMessageEventHandler)
                Me.Invoke(e, Sender, pMessage, pMessageLevel)
            Else
                ShowStatus(pMessageLevel, pMessage)
            End If
        Catch ex As Exception
            LogService.Write("Error on Form1.OnMessageEventHandler + [" + ex.ToString + "]", LogService.Type.FATAL)
            ShowStatus(3, "Error: " + ex.Message)
        End Try

    End Sub

    Private Sub ClearStatus()

        ShowStatus(lblStatus1, "")
        ShowStatus(lblStatus2, "")
        ShowStatus(lblStatus3, "")

    End Sub

    Private Sub ShowStatus(pMessageLevel As Integer, pMessage As String)

        Select Case pMessageLevel
            Case 1 : ShowStatus(lblStatus1, pMessage)
            Case 2 : ShowStatus(lblStatus2, pMessage)
            Case 3 : ShowStatus(lblStatus3, pMessage)
        End Select

    End Sub

    Private Sub ShowStatus(pLabel As ToolStripStatusLabel, pMessage As String)

        Try
            'para testes, retirar este log
            ' LogService.Write("ShowStatus: " + pMessage, LogService.Type.INFO)
            pLabel.Text = pMessage

            Application.DoEvents()
        Catch ex As Exception
            LogService.Write("Error on Form1.ShowStatus: " + ex.ToString, LogService.Type.FATAL)
            pLabel.Text = "Error: " + ex.Message
        End Try

    End Sub



    Private Sub ShowBetweenObjects(ByVal pObj As bvxConfigOportunities)
        Try
            lstBetweenExchanges.Items.Clear()
            For Each oBetweenRound In pObj.BetweenRounds
                lstBetweenExchanges.Items.Add(oBetweenRound)
            Next
            lstBetweenJson.Items.Clear()
            For Each oBetween In pObj.Between
                lstBetweenJson.Items.Add(oBetween)
            Next

        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
    End Sub


    Private Sub ShowIntraObjects(ByVal pObj As bvxConfigOportunities)
        Try
            lstIntraExchanges.Items.Clear()

            For Each oIntra In pObj.Intra
                lstIntraExchanges.Items.Add(oIntra.Exchange)
            Next
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
    End Sub

    Private Function GetArbitrageController() As bvxArbitrageController

        Dim objArbit As bvxArbitrageController = New bvxArbitrageController()
        AddHandler objArbit.OnMessageEvent, AddressOf OnMessageEventHandler
        AddHandler objArbit.OnLoadFinished, AddressOf OnLoadFinishedHandler
        AddHandler objArbit.OnRefreshFinished, AddressOf OnRefreshFinishedHandler
        Return objArbit

    End Function


    Private Function GetProcessManager() As bvxProcessManager

        Dim obj As bvxProcessManager = ProcessManagerSigleton.Instance
        AddHandler obj.OnMessageEvent, AddressOf OnMessageEventHandler
        AddHandler obj.OnRefreshFinished, AddressOf OnRefreshFinishedHandler
        Return obj

    End Function




    Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click

        Dim oWebAdapter = New WebAdapter()
        Try
            edResultBrasiliex.Text = oWebAdapter.SendRequest(edUrl.Text)
        Catch ex As Exception
            edResultBrasiliex.Text = "***Error*** - " + ex.Message
        End Try


    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        Dim oBrslx As BrasiliexAPI = APIFactory.CreateAPIObject("Brasiliex")
        edResultBrasiliex.Text = oBrslx.Ticker()

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        Dim oBrslx As BrasiliexAPI = APIFactory.CreateAPIObject("Brasiliex")
        edResultBrasiliex.Text = oBrslx.Teste()




    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click

        Dim oBrslx As BrasiliexAPI = APIFactory.CreateAPIObject("Brasiliex")
        Try
            edResultBrasiliex.Text = oBrslx.Balance()
        Catch ex As Exception
            edResultBrasiliex.Text = ex.Message
        End Try


    End Sub






    Private Sub Button4_Click(sender As Object, e As EventArgs)



    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs)


    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs)



    End Sub

    Private Sub Bleutrade_SendPublicReq(ByVal pCommand As String)

        'Dim obj As BleutradeAPI = New BleutradeAPI
        'edResultBleutrade.Text = obj.SendPublicRequest(pCommand)

    End Sub


    Private Sub lstBleutrade_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstResults.Click, lstResults.SelectedIndexChanged

        PropertyGrid1.SelectedObject = lstResults.SelectedItem


    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click

        Try
            ShowStatus(lblStatus1, "Requesting data...")
            Dim obj As BleutradeAPI = APIFactory.CreateAPIObject("Bleutrade")
            Dim Currencies As List(Of BleutradeCurrency) = obj.GetCurrencies()
            ShowResults(Of BleutradeCurrency)(Currencies)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try




    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click

        Try
            ShowStatus(lblStatus1, "Requesting data...")
            Dim obj As BleutradeAPI = APIFactory.CreateAPIObject("Bleutrade")
            Dim list As List(Of BleutradeMarket) = obj.GetMarkets()
            ShowResults(Of BleutradeMarket)(list)

            'Dim oDt As DataTable = bvxEncode.ListToDataTable(Of BleutradeMarket)(list)
            'bvxEncode.DatatableToCSV(oDt, edConfigFilesOutput.Text + oDt.TableName + ".csv")

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub





    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        Try
            ShowStatus(lblStatus1, "Requesting data...")
            Dim obj As BleutradeAPI = APIFactory.CreateAPIObject("Bleutrade")
            Dim List As List(Of BleutradeMarketSummary) = obj.GetMarketSummaries()
            ShowResults(Of BleutradeMarketSummary)(List)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click
        Try
            ShowStatus(lblStatus1, "Requesting data...")
            Dim obj As BleutradeAPI = APIFactory.CreateAPIObject("Bleutrade")
            Dim list As List(Of BleutradeMarketSummary) = obj.GetMarketSummary(edMarket.Text)
            ShowResults(Of BleutradeMarketSummary)(list)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
        Try
            ShowStatus(lblStatus1, "Requesting data...")
            Dim obj As BleutradeAPI = APIFactory.CreateAPIObject("Bleutrade")
            Dim Currencies As List(Of BleutradeTicker) = obj.GetTicker(edMarket.Text)

            ShowResults(Of BleutradeTicker)(Currencies)

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button12_Click(sender As Object, e As EventArgs) Handles Button12.Click

        Try
            ShowStatus(lblStatus1, "Requesting data...")
            Dim obj As BleutradeAPI = APIFactory.CreateAPIObject("Bleutrade")
            Dim result As BleutradeMarketBookOrders = obj.GetOrderBook(edMarket.Text, cboMarketType.Text, edDepth.Text)
            ShowResults(Of BleutradeMarketBookOrders)(result)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button13_Click(sender As Object, e As EventArgs) Handles Button13.Click
        Try
            ShowStatus(lblStatus1, "Requesting data...")
            Dim obj As BleutradeAPI = APIFactory.CreateAPIObject("Bleutrade")
            Dim result As List(Of BleutradeMarketHistory) = obj.GetMarketHistory(edMarket.Text, edDepth.Text)
            ShowResults(Of BleutradeMarketHistory)(result)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button14_Click(sender As Object, e As EventArgs) Handles Button14.Click
        Try
            ShowStatus(lblStatus1, "Requesting data...")
            Dim obj As BleutradeAPI = APIFactory.CreateAPIObject("Bleutrade")
            Dim result As List(Of BleutradeCandle) = obj.GetCandles(edMarket.Text, cboPeriod.Text, edDepth.Text, edLastHours.Text)
            ShowResults(Of BleutradeCandle)(result)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button4_Click_1(sender As Object, e As EventArgs) Handles Button5.Click
        Try
            ShowStatus(lblStatus1, "Requesting data...")
            Dim obj As YobitAPI = APIFactory.CreateAPIObject("Yobit")
            Dim result As YobitTicker = obj.GetTicker(edYobitPair.Text)
            ShowResults(Of YobitTicker)(result)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button6_Click_1(sender As Object, e As EventArgs) Handles Button6.Click
        ShowStatus(lblStatus1, "Requesting data...")
        Dim obj As YobitAPI = APIFactory.CreateAPIObject("Yobit")
        'Dim rawJson = File.ReadAllText(Path.Combine("c:\teste\yobitinfo.json"))
        Dim result As List(Of YobitInfoPair) = obj.GetInfo()
        ShowResults(Of YobitInfoPair)(result)


    End Sub

    Private Sub ShowResults(Of T)(pList As List(Of T))

        lstResults.Items.Clear()
        For Each oCur As T In pList
            lstResults.Items.Add(oCur)
        Next
        ShowStatus(lblStatus1, pList.Count.ToString + " resultados")

    End Sub





    Private Sub ShowResults(Of T)(pObj As T)

        lstResults.Items.Clear()
        If Not pObj Is Nothing Then
            lstResults.Items.Add(pObj)
            ShowStatus(lblStatus1, "1 resultado")
        Else
            ShowStatus(lblStatus1, "0 resultados")
        End If


    End Sub



    Private Sub Button4_Click_2(sender As Object, e As EventArgs) Handles Button4.Click

        ShowStatus(lblStatus1, "Requesting data...")
        Dim obj As YobitAPI = APIFactory.CreateAPIObject("Yobit")
        'Dim rawJson = File.ReadAllText(Path.Combine("c:\teste\yobitinfo.json"))
        Dim result As YobitDepth = obj.GetDepth(edYobitPair.Text)
        ShowResults(Of YobitDepth)(result)

    End Sub

    Private Sub Button15_Click(sender As Object, e As EventArgs) Handles Button15.Click

        ShowStatus(lblStatus1, "Requesting data...")
        Dim obj As YobitAPI = APIFactory.CreateAPIObject("Yobit")
        'Dim rawJson = File.ReadAllText(Path.Combine("c:\teste\yobitinfo.json"))
        Dim result As List(Of YobitTrade) = obj.GetTrades(edYobitPair.Text)
        ShowResults(Of YobitTrade)(result)

    End Sub

    Private Sub StatusStrip1_ItemClicked(sender As Object, e As ToolStripItemClickedEventArgs) Handles StatusStrip1.ItemClicked

    End Sub

    Private Sub Button16_Click(sender As Object, e As EventArgs) Handles Button16.Click

    End Sub

    Private Sub btnOportunities_Click(sender As Object, e As EventArgs) Handles btnOportunities.Click

        StartOportunities()

    End Sub



    Private Sub ShowIntraAndBetweenRounds(ByVal pObj As bvxConfigOportunities)

        grdRounds.DataSource = Nothing
        grdLegs.DataSource = Nothing
        Dim oController = GetArbitrageController()
        Dim oDt As DataTable = Nothing
        ' intra
        FilterIntraRounds()
        ' between
        FilterBetweenRounds()
    End Sub

    Private Sub FilterIntraRounds()
        Dim oController = GetArbitrageController()
        Dim oDt = oController.GetIntraRoundsDatatable(ConfigOportunitiesSingleton.Instance, radIntraOnlyOportunities.Checked)
        grdRounds.DataSource = oDt
    End Sub


    Private Sub FilterBetweenRounds()
        Dim oController = GetArbitrageController()
        Dim oDt = oController.GetBetweenRoundsDatatable(ConfigOportunitiesSingleton.Instance, radBetweenOnlyOportunities.Checked)
        grdRoundsBetween.DataSource = oDt
    End Sub





    Private Sub ShowBetweenJsonObjects(ByVal pObj As bvxConfigOportunities, pIndex As Integer)
        grdRoundsDetails.DataSource = Nothing
        grdLegsDetails.DataSource = Nothing
        Dim obj = pObj.Between(pIndex)
        Dim oController = GetArbitrageController()
        'Dim oDt = oController.GetBetweenRoundsDatatable(obj)
        'grdRoundsDetails.DataSource = oDt
        prpExchanges.SelectedObject = obj
    End Sub

    Private Sub ShowBetweenRoundsObjects(ByVal pObj As bvxConfigOportunities, pIndex As Integer)
        'grdRoundsDetails.DataSource = Nothing
        'grdLegsDetails.DataSource = Nothing
        'Dim obj = pObj.BetweenRounds(pIndex)
        'Dim oController = GetArbitrageController()
        'Dim oDt = oController.GetBetweenRoundsDatatable(obj)
        'grdRoundsDetails.DataSource = oDt
        'prpExchanges.SelectedObject = obj
    End Sub



    Private Sub ShowIntraRoundsObjects(ByVal pObj As bvxConfigOportunities, pIntraIndex As Integer)
        grdRoundsDetails.DataSource = Nothing
        grdLegsDetails.DataSource = Nothing
        Dim oIntra = pObj.Intra(pIntraIndex)
        Dim oController = GetArbitrageController()
        Dim oDt = oController.GetIntraRoundsDatatable(oIntra)
        grdRoundsDetails.DataSource = oDt
        prpExchanges.SelectedObject = oIntra
    End Sub

    Private Sub ShowIntraRoundLegs(ByVal pObj As bvxConfigOportunities, pIntraIndex As Integer, pRoundIndex As Integer)

        If pIntraIndex <= pObj.Intra.Count - 1 And pIntraIndex >= 0 Then
            Dim oIntra = pObj.Intra(pIntraIndex)
            If oIntra.Valid Then
                Dim oRound = oIntra.Rounds(pRoundIndex)
                If oRound.Valid Then
                    grdLegsDetails.DataSource = Nothing
                    Dim oController = GetArbitrageController()
                    Dim oDt = oController.GetLegsList(oRound)
                    grdLegsDetails.DataSource = oDt

                Else
                    ShowStatus(lblStatus1, "Round is invalid.")
                    grdLegsDetails.DataSource = Nothing
                End If
            Else
                ShowStatus(lblStatus1, "Exchange is invalid.")
            End If
        End If

    End Sub


    'Private Sub RefreshOportunities()

    '    Try
    '        StartTimeWatch()
    '        ClearStatus()
    '        Dim objArbit As bvxArbitrageController = GetArbitrageController()
    '        Dim oThread = New Thread(AddressOf objArbit.RefreshIntraOportunities)
    '        oThread.Start()

    '    Catch ex As Exception
    '        ShowStatus(lblStatus1, "Finished with errors.")
    '        MessageBox.Show(ex.ToString())
    '    End Try


    'End Sub



    Private Sub StartOportunities()
        'read oportunities file
        Dim Controller As bvxArbitrageController = GetArbitrageController()
        Controller.PersistInDatabase = chkUseMySql.Checked
        Try
            StartTimeWatch()
            ClearStatus()
            Dim OportunitiesFile As String = Application.StartupPath + AppSettingsReaderSingleton.Instance.GetValue("OportunitiesFile", GetType(String))

            ShowStatus(lblStatus1, "Loading configuration file.")
            Dim oThread = New Thread(AddressOf Controller.LoadConfigOportunitiesFromFile)
            oThread.Start(OportunitiesFile)
        Catch ex As Exception
            ShowStatus(lblStatus1, "Finished with errors.")
            MessageBox.Show(ex.ToString())
        End Try
    End Sub


    Private Sub RefreshOportunities3(pCalculateWithNoRefresh As Boolean)
        Try
            StartTimeWatch()
            ClearStatus()
            Dim Controller As bvxArbitrageController = GetArbitrageController()
            Dim oThread = New Thread(AddressOf Controller.RefreshIntraAndBetweenOportunities)
            oThread.Start(pCalculateWithNoRefresh)
        Catch ex As Exception
            ShowStatus(lblStatus1, "Finished with errors.")
            MessageBox.Show(ex.ToString())
        End Try
    End Sub

    Private Sub RefreshIntraAndBetweenOportunities()

        Try
            StartTimeWatch()
            ClearStatus()
            Dim objArbit As bvxArbitrageController = GetArbitrageController()
            Dim oThread = New Thread(AddressOf objArbit.RefreshIntraAndBetweenOportunities)
            oThread.Start()

        Catch ex As Exception
            ShowStatus(lblStatus1, "Finished with errors.")
            MessageBox.Show(ex.ToString())
        End Try


    End Sub


    Private Sub StartTimeWatch()

        StartProcessTime = Now
        timerWatch.Start()

    End Sub

    Private Sub StopTimeWatch()

        timerWatch.Stop()

    End Sub

    Private Sub RefreshTimeWatch()

        Dim elapsedtime = Now - StartProcessTime
        lblStatusTime.Text = "[" + Math.Truncate(elapsedtime.TotalMinutes).ToString.PadLeft(2, "0"c) + ":" +
            elapsedtime.Seconds.ToString.PadLeft(2, "0"c) + "]"
        Application.DoEvents()

    End Sub

    'Private Sub RefreshOportunities2()

    '    StartTimeWatch()
    '    ClearStatus()
    '    Dim Controller As bvxArbitrageController = GetArbitrageController()
    '    Dim ProcessManager = GetProcessManager()
    '    Try
    '        ProcessManager.Start_RefreshIntraOportunities(Controller)
    '    Catch ex As Exception
    '        MessageBox.Show(ex.ToString)
    '    Finally

    '    End Try


    'End Sub

    Private Sub CalculateOportunities()

        Try
            If Not ConfigOportunitiesSingleton.Instance Is Nothing Then
                Dim objArbit As bvxArbitrageController = GetArbitrageController()
                ShowStatus(lblStatus1, "Calculating oportunities.")
                objArbit.CalculateIntraOportunities2(ConfigOportunitiesSingleton.Instance)
                ShowStatus(lblStatus1, "Finished.")
            Else
                MessageBox.Show("Initialize oportunities first.")
            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
            ShowStatus(lblStatus1, "Finished With errors.")
        End Try

    End Sub


    Private Sub Button17_Click(sender As Object, e As EventArgs)



    End Sub

    'Private Sub refreshRoundMasterList()

    '    Dim controller = GetArbitrageController()
    '    controller.GetRoundsDataTable(ConfigOportunitiesSingleton.Instance)

    'End Sub

    Private Sub Button18_Click(sender As Object, e As EventArgs)



    End Sub

    Private Sub Button19_Click(sender As Object, e As EventArgs)


    End Sub

    Private Sub Button20_Click(sender As Object, e As EventArgs)



        'Dim obj As BleutradeAPI = APIFactory.CreateAPIObject("Bleutrade")
        'Dim result As String = obj.Balance()
        'MessageBox.Show(result)


        'Dim i As Integer = 4

        'Dim arr(i) As Double
        'arr(0) = 10
        'arr(1) = 20
        'arr(2) = 30
        'arr(3) = 40
        'Dim s As String = ""
        'For Each d In arr
        '    s = s + d.ToString() + " - "
        'Next
        'MessageBox.Show(s)

        'Dim duration As TimeSpan = New TimeSpan(0, 0, 23, 62)
        'Dim output As String = "Time of Travel: " + duration.ToString("c")
        'MessageBox.Show(output)
        'MessageBox.Show(String.Format("Time of Travel: {0:c}", duration))





        'Dim connString As String = "server=127.0.0.2; user id=bitvalex; password=123456; database=bitvalex;"

        'Dim oTran As MySqlTransaction = Nothing
        'Using sqlConn As New MySqlConnection(connString)
        '    Using sqlComm As New MySqlCommand()
        '        With sqlComm

        '            .Connection = sqlConn
        '        End With

        '        Try
        '            sqlConn.Open()
        '            otran = sqlConn.BeginTransaction()
        '            sqlComm.CommandText = "insert into exchange (name) values ('teste111')"
        '            sqlComm.ExecuteNonQuery()

        '            sqlComm.CommandText = "insert into exchange (name) values ('teste222')"
        '            sqlComm.ExecuteNonQuery()

        '            sqlComm.CommandText = "insert into exchang3e (name) values ('teste333')"
        '            sqlComm.ExecuteNonQuery()

        '            oTran.Commit()
        '        Catch ex As MySqlException
        '            oTran.Rollback()
        '            MessageBox.Show(ex.ToString)
        '        End Try
        '    End Using
        'End Using




        'Dim adapter = New MySQLDataAccess("127.0.0.2", "bitvalex", "123456", "bitvalex")
        'adapter.OpenConnection()
        'Dim list = adapter.ExecuteQuery("select * from exchange")
        'Dim oDt2 = New DataTable()
        'oDt2.Load(list)
        'MessageBox.Show(oDt2.Rows.Count)

        'Dim Dao = New OportunityDAO
        'Dim List = Dao.ListExchanges
        'MessageBox.Show(List.Count.ToString)
        'Dim id As Integer = Dao.SaveExchange("TESTE3")
        'MessageBox.Show(id.ToString())


    End Sub

    Private Sub lstExchanges_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstIntraExchanges.SelectedIndexChanged

        ShowIntraRoundsObjects(ConfigOportunitiesSingleton.Instance, lstIntraExchanges.SelectedIndex)

    End Sub

    Private Sub lstRounds_SelectedIndexChanged(sender As Object, e As EventArgs)



    End Sub

    Private Sub lstLegs_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub grdRounds_RowEnter(sender As Object, e As DataGridViewCellEventArgs) Handles grdRounds.RowEnter

        ShowLegsIntra(sender, e, grdLegs, False)


    End Sub


    Private Sub grdRoundsBetween_RowEnter(sender As Object, e As DataGridViewCellEventArgs) Handles grdRoundsBetween.RowEnter
        ShowLegsBetween(sender, e, grdLegsBetween, False, radBetweenLegsOnlyOportunities.Checked)
    End Sub


    Private Sub ShowLegsBetween(sender As Object, e As DataGridViewCellEventArgs, pGridDetail As DataGridView,
                                pOportunities As Boolean, pOnlyOportunities As Boolean)

        Dim ExchangeName1 = sender.Rows(e.RowIndex).Cells(sender.Columns("Exchange1").Index).FormattedValue
        Dim ExchangeName2 = sender.Rows(e.RowIndex).Cells(sender.Columns("Exchange2").Index).FormattedValue
        pGridDetail.DataSource = Nothing
        Dim oController = GetArbitrageController()
        Dim oDt = oController.GetLegsBetweenByExchangesNames(ConfigOportunitiesSingleton.Instance,
                                                             ExchangeName1, ExchangeName2, pOnlyOportunities)
        pGridDetail.DataSource = oDt

    End Sub



    Private Sub ShowLegsIntra(sender As Object, e As DataGridViewCellEventArgs, pGridDetail As DataGridView, pOportunities As Boolean)

        Dim Exchange = sender.Rows(e.RowIndex).Cells(sender.Columns("Exchange").Index).FormattedValue
        Dim Coins = sender.Rows(e.RowIndex).Cells(sender.Columns("Coins").Index).FormattedValue
        pGridDetail.DataSource = Nothing
        Dim oController = GetArbitrageController()
        Dim oDt = oController.GetLegsIntraByExchangeAndCoins(ConfigOportunitiesSingleton.Instance, Exchange, Coins, pOportunities)
        pGridDetail.DataSource = oDt

    End Sub


    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles timerRefresh.Tick

        ShowStatus(lblStatus2, "Timer on... " + Date.Now.ToString())
        timerRefresh.Enabled = False
        RefreshOportunities3(True)

    End Sub


    Private Sub SetTimer(ByVal pEnabled As Boolean)

        cboRefreshTime.Enabled = Not pEnabled

        If Not pEnabled Then
            timerRefresh.Enabled = False
            ShowStatus(lblStatus2, "Timer off")
        Else
            SetInterval()
            timerRefresh.Enabled = True
        End If

    End Sub


    Private Sub chkTimer_CheckedChanged(sender As Object, e As EventArgs) Handles chkTimer.CheckedChanged

        SetTimer(chkTimer.Checked)

    End Sub

    Private Sub SetInterval()

        Dim i As Integer
        If Not String.IsNullOrEmpty(cboRefreshTime.Text) Then

            Integer.TryParse(cboRefreshTime.Text, i)
            timerRefresh.Interval = i * 1000
            ShowStatus(lblStatus2, "Timer interval set: " + i.ToString())
        End If

    End Sub

    Private Sub cboRefreshTime_SelectedItemChanged(sender As Object, e As EventArgs) Handles cboRefreshTime.SelectedItemChanged




    End Sub

    Private Sub cboRefreshTime_TextChanged(sender As Object, e As EventArgs) Handles cboRefreshTime.TextChanged
        SetInterval()
    End Sub

    Private Sub grdRoundsDetails_RowEnter(sender As Object, e As DataGridViewCellEventArgs) Handles grdRoundsDetails.RowEnter
        ShowIntraRoundLegs(ConfigOportunitiesSingleton.Instance, lstIntraExchanges.SelectedIndex, e.RowIndex)
    End Sub


    Private Sub Button23_Click(sender As Object, e As EventArgs) Handles Button23.Click


        Try
            ShowStatus(lblStatus1, "Requesting data...")
            Dim obj As BinanceAPI = APIFactory.CreateAPIObject("Binance")
            Dim result As List(Of String) = obj.GetMarketNameList()
            ShowResults(Of String)(result)

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try



    End Sub

    Private Sub Button22_Click(sender As Object, e As EventArgs) Handles Button22.Click

        Try
            ShowStatus(lblStatus1, "Requesting data...")
            Dim obj As BinanceAPI = APIFactory.CreateAPIObject("Binance")
            Dim result As BinanceInfoResult = obj.GetExchangeInfo()
            ShowResults(Of BinanceInfoResult)(result)


            'Dim oDt As DataTable = bvxEncode.ListToDataTable(Of BleutradeMarket)(list)
            'bvxEncode.DatatableToCSV(oDt, edConfigFilesOutput.Text + oDt.TableName + ".csv")

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button21_Click(sender As Object, e As EventArgs) Handles Button21.Click

        GetGeneratedIntraRounds()

    End Sub






    Private Sub Button19_Click_1(sender As Object, e As EventArgs) Handles Button19.Click


        GetMarketsForExchange()


        'Try
        '    ShowStatus(lblStatus1, "Requesting data...")
        '    Dim obj As BinanceAPI = APIFactory.CreateAPIObject("Binance")
        '    Dim result As bvxArtbitIntraRoundLeg = obj.GetAskBidForMarket(edBinancePair.Text)
        '    ShowResults(Of bvxArtbitIntraRoundLeg)(result)

        'Catch ex As Exception
        '    MessageBox.Show(ex.Message)
        'End Try


    End Sub

    Private Sub Button26_Click(sender As Object, e As EventArgs) Handles Button26.Click


        Try
            ShowStatus(lblStatus1, "Requesting data...")
            Dim obj As HitbtcAPI = APIFactory.CreateAPIObject("Hitbtc")
            Dim result As List(Of HitbtcSymbol) = obj.GetSymbols()
            ShowResults(Of HitbtcSymbol)(result)

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try


    End Sub

    Private Sub Button27_Click(sender As Object, e As EventArgs) Handles Button27.Click


        Try
            ShowStatus(lblStatus1, "Requesting data...")
            Dim obj As HitbtcAPI = APIFactory.CreateAPIObject("Hitbtc")
            Dim result As List(Of String) = obj.GetMarketNameList()
            ShowResults(Of String)(result)

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button25_Click(sender As Object, e As EventArgs) Handles Button25.Click

        Try
            ShowStatus(lblStatus1, "Requesting data...")
            Dim obj As HitbtcAPI = APIFactory.CreateAPIObject("Hitbtc")
            Dim result As HitbtcMarketBookOrders = obj.GetOrderBook(edMarketHitbtc.Text)

            ShowResults(Of HitbtcMarketBookOrders)(result)

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button24_Click(sender As Object, e As EventArgs) Handles Button24.Click
        Try
            ShowStatus(lblStatus1, "Requesting data...")
            Dim obj As HitbtcAPI = APIFactory.CreateAPIObject("Hitbtc")
            Dim result As bvxPairPrices = obj.GetAskBidForMarket(edMarketHitbtc.Text)


            ShowResults(Of bvxPairPrices)(result)

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub frmBotTest_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ProcessManagerFactory.CreateObject()
        tabOportunities.SelectedIndex = 2
    End Sub

    Private Sub timerWatch_Tick(sender As Object, e As EventArgs) Handles timerWatch.Tick

        RefreshTimeWatch()

    End Sub


    Private Sub GetMarketsForExchange()
        Dim Controller = GetArbitrageController()
        Dim Exchange As bvxExchange = Controller.GetExchangeByNameFromDbWithMarketListFromAPI(
            cboExchangeName.Text, chkUseMySql.Checked)
        lstMarkets.Items.Clear()
        For Each s In Exchange.MarketList
            lstMarkets.Items.Add(s)
        Next
    End Sub


    Private Sub GetGeneratedIntraRounds()
        Dim Controller = GetArbitrageController()
        ClearStatus()
        Dim List = Controller.GenerateIntraRounds(cboExchangeName.Text,
                                                  Application.StartupPath + AppSettingsReaderSingleton.Instance.GetValue("ConfigurationFolder", GetType(String)) +
                                                  edPreferentialCoins.Text,
                                                  chkUseMySql.Checked)
        FileService.ListToTextFile(List, edConfigFilesOutput.Text + edFileoutput.Text)
        ClearStatus()
        MessageBox.Show("File saved in : " + edConfigFilesOutput.Text + edFileoutput.Text + " with " + List.Count.ToString + " records.")
    End Sub


    Private Sub btnExportTXT_Click(sender As Object, e As EventArgs) Handles btnExportTXT.Click

        SaveToFile(lstMarkets)


    End Sub


    Private Sub SaveToFile(pListBox As ListBox)

        Dim list = New List(Of String)
        For Each x As String In pListBox.Items
            list.Add(x)
        Next

        Dim s = FileService.ListToTextFile(list, edConfigFilesOutput.Text + edListToFile.Text)
        If s = "" Then
            MessageBox.Show("File saved: " + edConfigFilesOutput.Text + edListToFile.Text + " with " + list.Count.ToString + " records")
        Else
            MessageBox.Show("Error: " + edConfigFilesOutput.Text + edListToFile.Text + vbNewLine + vbNewLine + s)
        End If


    End Sub


    Private Sub Button29_Click(sender As Object, e As EventArgs) Handles Button29.Click
        Try
            ShowStatus(lblStatus1, "Requesting data...")
            Dim obj As BinanceAPI = APIFactory.CreateAPIObject("Binance")
            Dim result As BinanceDepthResult = obj.GetDepth(edBinancePair.Text)
            ShowResults(Of BinanceDepthResult)(result)

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button28_Click(sender As Object, e As EventArgs) Handles Button28.Click
        Try
            ShowStatus(lblStatus1, "Requesting data...")
            Dim obj As BinanceAPI = APIFactory.CreateAPIObject("Binance")
            Dim result As bvxPairPrices = obj.GetAskBidForMarket(edBinancePair.Text)
            ShowResults(Of bvxPairPrices)(result)

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub lstBetweenExchanges_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstBetweenExchanges.SelectedIndexChanged
        ShowBetweenRoundsObjects(ConfigOportunitiesSingleton.Instance, lstBetweenExchanges.SelectedIndex)
    End Sub

    Private Sub lstBetweenJson_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstBetweenJson.SelectedIndexChanged
        ShowBetweenJsonObjects(ConfigOportunitiesSingleton.Instance, lstBetweenJson.SelectedIndex)
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub Button30_Click(sender As Object, e As EventArgs) Handles Button30.Click
        RefreshOportunities3(True)
    End Sub





    Private Sub radBetweenOnlyOportunities_CheckedChanged_1(sender As Object, e As EventArgs) Handles radBetweenOnlyOportunities.CheckedChanged

        FilterBetweenRounds()

    End Sub

    Private Sub radBetweenAllRounds_CheckedChanged(sender As Object, e As EventArgs) Handles radBetweenAllRounds.CheckedChanged
        FilterBetweenRounds()
    End Sub



    Private Sub radIntraOnlyOportunities_CheckedChanged(sender As Object, e As EventArgs) Handles radIntraOnlyOportunities.CheckedChanged
        FilterIntraRounds()
    End Sub

    Private Sub radIntraAllRounds_CheckedChanged(sender As Object, e As EventArgs) Handles radIntraAllRounds.CheckedChanged
        FilterIntraRounds()
    End Sub

    Private Sub Button17_Click_1(sender As Object, e As EventArgs)
        Dim D As Decimal = Math.Pow(102, 0)
        MessageBox.Show(D.ToString())
        D = Math.Pow(102, 1)
        MessageBox.Show(D.ToString())
    End Sub

    Private Sub Button18_Click_1(sender As Object, e As EventArgs) Handles Button18.Click
        Try
            ShowStatus(lblStatus1, "Requesting data...")
            Dim obj As BleutradeAPI = APIFactory.CreateAPIObject("Bleutrade")
            Dim result As BleutradeResponseList(Of BleutradeBalance) = obj.Balances(edCoins.Text)
            ShowResults(Of BleutradeResponseList(Of BleutradeBalance))(result)
        Catch ex As Exception
            ShowStatus(lblStatus1, "Finished With errors.")
            MessageBox.Show(ex.Message)
        End Try




    End Sub

    Private Sub Button31_Click(sender As Object, e As EventArgs) Handles Button31.Click
        Try
            ShowStatus(lblStatus1, "Requesting data...")
            Dim obj As BleutradeAPI = APIFactory.CreateAPIObject("Bleutrade")
            Dim result As BleutradeResponseList(Of BleutradeOrder) = obj.GetOpenOrders()
            ShowResults(Of BleutradeResponseList(Of BleutradeOrder))(result)

        Catch ex As Exception
            ShowStatus(lblStatus1, "Finished With errors.")
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button32_Click(sender As Object, e As EventArgs) Handles Button32.Click
        Try
            ShowStatus(lblStatus1, "Requesting data...")
            Dim obj As BleutradeAPI = APIFactory.CreateAPIObject("Bleutrade")
            Dim result As BleutradeResponseSingle(Of BleutradeBalance) = obj.Balance(edCoins.Text)
            ShowResults(Of BleutradeResponseSingle(Of BleutradeBalance))(result)
        Catch ex As Exception
            ShowStatus(lblStatus1, "Finished With errors.")
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button33_Click(sender As Object, e As EventArgs) Handles Button33.Click
        Try
            ShowStatus(lblStatus1, "Requesting data...")
            Dim obj As BleutradeAPI = APIFactory.CreateAPIObject("Bleutrade")
            Dim result As BleutradeResponseSingle(Of BleutradeOrder) = obj.GetOrder(edOrderId.Text)
            ShowResults(Of BleutradeResponseSingle(Of BleutradeOrder))(result)
        Catch ex As Exception
            ShowStatus(lblStatus1, "Finished With errors.")
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button34_Click(sender As Object, e As EventArgs) Handles Button34.Click
        Try
            ShowStatus(lblStatus1, "Requesting data...")
            Dim obj As BleutradeAPI = APIFactory.CreateAPIObject("Bleutrade")
            Dim result As BleutradeResponseSingle(Of List(Of String)) = obj.CancelOrder(edOrderId.Text)
            ShowResults(Of BleutradeResponseSingle(Of List(Of String)))(result)
        Catch ex As Exception
            ShowStatus(lblStatus1, "Finished With errors.")
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button36_Click(sender As Object, e As EventArgs) Handles Button36.Click
        Try
            ShowStatus(lblStatus1, "Requesting data...")
            Dim obj As BleutradeAPI = APIFactory.CreateAPIObject("Bleutrade")
            Dim result As BleutradeResponseSingle(Of BleutradeOrderIdResult) =
                obj.BuyLimit(edMarketBleutrade.Text, edRateBleutrade.Text, edQuantityBleutrade.Text, edCommentBleutrade.Text)
            ShowResults(Of BleutradeResponseSingle(Of BleutradeOrderIdResult))(result)
        Catch ex As Exception
            ShowStatus(lblStatus1, "Finished With errors.")
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button37_Click(sender As Object, e As EventArgs) Handles Button37.Click
        GridToClipboard(grdLegs)
    End Sub

    Private Sub GridToClipboard(pGrid As DataGridView)
        Dim oDt As DataTable = pGrid.DataSource
        Dim s As String = bvxEncode.DataTableToString(oDt)
        Windows.Forms.Clipboard.SetText(s)
    End Sub

    Private Sub Button38_Click(sender As Object, e As EventArgs) Handles Button38.Click
        GridToClipboard(grdRounds)
    End Sub
End Class

