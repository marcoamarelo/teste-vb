﻿Imports NUnit.Framework
Imports Teste1

<TestFixture()> Public Class BleutradeTest

    Public Property api As BleutradeAPI

    <SetUp> Public Sub CreateAPIObject()
        Me.api = New BleutradeAPI("26.0F09Fa0bb0a7c22026dedb786bc4611",
                                   "ac9a52d2f4fa54de029e1efd78963eaaa0837dc9",
                                   "https://bleutrade.com/api/v2/public/",
                                   "https://bleutrade.com/api/v2/account/",
                                   "Bleutrade")
    End Sub

    <Test()> Public Sub GetBleutradeMarketNameList()
        Dim marketList = api.GetMarketNameList()

        Assert.NotZero(marketList.Count)
    End Sub

    <Test()> Public Sub GetBleutradeAskBidForMarketTest()
        Dim intraLegs = api.GetAskBidForMarket("ETH_BTC")

        Assert.Greater(intraLegs.Ask, 0)
        Assert.Greater(intraLegs.AskVolume, 0)
        Assert.Greater(intraLegs.Bid, 0)
        Assert.Greater(intraLegs.BidVolume, 0)
    End Sub
End Class
