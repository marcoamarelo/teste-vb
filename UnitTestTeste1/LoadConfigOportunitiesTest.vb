﻿Imports NUnit.Framework
Imports Teste1

<TestFixture()> Public Class LoadConfigOportunitiesTest

    <Test()> Public Sub JsonClassBuilding()
        Dim rawJson As String

        rawJson = "{""FileName"": """",
                    ""Intra"": [
                    {""Exchange"": ""Bleutrade"",""Arbit_Intra"": [""BTC,HTML,DOGE"",""BTC,HTML,ETH""]}, 
                    {""Exchange"": ""Yobit"",""Arbit_Intra"": [""BTC,HTML5,DOGE""]}
                    ]
                }"

        ConfigOportunitiesSingleton.LoadConfig(JsonFactory.CreateJsonObject(rawJson))

        Dim objConfigOportunities = ConfigOportunitiesSingleton.Instance

        Assert.IsInstanceOf(GetType(bvxConfigOportunities), objConfigOportunities)
        Assert.AreEqual(objConfigOportunities.Intra.Count, 2)

    End Sub

End Class